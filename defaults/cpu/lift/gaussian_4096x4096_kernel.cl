// High-level hash: b25aafd5344e2613607aeacf0ac8b39af4010fc888fe7aaa7d2630b91f7a6de9
// Low-level hash: 085e372cedf4d8af299866b338acf75ff98b7fbec6da7b4af4f58984685c5b6f
#define NUM_GROUPS_0 (256 / 8)
#define NUM_GROUPS_1 (512 / 2)
float idfloat(float x){
  { return x; }
}
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__155, global float* v__186){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__180[(16+(4*11)+(4*341)+(11*341))];
  /* Typed Value memory */
  /* Private Memory */
  float v__185_0;

  for (int v_wg_id_147 = get_group_id(1); v_wg_id_147 < (4092 / 11); v_wg_id_147 = (v_wg_id_147 + NUM_GROUPS_1)) {
    for (int v_wg_id_150 = get_group_id(0); v_wg_id_150 < (4092 / 341); v_wg_id_150 = (v_wg_id_150 + NUM_GROUPS_0)) {
      for (int v_l_id_151 = get_local_id(1); v_l_id_151 < (4 + 11); v_l_id_151 = (v_l_id_151 + 2)) {
        for (int v_l_id_152 = get_local_id(0); v_l_id_152 < (4 + 341); v_l_id_152 = (v_l_id_152 + 8)) {
          v__180[(v_l_id_152 + (4 * v_l_id_151) + (341 * v_l_id_151))] = idfloat(v__155[(v_l_id_152 + (4096 * 11 * v_wg_id_147) + (4096 * (v_wg_id_150 / (4092 / 341))) + (4096 * (((4092 / 341) * (v_l_id_151 % (4 + 11))) / (4092 / 341))) + (341 * ((v_wg_id_150 + ((4092 / 341) * (v_l_id_151 % (4 + 11)))) % (4092 / 341))))]);
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);

      for (int v_l_id_153 = get_local_id(1); v_l_id_153 < 11; v_l_id_153 = (v_l_id_153 + 2)) {
        for (int v_l_id_154 = get_local_id(0); v_l_id_154 < 341; v_l_id_154 = (v_l_id_154 + 8)) {
          v__185_0 = jacobi(v__180[((v_l_id_154 % 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(1 + (v_l_id_154 % 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(2 + (v_l_id_154 % 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(3 + (v_l_id_154 % 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(4 + (v_l_id_154 % 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(4 + 341 + ((341 + v_l_id_154) % 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(5 + 341 + ((341 + v_l_id_154) % 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(6 + 341 + ((341 + v_l_id_154) % 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(7 + 341 + ((341 + v_l_id_154) % 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(8 + 341 + ((341 + v_l_id_154) % 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(8 + v_l_id_154 + (2 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(9 + v_l_id_154 + (2 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(10 + v_l_id_154 + (2 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(11 + v_l_id_154 + (2 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(12 + v_l_id_154 + (2 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(12 + v_l_id_154 + (3 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(13 + v_l_id_154 + (3 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(14 + v_l_id_154 + (3 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(15 + v_l_id_154 + (3 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(16 + v_l_id_154 + (3 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(16 + v_l_id_154 + (4 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(17 + v_l_id_154 + (4 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(18 + v_l_id_154 + (4 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(19 + v_l_id_154 + (4 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))], v__180[(20 + v_l_id_154 + (4 * 341) + (4 * v_l_id_153) + (341 * v_l_id_153))]);
          v__186[(8194 + v_l_id_154 + (-4096 * ((v_l_id_154 + (341 * ((v_wg_id_150 + ((4092 / 341) * ((v_l_id_153 + (11 * v_wg_id_150)) % 11))) % (4092 / 341)))) / 4095)) + (4096 * 11 * v_wg_id_147) + (-16777216 * (((((4092 / 341) * ((v_l_id_153 + (11 * v_wg_id_150)) % 11)) / (4092 / 341)) + (v_wg_id_150 / (4092 / 341)) + (11 * v_wg_id_147)) / 4095)) + (4096 * (((4092 / 341) * ((v_l_id_153 + (11 * v_wg_id_150)) % 11)) / (4092 / 341))) + (4096 * (v_wg_id_150 / (4092 / 341))) + (341 * ((v_wg_id_150 + ((4092 / 341) * ((v_l_id_153 + (11 * v_wg_id_150)) % 11))) % (4092 / 341))))] = id(v__185_0);
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);

    }
  }
}}