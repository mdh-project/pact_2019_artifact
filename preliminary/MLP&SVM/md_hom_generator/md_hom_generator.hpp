//
// Created by   on 31.10.2017.
//

#ifndef MD_BLAS_MD_HOM_HPP
#define MD_BLAS_MD_HOM_HPP

//#define INDIVIDUAL_INPUT_CACHING
#define POST_PROCESSING_1
#define POST_PROCESSING_2
//#define RUNTIME_INPUT_SIZE

//#define ADD_CB_INFO_COMMENTS

#include "include/helper.hpp"
#include "include/input_buffer.hpp"
#include "include/input_buffer_wrapper.hpp"
#include "include/input_scalar.hpp"
#include "include/input_scalar_wrapper.hpp"
#include "include/input_stencil_buffer.hpp"
#include "include/input_stencil_buffer_wrapper.hpp"
#include "include/input_wrapper.hpp"
#include "include/loop_generator.hpp"
#include "include/macros.hpp"
#include "include/md_hom.hpp"
#include "include/ocl_generator.hpp"
#include "include/result_buffer.hpp"
#include "include/scalar_function.hpp"
#include "include/types.hpp"
#include "include/configuration_generator.hpp"

#endif //MD_BLAS_MD_HOM_HPP
