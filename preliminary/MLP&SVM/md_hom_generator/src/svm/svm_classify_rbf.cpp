#include "../../md_hom_generator.hpp"

int generate_rbf_kernel_eval(){
    auto testData = md_hom::input_buffer("test_data", {md_hom::L(1), md_hom::R(1)});
    auto inputSV = md_hom::input_buffer("input_sv", {md_hom::L(2), md_hom::R(1)});
    auto result = md_hom::result_buffer("res", {md_hom::L(1), md_hom::L(2)});
    auto md_hom_gemm =  md_hom::md_hom<2, 1>("svm_compute_rbf",
                                             md_hom::inputs(testData, inputSV),
                                             md_hom::scalar_function("return 2 * test_data_val * input_sv_val - test_data_val * test_data_val - input_sv_val * input_sv_val;"),
                                             md_hom::scalar_function(""),
                                             result
    );
    auto generator = md_hom::generator::ocl_generator(md_hom_gemm);
    std::ofstream kernel_file;
    kernel_file.open("svm_rbf_kernel_batch.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
    kernel_file.open("svm_rbf_kernel_batch_2.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_2();
    kernel_file.close();
}

int generate_rbf_reduce_eval() {
    auto result = md_hom::input_buffer("result", {md_hom::L(1), md_hom::R(1)});
    auto alphas = md_hom::input_buffer("alphas", {md_hom::R(1)});
    auto labels = md_hom::input_buffer("labels", {md_hom::R(1)});
    auto res = md_hom::result_buffer("res", {md_hom::L(1)});

    auto rbf_reduce =  md_hom::md_hom<1, 1>("svm_reduce_rbf_eval",
                                            md_hom::inputs(result, alphas, labels),
                                            md_hom::scalar_function("return exp(GAMMA * result_val) * labels_val * alphas_val;"),
                                            md_hom::scalar_function(""),
                                            res
    );

    auto generator = md_hom::generator::ocl_generator(rbf_reduce);
    std::ofstream kernel_file;
    kernel_file.open("svm_reduce_rbf_eval.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
    kernel_file.open("svm_reduce_rbf_eval_2.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_2();
    kernel_file.close();
}

int main(){
    generate_rbf_kernel_eval();
    generate_rbf_reduce_eval();
}

