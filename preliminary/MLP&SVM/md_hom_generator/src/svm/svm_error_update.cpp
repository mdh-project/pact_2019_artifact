#include "../../md_hom_generator.hpp"

int error_update(){
    auto cache_data_i = md_hom::input_buffer("cache_data_i", {md_hom::L(1)});
    auto cache_data_j = md_hom::input_buffer("cache_data_j", {md_hom::L(1)});
    auto error = md_hom::input_buffer("error", {md_hom::L(1)});
    auto errorResult = md_hom::result_buffer("result", {md_hom::L(1)});

    auto coef_i = md_hom::input_scalar("coef_i");
    auto coef_j = md_hom::input_scalar("coef_j");

    auto md_hom_add =  md_hom::md_hom<1, 0>("compute_error_update",
                                            md_hom::inputs(cache_data_i, cache_data_j, coef_i, coef_j, error),
                                            md_hom::scalar_function("return error_val + coef_i_val * cache_data_i_val + coef_j_val * cache_data_j_val;"),
                                            md_hom::scalar_function(""),
                                            errorResult
    );
    auto generator = md_hom::generator::ocl_generator(md_hom_add);
    std::ofstream kernel_file;
    kernel_file.open("svm_error_update_kernel.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
    kernel_file.open("svm_error_update_kernel_2.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_2();
    kernel_file.close();
}

int main(){
    error_update();
}