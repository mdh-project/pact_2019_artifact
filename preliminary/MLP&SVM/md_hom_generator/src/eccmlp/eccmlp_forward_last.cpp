#include "../../md_hom_generator.hpp"

/**
 * L1: Number of CC's
 * L2: Size of next layer
 * R1: Size of current layer
 */
int main(){
    // Z of current layer
    auto Z = md_hom::input_buffer("Z", {md_hom::L(1), md_hom::R(1)});

    // B of current layer
    auto B = md_hom::input_buffer("B", {md_hom::L(1), md_hom::L(2)}, true);

    // weights between current and next layer
    auto W = md_hom::input_buffer("W", {md_hom::L(1), md_hom::L(2), md_hom::R(1)});

    // label of instances
    auto L = md_hom::input_buffer("L", {md_hom::L(1), md_hom::L(2)}, true);

    // after first scalar function: S of next layer
    // after result scalar function: D of next layer
    auto result = md_hom::result_buffer("result", {md_hom::L(1), md_hom::L(2)});

    // first scalar function to get S of next layer: Z * W
    auto f = md_hom::scalar_function("return Z_val * W_val;");

    // result scalar function to get D of next layer: theta'(S) * (L - theta(S))  // Z = theta(S) + B
    auto g = md_hom::scalar_function("return 1 / (1 + exp(-(res + B_val))) * (1 - 1 / (1 + exp(-(res + B_val)))) * (L_val - (1 / (1 + exp(-(res + B_val)))));");

    auto md_hom_gemm =  md_hom::md_hom<2, 1>("mlp_forward_last", md_hom::inputs(Z, W, B, L), f, g, result, true);

    auto generator = md_hom::generator::ocl_generator(md_hom_gemm);
    std::ofstream kernel_file;
    kernel_file.open("mlp_forward_last_1.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_1();
    kernel_file.close();
    kernel_file.open("mlp_forward_last_2.cl", std::fstream::out | std::fstream::trunc);
    kernel_file << generator.kernel_2();
    kernel_file.close();
}

