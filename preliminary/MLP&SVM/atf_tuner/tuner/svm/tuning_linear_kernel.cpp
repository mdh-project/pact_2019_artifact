#include "../../atf.h"
#include "generic/tuning_kernel_L1R1.hpp"

int main(int argc, char *argv[]) {
    if(std::string(argv[1]) != ".") {
        std::string path = "../tuning_results/" + std::string(argv[1]);
        _mkdir(path.c_str());
    }

    return tuning_kernel("./scripts/tuning_linear_kernel.sh",
                         "../tuning_results/" + std::string(argv[1]) + "/linear_kernel_tp.json",
                         argc,
                         argv);
}