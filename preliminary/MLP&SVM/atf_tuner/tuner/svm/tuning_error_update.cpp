#include "../../atf.h"
#include "../util.h"

int tuning_L1_kernel(std::string bash, std::string result, int argc, char **argv) {
    int platformId = atoi(argv[2]);
    int deviceId = atoi(argv[3]);
    std::string deviceType = argv[4];

    int threshold = atoi(argv[5]);
    int max_time = atoi(argv[6]);
    int M_1 = atoi(argv[7]);

    // get gpu device
    cl::Device device;
    getDevice(device, platformId, deviceId, deviceType);

    // get max devices sizes
    std::vector<size_t> max_wi_size;
    size_t max_wg_size;
    get_ocl_device_limits(device, &max_wi_size, &max_wg_size);

    auto TP_CACHE_L_CB = atf::tp("CACHE_L_CB", {0, 1});
    auto TP_CACHE_P_CB = atf::tp("CACHE_P_CB", {0, 1});

    auto TP_G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto TP_L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {0, 1, 2}, [&](auto TP_L_CB_RES_DEST_LEVEL) { return TP_L_CB_RES_DEST_LEVEL <= TP_G_CB_RES_DEST_LEVEL; });
    auto TP_P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {0, 1, 2}, [&](auto TP_P_CB_RES_DEST_LEVEL) { return TP_P_CB_RES_DEST_LEVEL <= TP_L_CB_RES_DEST_LEVEL; });

    auto TP_G_CB_SIZE_L_1 = atf::tp("G_CB_SIZE_L_1", {M_1});
    auto TP_L_CB_SIZE_L_1 = atf::tp("L_CB_SIZE_L_1", atf::interval(0, (int) ceil(log2(M_1)), atf::pow_2),
                                    [&](auto TP_L_CB_SIZE_L_1) { return TP_G_CB_SIZE_L_1 >= TP_L_CB_SIZE_L_1; });
    auto TP_P_CB_SIZE_L_1 = atf::tp("P_CB_SIZE_L_1", atf::interval(0, (int) ceil(log2(M_1)), atf::pow_2),
                                    [&](auto TP_P_CB_SIZE_L_1) { return TP_L_CB_SIZE_L_1 >= TP_P_CB_SIZE_L_1; });

    auto TP_NUM_WG_L_1 = atf::tp("NUM_WG_L_1", atf::interval(0, (int) ceil(log2(M_1)), atf::pow_2));

    auto TP_OCL_DIM_L_1 = atf::tp("OCL_DIM_L_1", {0});

    auto TP_NUM_WI_L_1 = atf::tp("NUM_WI_L_1", atf::interval(0, (int) ceil(log2(std::min(M_1, (int) max_wi_size[0]))), atf::pow_2),
                                 [&](auto TP_NUM_WI_L_1) { return TP_NUM_WI_L_1 <= max_wi_size[0]
                                        && TP_NUM_WI_L_1 <= max_wg_size; });

    size_t search_space_size = 0;
    {
        auto tuner = atf::open_tuner(atf::cond::evaluations(0));
        tuner(TP_CACHE_L_CB);
        tuner(TP_CACHE_P_CB);
        tuner(TP_G_CB_RES_DEST_LEVEL, TP_L_CB_RES_DEST_LEVEL, TP_P_CB_RES_DEST_LEVEL);
        tuner(TP_G_CB_SIZE_L_1, TP_L_CB_SIZE_L_1, TP_P_CB_SIZE_L_1);
        tuner(TP_NUM_WG_L_1);
        tuner(TP_OCL_DIM_L_1, TP_NUM_WI_L_1);
        search_space_size = tuner.search_space_size();
    }

    std::unique_ptr<atf::tuner_with_constraints> tuner;
    if(search_space_size <= threshold) {
        tuner = std::unique_ptr<atf::tuner_with_constraints>(new atf::exhaustive_class<>(atf::cond::evaluations(search_space_size) || atf::cond::duration<std::chrono::minutes>(max_time)));
    } else {
        tuner = std::unique_ptr<atf::tuner_with_constraints>(new atf::open_tuner_class<>(atf::cond::evaluations(std::min((size_t) threshold, size_t(0.25f * search_space_size))) || atf::cond::duration<std::chrono::minutes>(max_time)));
        if(0.25f * search_space_size < threshold)
            std::cout << "Small search space: Reducing # of configs to " << (0.25f * search_space_size) << std::endl;
    }

    (*tuner)(TP_CACHE_L_CB);
    (*tuner)(TP_CACHE_P_CB);
    (*tuner)(TP_G_CB_RES_DEST_LEVEL, TP_L_CB_RES_DEST_LEVEL, TP_P_CB_RES_DEST_LEVEL);
    (*tuner)(TP_G_CB_SIZE_L_1, TP_L_CB_SIZE_L_1, TP_P_CB_SIZE_L_1);
    (*tuner)(TP_NUM_WG_L_1);
    (*tuner)(TP_OCL_DIM_L_1, TP_NUM_WI_L_1);

    std::string args_sh = std::to_string(M_1);
    std::string bashStr = bash + std::string(" ") + args_sh;
    auto cf = atf::cf::bash(bashStr, "runtime");
    auto best_configuration = (*tuner)(cf);

    std::ostringstream result_oss;
    result_oss << "{\n"
               << "\t\"CACHE_L_CB\": " << best_configuration.at(TP_CACHE_L_CB.name()) << ",\n"
               << "\t\"CACHE_P_CB\": " << best_configuration.at(TP_CACHE_P_CB.name()) << ",\n"
               << "\t\"G_CB_RES_DEST_LEVEL\": " << best_configuration.at(TP_G_CB_RES_DEST_LEVEL.name()) << ",\n"
               << "\t\"L_CB_RES_DEST_LEVEL\": " << best_configuration.at(TP_L_CB_RES_DEST_LEVEL.name()) << ",\n"
               << "\t\"P_CB_RES_DEST_LEVEL\": " << best_configuration.at(TP_P_CB_RES_DEST_LEVEL.name()) << ",\n"
               << "\t\"L_CB_SIZE_L_1\": " << best_configuration.at(TP_L_CB_SIZE_L_1.name()) << ",\n"
               << "\t\"P_CB_SIZE_L_1\": " << best_configuration.at(TP_P_CB_SIZE_L_1.name()) << ",\n"
               << "\t\"NUM_WG_L_1\": " << best_configuration.at(TP_NUM_WG_L_1.name()) << ",\n"
               << "\t\"NUM_WI_L_1\": " << best_configuration.at(TP_NUM_WI_L_1.name()) << ",\n"
               << "\t\"OCL_DIM_L_1\": " << best_configuration.at(TP_OCL_DIM_L_1.name()) << "\n"
               << "}\n";

    std::string resultStr = result_oss.str();
    std::ofstream out(result);
    out << resultStr;
    out.close();

    return 0;
}

int main(int argc, char *argv[]) {
    if(std::string(argv[1]) != ".") {
        std::string path = "../tuning_results/" + std::string(argv[1]);
        _mkdir(path.c_str());
    }

    return tuning_L1_kernel("./scripts/tuning_error_update.sh",
                            "../tuning_results/" + std::string(argv[1]) + "/error_update_kernel_tp.json",
                            argc,
                            argv);
}