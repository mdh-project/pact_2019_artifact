# README 

### Dependencies
The tuner files were build with the following dependencies (other versions may work too):

- GCC 6.4.0
- CUDA 9.0
- OpenCL 2.0
- Python 2.7.12
- OpenTuner (http://opentuner.org/tutorial/setup/)

### Build
1. `mkdir build`
2. `cd build`
3. `cmake ..`
4. `make`

### Copy files:
- `./copy_eccmlp.sh`: Files will be copied to `../atf_tuning_eccmlp/tuner/`.
- `./copy_svm.sh`: Files will be copied to `../atf_tuning_svm/tuner/`.

### Tuning
Tuning for ECC+MLP: Use `../tuning_eccmlp/`.
Tuning for SVM: Use `../tuning_svm/`.
