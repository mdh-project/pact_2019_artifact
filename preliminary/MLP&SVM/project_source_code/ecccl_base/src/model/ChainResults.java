package model;

import java.util.HashMap;
import java.util.List;

public class ChainResults {
    private float[][] result_array;
    private int[] labelOrder;

    public ChainResults(int numInstances, int numLabels) {
        result_array = new float[numLabels][numInstances];
        labelOrder = new int[numLabels];
    }

    public void setLabelOrder(int id, int indexLabel) {
        labelOrder[id] = indexLabel;
    }

    public void putWithOrder(int idLabel, float[] result) {
        result_array[labelOrder[idLabel]] = result;
    }

    public void put(int idLabel, float[] result) {
        result_array[idLabel] = result;
    }

    public float getWithOrder(int indexLabel, int indexInstance) {
        return result_array[labelOrder[indexLabel]][indexInstance];
    }

    public float get(int idLabel, int indexInstance) {
        return result_array[idLabel][indexInstance];
    }
}
