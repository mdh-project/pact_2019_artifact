package model;

public class EccLabelResults {
    private int chainCount;
    private int instanceCount;
    private int labelCount;

    private float[][][] labelResults;
    private int[][] chainLabelOrder;

    public EccLabelResults(int chainCount, int instanceCount, int labelCount, int[][] chainLabelOrder) {
        this.chainCount = chainCount;
        this.instanceCount = instanceCount;
        this.labelCount = labelCount;
        this.chainLabelOrder = chainLabelOrder;

        this.labelResults = new float[chainCount][instanceCount][labelCount];
    }

    public void setChainLabelOrder(int[][] chainLabelOrder) {
        this.chainLabelOrder = chainLabelOrder;
    }

    public void set(int chain, int instance, int label, float value) {
        labelResults[chain][instance][label] = value;
    }

    public float[] get(int chain, int instance) {
        return labelResults[chain][instance];
    }

    public float[][] reduce() {
        float[][] result = new float[instanceCount][labelCount];

        for(int chain = 0; chain < chainCount; chain++) {
            for(int instance = 0; instance < instanceCount; instance++) {
                for(int label = 0; label < labelCount; label++) {
                    result[instance][chainLabelOrder[chain][label]] += labelResults[chain][instance][label];
                }
            }
        }

        return result;
    }
}
