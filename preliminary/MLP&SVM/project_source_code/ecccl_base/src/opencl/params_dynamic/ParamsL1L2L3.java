package opencl.params_dynamic;

import org.json.simple.JSONObject;
import util.JSONUtil;

public class ParamsL1L2L3 {
    public static final String TUNING_FILE_WEIGHT_UPDATE = "/tuning_results/eccmlp/weight_update_tp.json";

    private static ParamsL1L2L3 paramsForwardBatchActivation;

    private int CACHE_L_CB;
    private int CACHE_P_CB;

    private int G_CB_RES_DEST_LEVEL;
    private int L_CB_RES_DEST_LEVEL;
    private int P_CB_RES_DEST_LEVEL;

    // L1 Dimension
    private int L_CB_SIZE_L_1;
    private int P_CB_SIZE_L_1;

    private int NUM_WG_L_1;
    private int NUM_WI_L_1;
    private int OCL_DIM_L_1;

    // L2 Dimension
    private int L_CB_SIZE_L_2;
    private int P_CB_SIZE_L_2;

    private int NUM_WG_L_2;
    private int NUM_WI_L_2;
    private int OCL_DIM_L_2;

    // L3 Dimension
    private int L_CB_SIZE_L_3;
    private int P_CB_SIZE_L_3;

    private int NUM_WG_L_3;
    private int NUM_WI_L_3;
    private int OCL_DIM_L_3;

    public int LOCAL_SIZE_DIM_0;
    public int GLOBAL_SIZE_DIM_0;

    public int LOCAL_SIZE_DIM_1;
    public int GLOBAL_SIZE_DIM_1;

    public int LOCAL_SIZE_DIM_2;
    public int GLOBAL_SIZE_DIM_2;

    public int WORK_DIM = 3;

    public ParamsL1L2L3(String path) {
        JSONObject tps = JSONUtil.readFileArgs(path);
        setVars(tps);
    }

    public ParamsL1L2L3(JSONObject tps) {
        setVars(tps);
    }

    private void setVars(JSONObject tps) {
        CACHE_L_CB = ((Long) tps.get("CACHE_L_CB")).intValue();
        CACHE_P_CB = ((Long) tps.get("CACHE_P_CB")).intValue();
        G_CB_RES_DEST_LEVEL = ((Long) tps.get("G_CB_RES_DEST_LEVEL")).intValue();
        L_CB_RES_DEST_LEVEL = ((Long) tps.get("L_CB_RES_DEST_LEVEL")).intValue();
        P_CB_RES_DEST_LEVEL = ((Long) tps.get("P_CB_RES_DEST_LEVEL")).intValue();

        L_CB_SIZE_L_1 = ((Long) tps.get("L_CB_SIZE_L_1")).intValue();
        P_CB_SIZE_L_1 = ((Long) tps.get("P_CB_SIZE_L_1")).intValue();
        NUM_WG_L_1 = ((Long) tps.get("NUM_WG_L_1")).intValue();
        NUM_WI_L_1 = ((Long) tps.get("NUM_WI_L_1")).intValue();
        OCL_DIM_L_1 = ((Long) tps.get("OCL_DIM_L_1")).intValue();

        L_CB_SIZE_L_2 = ((Long) tps.get("L_CB_SIZE_L_2")).intValue();
        P_CB_SIZE_L_2 = ((Long) tps.get("P_CB_SIZE_L_2")).intValue();
        NUM_WG_L_2 = ((Long) tps.get("NUM_WG_L_2")).intValue();
        NUM_WI_L_2 = ((Long) tps.get("NUM_WI_L_2")).intValue();
        OCL_DIM_L_2 = ((Long) tps.get("OCL_DIM_L_2")).intValue();

        L_CB_SIZE_L_3 = ((Long) tps.get("L_CB_SIZE_L_3")).intValue();
        P_CB_SIZE_L_3 = ((Long) tps.get("P_CB_SIZE_L_3")).intValue();
        NUM_WG_L_3 = ((Long) tps.get("NUM_WG_L_3")).intValue();
        NUM_WI_L_3 = ((Long) tps.get("NUM_WI_L_3")).intValue();
        OCL_DIM_L_3 = ((Long) tps.get("OCL_DIM_L_3")).intValue();

        if(OCL_DIM_L_1 == 0) {
            LOCAL_SIZE_DIM_0 = NUM_WI_L_1;
            GLOBAL_SIZE_DIM_0 = NUM_WG_L_1 * NUM_WI_L_1;
        } else if (OCL_DIM_L_1 == 1) {
            LOCAL_SIZE_DIM_1 = NUM_WI_L_1;
            GLOBAL_SIZE_DIM_1 = NUM_WG_L_1 * NUM_WI_L_1;
        } else if (OCL_DIM_L_1 == 2) {
            LOCAL_SIZE_DIM_2 = NUM_WI_L_1;
            GLOBAL_SIZE_DIM_2 = NUM_WG_L_1 * NUM_WI_L_1;
        }

        if(OCL_DIM_L_2 == 0) {
            LOCAL_SIZE_DIM_0 = NUM_WI_L_2;
            GLOBAL_SIZE_DIM_0 = NUM_WG_L_2 * NUM_WI_L_2;
        } else if (OCL_DIM_L_2 == 1) {
            LOCAL_SIZE_DIM_1 = NUM_WI_L_2;
            GLOBAL_SIZE_DIM_1 = NUM_WG_L_2 * NUM_WI_L_2;
        } else if (OCL_DIM_L_2 == 2) {
            LOCAL_SIZE_DIM_2 = NUM_WI_L_2;
            GLOBAL_SIZE_DIM_2 = NUM_WG_L_2 * NUM_WI_L_2;
        }

        if(OCL_DIM_L_3 == 0) {
            LOCAL_SIZE_DIM_0 = NUM_WI_L_3;
            GLOBAL_SIZE_DIM_0 = NUM_WG_L_3 * NUM_WI_L_3;
        } else if (OCL_DIM_L_3 == 1) {
            LOCAL_SIZE_DIM_1 = NUM_WI_L_3;
            GLOBAL_SIZE_DIM_1 = NUM_WG_L_3 * NUM_WI_L_3;
        } else if (OCL_DIM_L_3 == 2) {
            LOCAL_SIZE_DIM_2 = NUM_WI_L_3;
            GLOBAL_SIZE_DIM_2 = NUM_WG_L_3 * NUM_WI_L_3;
        }
    }

    public String getOptions(int L1, int L2, int L3){
        return "-D CACHE_L_CB=" + CACHE_L_CB +
                " -D CACHE_P_CB=" + CACHE_P_CB +
                " -D G_CB_RES_DEST_LEVEL=" + G_CB_RES_DEST_LEVEL +
                " -D L_CB_RES_DEST_LEVEL=" + L_CB_RES_DEST_LEVEL +
                " -D P_CB_RES_DEST_LEVEL=" + P_CB_RES_DEST_LEVEL +
                " -D G_CB_SIZE_L_1=" + L1 +
                " -D L_CB_SIZE_L_1=" + L_CB_SIZE_L_1 +
                " -D P_CB_SIZE_L_1=" + P_CB_SIZE_L_1 +
                " -D NUM_WG_L_1=" + NUM_WG_L_1 +
                " -D NUM_WI_L_1=" + NUM_WI_L_1 +
                " -D OCL_DIM_L_1=" + OCL_DIM_L_1 +
                " -D G_CB_SIZE_L_2=" + L2 +
                " -D L_CB_SIZE_L_2=" + L_CB_SIZE_L_2 +
                " -D P_CB_SIZE_L_2=" + P_CB_SIZE_L_2 +
                " -D NUM_WG_L_2=" + NUM_WG_L_2 +
                " -D NUM_WI_L_2=" + NUM_WI_L_2 +
                " -D OCL_DIM_L_2=" + OCL_DIM_L_2 +
                " -D G_CB_SIZE_L_3=" + L3 +
                " -D L_CB_SIZE_L_3=" + L_CB_SIZE_L_3 +
                " -D P_CB_SIZE_L_3=" + P_CB_SIZE_L_3 +
                " -D NUM_WG_L_3=" + NUM_WG_L_3 +
                " -D NUM_WI_L_3=" + NUM_WI_L_3 +
                " -D OCL_DIM_L_3=" + OCL_DIM_L_3;
    }
}
