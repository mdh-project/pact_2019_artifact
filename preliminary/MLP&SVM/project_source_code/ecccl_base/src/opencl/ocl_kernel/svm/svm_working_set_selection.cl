// struct for storing value and index
typedef struct {
    float max;
    uint index_max;
    float min;
    uint index_min;
} value_index;

// max operator on struct
inline value_index min_max_op(value_index v1, value_index v2){
    value_index val;
    if(v1.max > v2.max) {
        val.max = v1.max;
        val.index_max = v1.index_max;
    } else {
        val.max = v2.max;
        val.index_max = v2.index_max;
    }
    if(v1.min < v2.min) {
        val.min = v1.min;
        val.index_min = v1.index_min;
    } else {
        val.min = v2.min;
        val.index_min = v2.index_min;
    }
    return val;
}

// helper macros
#define CONCAT_IN_DESCENDING_OCL_ORDER_0(i) i
#define CONCAT_IN_DESCENDING_OCL_ORDER(i) CONCAT_IN_DESCENDING_OCL_ORDER_0(i)

#define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(i_id, i_size) i_id
#define FLAT_INDEX_IN_DESCENDING_OCL_ORDER(i_id, i_size) FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(i_id, i_size)

// md_hom type definition
#define TYPE_T value_index
#define TYPE_TS value_index

#define PRIVATE 0
#define LOCAL   1
#define GLOBAL  2

// =============== macro definitions per dimension ============================
// -------------------- R_1 --------------------

// cache block sizes
#define K1_G_CB_SIZE_R_1 G_CB_SIZE_R_1
#define K1_L_CB_SIZE_R_1 L_CB_SIZE_R_1
#define K1_P_CB_SIZE_R_1 P_CB_SIZE_R_1

// functional unit ids
#define K1_G_FU_ID_R_1 i_wg_r_1
#define K1_L_FU_ID_R_1 i_wi_r_1
#define K1_P_FU_ID_R_1 0

// number of functional units
#define K1_G_NUM_FU_R_1 NUM_WG_R_1
#define K1_L_NUM_FU_R_1 NUM_WI_R_1
#define K1_P_NUM_FU_R_1 1

// number of cache blocks per functional unit
#define K1_G_NUM_CB_R_1 1 // == (N_1 / K1_G_CB_SIZE_R_1 / 1)
#define K1_L_NUM_CB_R_1 (K1_G_CB_SIZE_R_1 / K1_L_CB_SIZE_R_1 / K1_G_NUM_FU_R_1)
#define K1_P_NUM_CB_R_1 (K1_L_CB_SIZE_R_1 / K1_P_CB_SIZE_R_1 / K1_L_NUM_FU_R_1)

// number of extra cache blocks
#define K1_G_NUM_EXTRA_CB_R_1 0 // == (N_1 / K1_G_CB_SIZE_R_1 % 1)
#define K1_L_NUM_EXTRA_CB_R_1 (K1_G_CB_SIZE_R_1 / K1_L_CB_SIZE_R_1 % K1_G_NUM_FU_R_1)
#define K1_P_NUM_EXTRA_CB_R_1 (K1_L_CB_SIZE_R_1 / K1_P_CB_SIZE_R_1 % K1_L_NUM_FU_R_1)

// number of extra elements
#define K1_G_NUM_EXTRA_ELEMS_R_1 0 // == (N_1 % K1_G_CB_SIZE_R_1)
#define K1_L_NUM_EXTRA_ELEMS_R_1 (K1_G_CB_SIZE_R_1 % K1_L_CB_SIZE_R_1)
#define K1_P_NUM_EXTRA_ELEMS_R_1 (K1_L_CB_SIZE_R_1 % K1_P_CB_SIZE_R_1)

// size of incomplete cache blocks
#define K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1 (K1_L_CB_SIZE_R_1 % K1_P_CB_SIZE_R_1)
#define K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1 (K1_G_CB_SIZE_R_1 % K1_L_CB_SIZE_R_1 % K1_P_CB_SIZE_R_1)
#define K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1 (K1_G_CB_SIZE_R_1 % K1_L_CB_SIZE_R_1)

// number of cache blocks in incomplete parent cache block per functional unit
#define K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1 / K1_P_CB_SIZE_R_1 / K1_L_NUM_FU_R_1)

// number of extra cache blocks in incomplete parent cache block
#define K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 ((K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1 / K1_P_CB_SIZE_R_1) % K1_L_NUM_FU_R_1)

// number of extra elements in incomplete parent cache block
#define K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1 % K1_P_CB_SIZE_R_1)

// cache block offsets
#define K1_G_CB_OFFSET_R_1 0 // == (K1_G_CB_SIZE_R_1 * (0 + i_g_cb_R_1 * 1))
#define K1_L_CB_OFFSET_R_1 (K1_L_CB_SIZE_R_1 * (K1_G_FU_ID_R_1 + i_l_cb_r_1 * K1_G_NUM_FU_R_1))
#define K1_P_CB_OFFSET_R_1 (K1_P_CB_SIZE_R_1 * (K1_L_FU_ID_R_1 + i_p_cb_r_1 * K1_L_NUM_FU_R_1))


// -------------------- combined over dimensions --------------------

// flat WI ids
#define K1_G_FLAT_WI_ID (FLAT_INDEX_IN_DESCENDING_OCL_ORDER(get_global_id(OCL_DIM_R_1), get_global_size(OCL_DIM_R_1)))
#define K1_L_FLAT_WI_ID (FLAT_INDEX_IN_DESCENDING_OCL_ORDER(get_local_id(OCL_DIM_R_1), get_local_size(OCL_DIM_R_1)))
#define K1_P_FLAT_WI_ID (0)

// flat number of WIs
#define K1_G_FLAT_NUM_WI (K1_G_NUM_FU_R_1 * K1_L_NUM_FU_R_1)
#define K1_L_FLAT_NUM_WI (K1_L_NUM_FU_R_1)
#define K1_P_FLAT_NUM_WI (1)
// =============== end of macro definitions per dimension =====================

// =============== macro definitions per buffer ===============================
// -------------------- buffer ERROR --------------------

// buffer abstraction
#define BUFFER_ERROR_INDEX_0(i) i
#define BUFFER_ERROR_G_SIZE_0 K1_G_CB_SIZE_R_1
#define BUFFER_ERROR_L_SIZE_0 K1_L_CB_SIZE_R_1
#define BUFFER_ERROR_P_SIZE_0 K1_P_CB_SIZE_R_1
#define K1_G_BUFFER_ERROR(i) (value_index) {(list[(i)] == 1 ? -INFINITY : error[(i)]), (i), (list[(i)] == 2 ? INFINITY : error[(i)]), (i)}
#define K1_L_BUFFER_ERROR(i) cb_l_error[(BUFFER_ERROR_INDEX_0(i))]
#define K1_P_BUFFER_ERROR(i) cb_p_error[(BUFFER_ERROR_INDEX_0(i))]

// partitioning and cache usage
#define K1_G_MEM_ERROR(i) K1_G_BUFFER_ERROR(i)
#if CACHE_L_CB != 0
#define K1_L_MEM_ERROR(i) K1_L_BUFFER_ERROR(i)
#else
#define K1_L_MEM_ERROR(i) K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + (i))
#endif
#if CACHE_P_CB != 0
#define K1_P_MEM_ERROR(i) K1_P_BUFFER_ERROR(i)
#else
#define K1_P_MEM_ERROR(i) K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + (i))
#endif

// cache block sizes
#define K1_G_CB_SIZE_ERROR (K1_G_CB_SIZE_R_1)
#define K1_L_CB_SIZE_ERROR (K1_L_CB_SIZE_R_1)
#define K1_P_CB_SIZE_ERROR (K1_P_CB_SIZE_R_1)


// -------------------- result buffer --------------------

// check which levels are used
#if G_CB_RES_DEST_LEVEL == PRIVATE || L_CB_RES_DEST_LEVEL == PRIVATE || P_CB_RES_DEST_LEVEL == PRIVATE
#define K1_P_LEVEL_HAS_RESULTS
#endif
#if G_CB_RES_DEST_LEVEL == LOCAL || L_CB_RES_DEST_LEVEL == LOCAL || P_CB_RES_DEST_LEVEL == LOCAL
#define K1_L_LEVEL_HAS_RESULTS
#endif
#if G_CB_RES_DEST_LEVEL == GLOBAL || L_CB_RES_DEST_LEVEL == GLOBAL || P_CB_RES_DEST_LEVEL == GLOBAL
#define K1_G_LEVEL_HAS_RESULTS
#endif

// ------ PRIVATE ------
#ifdef K1_P_LEVEL_HAS_RESULTS
// construct suffix for res_p
#if   P_CB_RES_DEST_LEVEL == PRIVATE
#define K1_RES_P_BUFFER_SUFFIX_R_1(i) [K1_P_FU_ID_R_1 + (i)]
#define K1_RES_P_BUFFER_DEF_SUFFIX_R_1 [K1_P_NUM_FU_R_1]
#endif
// buffer abstraction for res_p
#define K1_RES_P_BUFFER_NAME() res_p
#define K1_RES_P_BUFFER_DEF K1_RES_P_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_P_BUFFER_DEF_SUFFIX_R_1)
#define K1_RES_P_BUFFER(i) K1_RES_P_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_P_BUFFER_SUFFIX_R_1(i))
#endif

// ------ LOCAL ------
#ifdef K1_L_LEVEL_HAS_RESULTS
// construct suffix for res_l
#if   P_CB_RES_DEST_LEVEL == LOCAL
#define K1_RES_L_BUFFER_SUFFIX_R_1(i, j) [K1_L_FU_ID_R_1 + (i)][K1_P_FU_ID_R_1 + (j)]
#define K1_RES_L_BUFFER_DEF_SUFFIX_R_1 [K1_L_NUM_FU_R_1][K1_P_NUM_FU_R_1]
#elif L_CB_RES_DEST_LEVEL == LOCAL
#define K1_RES_L_BUFFER_SUFFIX_R_1(i, j) [K1_L_FU_ID_R_1 + (i)]
#define K1_RES_L_BUFFER_DEF_SUFFIX_R_1 [K1_L_NUM_FU_R_1]
#endif
// buffer abstraction for res_l
#define K1_RES_L_BUFFER_NAME() res_l
#define K1_RES_L_BUFFER_DEF K1_RES_L_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_L_BUFFER_DEF_SUFFIX_R_1)
#define K1_RES_L_BUFFER(i, j) K1_RES_L_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_L_BUFFER_SUFFIX_R_1(i, j))
#endif

// ------ GLOBAL ------
#ifdef K1_G_LEVEL_HAS_RESULTS
// construct suffix for res_g
#if   P_CB_RES_DEST_LEVEL == GLOBAL
#define K1_RES_G_BUFFER_SUFFIX_R_1(i, j, k) (K1_G_FU_ID_R_1 + (i)) * K1_L_NUM_FU_R_1 * K1_P_NUM_FU_R_1 + (K1_L_FU_ID_R_1 + (j)) * K1_P_NUM_FU_R_1 + (K1_P_FU_ID_R_1 + (k))
#define K1_RES_G_BUFFER_DEF_SUFFIX_R_1 K1_G_NUM_FU_R_1 * K1_L_NUM_FU_R_1 * K1_P_NUM_FU_R_1
#elif L_CB_RES_DEST_LEVEL == GLOBAL
#define K1_RES_G_BUFFER_SUFFIX_R_1(i, j, k) (K1_G_FU_ID_R_1 + (i)) * K1_L_NUM_FU_R_1 + (K1_L_FU_ID_R_1 + (j))
#define K1_RES_G_BUFFER_DEF_SUFFIX_R_1 K1_G_NUM_FU_R_1 * K1_L_NUM_FU_R_1
#elif G_CB_RES_DEST_LEVEL == GLOBAL
#define K1_RES_G_BUFFER_SUFFIX_R_1(i, j, k) (K1_G_FU_ID_R_1 + (i))
#define K1_RES_G_BUFFER_DEF_SUFFIX_R_1 K1_G_NUM_FU_R_1
#endif
// buffer abstraction for res_g
#if (L_CB_RES_DEST_LEVEL == GLOBAL && K1_L_NUM_FU_R_1 > 1) || (P_CB_RES_DEST_LEVEL == GLOBAL && K1_P_NUM_FU_R_1 > 1)
#define K1_RES_G_BUFFER_NAME() res_g
#define K1_RES_G_BUFFER(i, j, k) K1_RES_G_BUFFER_NAME()[FLAT_INDEX_IN_DESCENDING_OCL_ORDER(K1_RES_G_BUFFER_SUFFIX_R_1(i, j, k), K1_RES_G_BUFFER_DEF_SUFFIX_R_1)]
#else
#define K1_RES_G_BUFFER_NAME() int_res
#if K1_G_NUM_FU_R_1 == 1
// if result is final result, use requested ordering of L-dimensions
#define K1_RES_G_BUFFER(i, j, k) K1_RES_G_BUFFER_NAME()[0]
#else
// else order in descending OpenCL dimension order
#define K1_RES_G_BUFFER(i, j, k) K1_RES_G_BUFFER_NAME()[FLAT_INDEX_IN_DESCENDING_OCL_ORDER(K1_RES_G_BUFFER_SUFFIX_R_1(i, j, k), K1_RES_G_BUFFER_DEF_SUFFIX_R_1)]
#endif
#endif
#endif

// determine memory destination for results
#if   P_CB_RES_DEST_LEVEL == PRIVATE
#define K1_P_CB_RES_DEST() K1_RES_P_BUFFER(0)
#elif P_CB_RES_DEST_LEVEL == LOCAL
#define K1_P_CB_RES_DEST() K1_RES_L_BUFFER(0, 0)
#elif P_CB_RES_DEST_LEVEL == GLOBAL
#define K1_P_CB_RES_DEST() K1_RES_G_BUFFER(0, 0, 0)
#endif

#if   L_CB_RES_DEST_LEVEL == PRIVATE
#define K1_L_CB_RES_DEST() K1_RES_P_BUFFER(0)
#elif L_CB_RES_DEST_LEVEL == LOCAL
#define K1_L_CB_RES_DEST() K1_RES_L_BUFFER(0, 0)
#elif L_CB_RES_DEST_LEVEL == GLOBAL
#define K1_L_CB_RES_DEST() K1_RES_G_BUFFER(0, 0, 0)
#endif

#if   G_CB_RES_DEST_LEVEL == PRIVATE
#define K1_G_CB_RES_DEST() K1_RES_P_BUFFER(0)
#elif G_CB_RES_DEST_LEVEL == LOCAL
#define K1_G_CB_RES_DEST() K1_RES_L_BUFFER(0, 0)
#elif G_CB_RES_DEST_LEVEL == GLOBAL
#define K1_G_CB_RES_DEST() K1_RES_G_BUFFER(0, 0, 0)
#endif

// check if seperate memory for reduction is needed
// will only be checked for local level, because of constraints for OpenCL
#if L_CB_RES_DEST_LEVEL < LOCAL
#define K1_L_REDUCTION_MEM_NAME() l_reduction_mem
#define K1_L_REDUCTION_MEM(i) K1_L_REDUCTION_MEM_NAME()CONCAT_IN_DESCENDING_OCL_ORDER([K1_L_FU_ID_R_1 + (i)])
#endif

// buffer abstraction for kernel_res buffer
#if K1_G_NUM_FU_R_1 == 1
// if result is final result, use requested ordering of L-dimensions
#define K1_KERNEL_RES_BUFFER() int_res[0]
#else
// else order in descending OpenCL dimension order
#define K1_KERNEL_RES_BUFFER() int_res[FLAT_INDEX_IN_DESCENDING_OCL_ORDER(K1_G_FU_ID_R_1, K1_G_NUM_FU_R_1)]
#endif

#define K1_G_KERNEL_RES() K1_KERNEL_RES_BUFFER()
#define K1_L_KERNEL_RES() K1_G_KERNEL_RES()
#define K1_P_KERNEL_RES() K1_L_KERNEL_RES()
// =============== end of macro definitions per buffer ========================

// =============== scalar function ============================================
inline TYPE_TS f(const TYPE_T error_val) {
  return error_val;
}
// =============== end of scalar function =====================================

// =============== kernel 1 ===================================================
__kernel void svm_update_bias(
        __global float const * const restrict error,
        __global char const * const restrict list,
        __global TYPE_TS * const restrict res_g,
        __global TYPE_TS * const restrict int_res) {
  // map md_hom dimensions to OpenCL dimensions
  const size_t i_wg_r_1 = get_group_id(OCL_DIM_R_1);
  const size_t i_wi_r_1 = get_local_id(OCL_DIM_R_1);

  // declare variables for caching inputs
  #if CACHE_L_CB != 0
  __local TYPE_T cb_l_error[BUFFER_ERROR_L_SIZE_0];
  #endif
  #if CACHE_P_CB != 0
  __private TYPE_T cb_p_error[BUFFER_ERROR_P_SIZE_0];
  #endif

  // declare variables for result memory
  // ------ LOCAL ------
  #ifdef K1_L_LEVEL_HAS_RESULTS
  __local TYPE_TS K1_RES_L_BUFFER_DEF;
  #endif
  // ------ PRIVATE ------
  #ifdef K1_P_LEVEL_HAS_RESULTS
  __private TYPE_TS K1_RES_P_BUFFER_DEF;
  #endif
  
  // declare variables for reduction memory (if needed)
  #if L_CB_RES_DEST_LEVEL < LOCAL && (K1_L_NUM_FU_R_1 > 1)
  __local TYPE_TS K1_L_REDUCTION_MEM_NAME()CONCAT_IN_DESCENDING_OCL_ORDER([K1_L_NUM_FU_R_1]);
  #endif

  // reset memory where reduction takes place for WIs that do not
  // calculate intermediate results, so that reduction can always
  // be done with all WIs
  #if K1_L_NUM_FU_R_1 > 1
  #if K1_L_NUM_CB_R_1 == 0 && (K1_L_NUM_EXTRA_CB_R_1 + (K1_L_NUM_EXTRA_ELEMS_R_1 > 0)) < K1_G_NUM_FU_R_1
  if (K1_G_FU_ID_R_1 < (K1_L_NUM_EXTRA_CB_R_1 + (K1_L_NUM_EXTRA_ELEMS_R_1 > 0)))
  #endif
  {
    #if K1_L_NUM_CB_R_1 > 0
    // all active WGs process whole local cache blocks
    #if K1_P_NUM_CB_R_1 > 0
    const size_t num_active_wi_r_1 = K1_L_NUM_FU_R_1;
    #else
    const size_t num_active_wi_r_1 = K1_P_NUM_EXTRA_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_R_1 > 0);
    #endif
    #else
    // not all active WGs process whole cache blocks
    size_t num_active_wi_r_1;
    if (K1_G_FU_ID_R_1 < K1_L_NUM_EXTRA_CB_R_1) {
      #if K1_P_NUM_CB_R_1 > 0
      num_active_wi_r_1 = K1_L_NUM_FU_R_1;
      #else
      num_active_wi_r_1 = K1_P_NUM_EXTRA_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_R_1 > 0);
      #endif
    } else {
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0
      num_active_wi_r_1 = K1_L_NUM_FU_R_1;
      #else
      num_active_wi_r_1 = K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0);
      #endif
    }
    #endif
    
    if (K1_L_FU_ID_R_1 >= num_active_wi_r_1) {
      K1_L_CB_RES_DEST() = (value_index) {-INFINITY, 0, INFINITY, 0};
    }
  }
  #endif

  #if K1_L_NUM_CB_R_1 > 0
  size_t i_l_cb_r_1 = 0;
  // ---------- L caching --------------------
  #if CACHE_L_CB != 0
  #if K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI > 0
  for (size_t step = 0; step < K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI; ++step) {
    const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
    const size_t i_l_elem_r_1 = index;
    K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
  }
  #endif
  #if K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI > 0
  if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI) {
    const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
    const size_t i_l_elem_r_1 = index;
    K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
  }
  #endif
  barrier(CLK_LOCAL_MEM_FENCE);
  #endif
  // ---------- end of L caching -------------
  
  #if K1_P_NUM_CB_R_1 > 0
  size_t i_p_cb_r_1 = 0;
  // ---------- P caching --------------------
  #if CACHE_P_CB != 0
  #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
  for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
    const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
    const size_t i_p_elem_r_1 = index;
    K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
  }
  #endif
  #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
  if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
    const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
    const size_t i_p_elem_r_1 = index;
    K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
  }
  #endif
  #endif
  // ---------- end of P caching -------------
  
  size_t i_p_elem_r_1 = 0;
  // process one mda element
  K1_P_CB_RES_DEST() = f(
      K1_P_MEM_ERROR(i_p_elem_r_1)
  );
  
  #if K1_P_CB_SIZE_R_1 > 1
  for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
    // process one mda element
    K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
        K1_P_MEM_ERROR(i_p_elem_r_1)
    ));
  
  }
  #endif
  
  #if K1_P_NUM_CB_R_1 > 1
  for (i_p_cb_r_1 = 1; i_p_cb_r_1 < K1_P_NUM_CB_R_1; ++i_p_cb_r_1) {
    // ---------- P caching --------------------
    #if CACHE_P_CB != 0
    #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
      const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
    if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
      const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #endif
    // ---------- end of P caching -------------
    
    for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
  } // end of "i_p_cb_r_1"-loop
  #endif
  #endif
  // post process whole extra cache blocks in dimension R_1
  #if K1_P_NUM_EXTRA_CB_R_1 > 0
  if (K1_L_FU_ID_R_1 < K1_P_NUM_EXTRA_CB_R_1) {  
    const size_t i_p_cb_r_1 = K1_P_NUM_CB_R_1;
    
    #if K1_P_NUM_CB_R_1 == 0
    // ---------- P caching --------------------
    #if CACHE_P_CB != 0
    #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
      const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
    if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
      const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #endif
    // ---------- end of P caching -------------
    
    size_t i_p_elem_r_1 = 0;
    // process one mda element
    K1_P_CB_RES_DEST() = f(
        K1_P_MEM_ERROR(i_p_elem_r_1)
    );
    
    #if K1_P_CB_SIZE_R_1 > 1
    for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
    #endif
    
    #else
    // ---------- P caching --------------------
    #if CACHE_P_CB != 0
    #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
      const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
    if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
      const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #endif
    // ---------- end of P caching -------------
    
    for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }  
    #endif
  } // end of post processing whole extra cache blocks in dimension R_1
  #endif
  // post process single extra incomplete cache block in dimension R_1
  #if K1_P_NUM_EXTRA_ELEMS_R_1 > 0
  if (K1_L_FU_ID_R_1 == K1_P_NUM_EXTRA_CB_R_1 % K1_L_NUM_FU_R_1) {  
    const size_t i_p_cb_r_1 = K1_P_NUM_CB_R_1;
    
    #if K1_P_NUM_CB_R_1 == 0
    // ---------- P caching --------------------
    #if CACHE_P_CB != 0
    #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI > 0
    for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
      const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI > 0
    if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI) {
      const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #endif
    // ---------- end of P caching -------------
    
    size_t i_p_elem_r_1 = 0;
    // process one mda element
    K1_P_CB_RES_DEST() = f(
        K1_P_MEM_ERROR(i_p_elem_r_1)
    );
    
    #if K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1 > 1
    for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
    #endif
    
    #else
    // ---------- P caching --------------------
    #if CACHE_P_CB != 0
    #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI > 0
    for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
      const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI > 0
    if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI) {
      const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #endif
    // ---------- end of P caching -------------
    
    for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }  
    #endif
  }// end of post process single extra incomplete cache block in dimension R_1
  #endif
  
  // ---------- reduction --------------------
  // will never be necessary on this level
  // because K1_P_NUM_FU_R == 1 for OpenCL
  // ---------- end of reduction -------------
  
  // move results upwards in memory hierarchy if not already done by reduction
  #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL && (K1_P_NUM_FU_R_1 == 1 || P_CB_RES_DEST_LEVEL >= PRIVATE)
  #if K1_P_NUM_CB_R_1 == 0 && (K1_P_NUM_EXTRA_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_R_1 > 0)) < K1_L_NUM_FU_R_1
  if (K1_L_FU_ID_R_1 < (K1_P_NUM_EXTRA_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_R_1 > 0)))
  #endif
  {
    K1_L_CB_RES_DEST() = K1_P_CB_RES_DEST();
  }
  #endif
  
  // wait for all WIs to finish computation on local cache block
  #if CACHE_L_CB != 0
  barrier(CLK_LOCAL_MEM_FENCE);
  #endif
  #if K1_L_NUM_CB_R_1 > 1
  for (i_l_cb_r_1 = 1; i_l_cb_r_1 < K1_L_NUM_CB_R_1; ++i_l_cb_r_1) {
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    #if K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_r_1 = index;
      K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
    }
    #endif
    #if K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_r_1 = index;
      K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
    }
    #endif
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------
    
    #if K1_P_NUM_CB_R_1 > 0
    size_t i_p_cb_r_1 = 0;
    // ---------- P caching --------------------
    #if CACHE_P_CB != 0
    #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
      const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
    if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
      const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #endif
    // ---------- end of P caching -------------
    
    #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
    size_t i_p_elem_r_1 = 0;
    // process one mda element
    K1_P_CB_RES_DEST() = f(
        K1_P_MEM_ERROR(i_p_elem_r_1)
    );
    
    #if K1_P_CB_SIZE_R_1 > 1
    for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
    #endif
    
    #else
    for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
    #endif
    #if K1_P_NUM_CB_R_1 > 1
    for (i_p_cb_r_1 = 1; i_p_cb_r_1 < K1_P_NUM_CB_R_1; ++i_p_cb_r_1) {
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
    } // end of "i_p_cb_r_1"-loop
    #endif
    #endif
    // post process whole extra cache blocks in dimension R_1
    #if K1_P_NUM_EXTRA_CB_R_1 > 0
    if (K1_L_FU_ID_R_1 < K1_P_NUM_EXTRA_CB_R_1) {  
      const size_t i_p_cb_r_1 = K1_P_NUM_CB_R_1;
      
      #if K1_P_NUM_CB_R_1 == 0
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      size_t i_p_elem_r_1 = 0;
      // process one mda element
      K1_P_CB_RES_DEST() = f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      );
      
      #if K1_P_CB_SIZE_R_1 > 1
      for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif
      
      #else
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif  
      #else
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }  
      #endif
    } // end of post processing whole extra cache blocks in dimension R_1
    #endif
    // post process single extra incomplete cache block in dimension R_1
    #if K1_P_NUM_EXTRA_ELEMS_R_1 > 0
    if (K1_L_FU_ID_R_1 == K1_P_NUM_EXTRA_CB_R_1 % K1_L_NUM_FU_R_1) {  
      const size_t i_p_cb_r_1 = K1_P_NUM_CB_R_1;
      
      #if K1_P_NUM_CB_R_1 == 0
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      size_t i_p_elem_r_1 = 0;
      // process one mda element
      K1_P_CB_RES_DEST() = f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      );
      
      #if K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1 > 1
      for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif
      
      #else
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif  
      #else
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }  
      #endif
    }// end of post process single extra incomplete cache block in dimension R_1
    #endif
    
    // ---------- reduction --------------------
    // will never be necessary on this level
    // because K1_P_NUM_FU_R == 1 for OpenCL
    // ---------- end of reduction -------------
    
    // move results upwards in memory hierarchy if not already done by reduction
    #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL && (K1_P_NUM_FU_R_1 == 1 || P_CB_RES_DEST_LEVEL >= PRIVATE)
    #if K1_P_NUM_CB_R_1 == 0 && (K1_P_NUM_EXTRA_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_R_1 > 0)) < K1_L_NUM_FU_R_1
    if (K1_L_FU_ID_R_1 < (K1_P_NUM_EXTRA_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_R_1 > 0)))
    #endif
    {
      K1_L_CB_RES_DEST() = min_max_op(K1_L_CB_RES_DEST(), K1_P_CB_RES_DEST());
    }
    #endif
    
    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
  } // end of "i_l_cb_r_1"-loop
  #endif
  #endif
  // post process whole extra cache blocks in dimension R_1
  #if K1_L_NUM_EXTRA_CB_R_1 > 0
  if (K1_G_FU_ID_R_1 < K1_L_NUM_EXTRA_CB_R_1) {  
    const size_t i_l_cb_r_1 = K1_L_NUM_CB_R_1;
    
    #if K1_L_NUM_CB_R_1 == 0
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    #if K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_r_1 = index;
      K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
    }
    #endif
    #if K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_r_1 = index;
      K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
    }
    #endif
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------
    
    #if K1_P_NUM_CB_R_1 > 0
    size_t i_p_cb_r_1 = 0;
    // ---------- P caching --------------------
    #if CACHE_P_CB != 0
    #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
      const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
    if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
      const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #endif
    // ---------- end of P caching -------------
    
    size_t i_p_elem_r_1 = 0;
    // process one mda element
    K1_P_CB_RES_DEST() = f(
        K1_P_MEM_ERROR(i_p_elem_r_1)
    );
    
    #if K1_P_CB_SIZE_R_1 > 1
    for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
    #endif
    
    #if K1_P_NUM_CB_R_1 > 1
    for (i_p_cb_r_1 = 1; i_p_cb_r_1 < K1_P_NUM_CB_R_1; ++i_p_cb_r_1) {
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
    } // end of "i_p_cb_r_1"-loop
    #endif
    #endif
    // post process whole extra cache blocks in dimension R_1
    #if K1_P_NUM_EXTRA_CB_R_1 > 0
    if (K1_L_FU_ID_R_1 < K1_P_NUM_EXTRA_CB_R_1) {  
      const size_t i_p_cb_r_1 = K1_P_NUM_CB_R_1;
      
      #if K1_P_NUM_CB_R_1 == 0
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      size_t i_p_elem_r_1 = 0;
      // process one mda element
      K1_P_CB_RES_DEST() = f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      );
      
      #if K1_P_CB_SIZE_R_1 > 1
      for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif
      
      #else
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }  
      #endif
    } // end of post processing whole extra cache blocks in dimension R_1
    #endif
    // post process single extra incomplete cache block in dimension R_1
    #if K1_P_NUM_EXTRA_ELEMS_R_1 > 0
    if (K1_L_FU_ID_R_1 == K1_P_NUM_EXTRA_CB_R_1 % K1_L_NUM_FU_R_1) {  
      const size_t i_p_cb_r_1 = K1_P_NUM_CB_R_1;
      
      #if K1_P_NUM_CB_R_1 == 0
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      size_t i_p_elem_r_1 = 0;
      // process one mda element
      K1_P_CB_RES_DEST() = f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      );
      
      #if K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1 > 1
      for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif
      
      #else
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }  
      #endif
    }// end of post process single extra incomplete cache block in dimension R_1
    #endif
    
    // ---------- reduction --------------------
    // will never be necessary on this level
    // because K1_P_NUM_FU_R == 1 for OpenCL
    // ---------- end of reduction -------------
    
    // move results upwards in memory hierarchy if not already done by reduction
    #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL && (K1_P_NUM_FU_R_1 == 1 || P_CB_RES_DEST_LEVEL >= PRIVATE)
    #if K1_P_NUM_CB_R_1 == 0 && (K1_P_NUM_EXTRA_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_R_1 > 0)) < K1_L_NUM_FU_R_1
    if (K1_L_FU_ID_R_1 < (K1_P_NUM_EXTRA_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_R_1 > 0)))
    #endif
    {
      K1_L_CB_RES_DEST() = K1_P_CB_RES_DEST();
    }
    #endif
    
    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif  
    #else
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    #if K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_r_1 = index;
      K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
    }
    #endif
    #if K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_ERROR % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_ERROR / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_r_1 = index;
      K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
    }
    #endif
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------
    
    #if K1_P_NUM_CB_R_1 > 0
    size_t i_p_cb_r_1 = 0;
    // ---------- P caching --------------------
    #if CACHE_P_CB != 0
    #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
      const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
    if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
      const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #endif
    // ---------- end of P caching -------------
    
    #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
    size_t i_p_elem_r_1 = 0;
    // process one mda element
    K1_P_CB_RES_DEST() = f(
        K1_P_MEM_ERROR(i_p_elem_r_1)
    );
    
    #if K1_P_CB_SIZE_R_1 > 1
    for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
    #endif
    
    #else
    for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
    #endif
    #if K1_P_NUM_CB_R_1 > 1
    for (i_p_cb_r_1 = 1; i_p_cb_r_1 < K1_P_NUM_CB_R_1; ++i_p_cb_r_1) {
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
    } // end of "i_p_cb_r_1"-loop
    #endif
    #endif
    // post process whole extra cache blocks in dimension R_1
    #if K1_P_NUM_EXTRA_CB_R_1 > 0
    if (K1_L_FU_ID_R_1 < K1_P_NUM_EXTRA_CB_R_1) {  
      const size_t i_p_cb_r_1 = K1_P_NUM_CB_R_1;
      
      #if K1_P_NUM_CB_R_1 == 0
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      size_t i_p_elem_r_1 = 0;
      // process one mda element
      K1_P_CB_RES_DEST() = f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      );
      
      #if K1_P_CB_SIZE_R_1 > 1
      for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif
      
      #else
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif  
      #else
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }  
      #endif
    } // end of post processing whole extra cache blocks in dimension R_1
    #endif
    // post process single extra incomplete cache block in dimension R_1
    #if K1_P_NUM_EXTRA_ELEMS_R_1 > 0
    if (K1_L_FU_ID_R_1 == K1_P_NUM_EXTRA_CB_R_1 % K1_L_NUM_FU_R_1) {  
      const size_t i_p_cb_r_1 = K1_P_NUM_CB_R_1;
      
      #if K1_P_NUM_CB_R_1 == 0
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      size_t i_p_elem_r_1 = 0;
      // process one mda element
      K1_P_CB_RES_DEST() = f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      );
      
      #if K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1 > 1
      for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif
      
      #else
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif  
      #else
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }  
      #endif
    }// end of post process single extra incomplete cache block in dimension R_1
    #endif
    
    // ---------- reduction --------------------
    // will never be necessary on this level
    // because K1_P_NUM_FU_R == 1 for OpenCL
    // ---------- end of reduction -------------
    
    // move results upwards in memory hierarchy if not already done by reduction
    #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL && (K1_P_NUM_FU_R_1 == 1 || P_CB_RES_DEST_LEVEL >= PRIVATE)
    #if K1_P_NUM_CB_R_1 == 0 && (K1_P_NUM_EXTRA_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_R_1 > 0)) < K1_L_NUM_FU_R_1
    if (K1_L_FU_ID_R_1 < (K1_P_NUM_EXTRA_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_R_1 > 0)))
    #endif
    {
      K1_L_CB_RES_DEST() = min_max_op(K1_L_CB_RES_DEST(), K1_P_CB_RES_DEST());
    }
    #endif
    
    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif  
    #endif
  } // end of post processing whole extra cache blocks in dimension R_1
  #endif
  // post process single extra incomplete cache block in dimension R_1
  #if K1_L_NUM_EXTRA_ELEMS_R_1 > 0
  if (K1_G_FU_ID_R_1 == K1_L_NUM_EXTRA_CB_R_1 % K1_G_NUM_FU_R_1) {  
    const size_t i_l_cb_r_1 = K1_L_NUM_CB_R_1;
    
    #if K1_L_NUM_CB_R_1 == 0
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    #if (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_R_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_R_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_r_1 = index;
      K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
    }
    #endif
    #if (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_R_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_R_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_R_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_r_1 = index;
      K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
    }
    #endif
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------
    
    #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0
    size_t i_p_cb_r_1 = 0;
    // ---------- P caching --------------------
    #if CACHE_P_CB != 0
    #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
      const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
    if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
      const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #endif
    // ---------- end of P caching -------------
    
    size_t i_p_elem_r_1 = 0;
    // process one mda element
    K1_P_CB_RES_DEST() = f(
        K1_P_MEM_ERROR(i_p_elem_r_1)
    );
    
    #if K1_P_CB_SIZE_R_1 > 1
    for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
    #endif
    
    #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 1
    for (i_p_cb_r_1 = 1; i_p_cb_r_1 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1; ++i_p_cb_r_1) {
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
    } // end of "i_p_cb_r_1"-loop
    #endif
    #endif
    // post process whole extra cache blocks in dimension R_1
    #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0
    if (K1_L_FU_ID_R_1 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1) {  
      const size_t i_p_cb_r_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1;
      
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 == 0
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      size_t i_p_elem_r_1 = 0;
      // process one mda element
      K1_P_CB_RES_DEST() = f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      );
      
      #if K1_P_CB_SIZE_R_1 > 1
      for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif
      
      #else
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }  
      #endif
    } // end of post processing whole extra cache blocks in dimension R_1
    #endif
    // post process single extra incomplete cache block in dimension R_1
    #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0
    if (K1_L_FU_ID_R_1 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 % K1_L_NUM_FU_R_1) {  
      const size_t i_p_cb_r_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1;
      
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 == 0
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      size_t i_p_elem_r_1 = 0;
      // process one mda element
      K1_P_CB_RES_DEST() = f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      );
      
      #if K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1 > 1
      for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif
      
      #else
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }  
      #endif
    }// end of post process single extra incomplete cache block in dimension R_1
    #endif
    
    // ---------- reduction --------------------
    // will never be necessary on this level
    // because K1_P_NUM_FU_R == 1 for OpenCL
    // ---------- end of reduction -------------
    
    // move results upwards in memory hierarchy if not already done by reduction
    #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL && (K1_P_NUM_FU_R_1 == 1 || P_CB_RES_DEST_LEVEL >= PRIVATE)
    #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 == 0 && (K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0)) < K1_L_NUM_FU_R_1
    if (K1_L_FU_ID_R_1 < (K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0)))
    #endif
    {
      K1_L_CB_RES_DEST() = K1_P_CB_RES_DEST();
    }
    #endif
    
    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif  
    #else
    // ---------- L caching --------------------
    #if CACHE_L_CB != 0
    #if (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_R_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_L_FLAT_NUM_WI > 0
    for (size_t step = 0; step < (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_R_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_L_FLAT_NUM_WI; ++step) {
      const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_r_1 = index;
      K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
    }
    #endif
    #if (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_R_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_L_FLAT_NUM_WI > 0
    if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_R_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_L_FLAT_NUM_WI) {
      const size_t index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_ERROR / K1_L_CB_SIZE_R_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
      const size_t i_l_elem_r_1 = index;
      K1_L_MEM_ERROR(i_l_elem_r_1) = K1_G_MEM_ERROR(K1_L_CB_OFFSET_R_1 + i_l_elem_r_1);
    }
    #endif
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    // ---------- end of L caching -------------
    
    #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0
    size_t i_p_cb_r_1 = 0;
    // ---------- P caching --------------------
    #if CACHE_P_CB != 0
    #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
    for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
      const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
    if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
      const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
      const size_t i_p_elem_r_1 = index;
      K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
    }
    #endif
    #endif
    // ---------- end of P caching -------------
    
    #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
    size_t i_p_elem_r_1 = 0;
    // process one mda element
    K1_P_CB_RES_DEST() = f(
        K1_P_MEM_ERROR(i_p_elem_r_1)
    );
    
    #if K1_P_CB_SIZE_R_1 > 1
    for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
    #endif
    
    #else
    for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
      // process one mda element
      K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      ));
    
    }
    #endif
    #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 1
    for (i_p_cb_r_1 = 1; i_p_cb_r_1 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1; ++i_p_cb_r_1) {
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
    } // end of "i_p_cb_r_1"-loop
    #endif
    #endif
    // post process whole extra cache blocks in dimension R_1
    #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0
    if (K1_L_FU_ID_R_1 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1) {  
      const size_t i_p_cb_r_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1;
      
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 == 0
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      size_t i_p_elem_r_1 = 0;
      // process one mda element
      K1_P_CB_RES_DEST() = f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      );
      
      #if K1_P_CB_SIZE_R_1 > 1
      for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif
      
      #else
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif  
      #else
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_ERROR % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_ERROR / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_SIZE_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }  
      #endif
    } // end of post processing whole extra cache blocks in dimension R_1
    #endif
    // post process single extra incomplete cache block in dimension R_1
    #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0
    if (K1_L_FU_ID_R_1 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 % K1_L_NUM_FU_R_1) {  
      const size_t i_p_cb_r_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1;
      
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 == 0
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
      size_t i_p_elem_r_1 = 0;
      // process one mda element
      K1_P_CB_RES_DEST() = f(
          K1_P_MEM_ERROR(i_p_elem_r_1)
      );
      
      #if K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1 > 1
      for (i_p_elem_r_1 = 1; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif
      
      #else
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }
      #endif  
      #else
      // ---------- P caching --------------------
      #if CACHE_P_CB != 0
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI; ++step) {
        const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #if (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_P_FLAT_NUM_WI > 0
      if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) % K1_P_FLAT_NUM_WI) {
        const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_ERROR / K1_P_CB_SIZE_R_1 * K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
        const size_t i_p_elem_r_1 = index;
        K1_P_MEM_ERROR(i_p_elem_r_1) = K1_L_MEM_ERROR(K1_P_CB_OFFSET_R_1 + i_p_elem_r_1);
      }
      #endif
      #endif
      // ---------- end of P caching -------------
      
      for (size_t i_p_elem_r_1 = 0; i_p_elem_r_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_R_1; ++i_p_elem_r_1) {
        // process one mda element
        K1_P_CB_RES_DEST() = min_max_op(K1_P_CB_RES_DEST(), f(
            K1_P_MEM_ERROR(i_p_elem_r_1)
        ));
      
      }  
      #endif
    }// end of post process single extra incomplete cache block in dimension R_1
    #endif
    
    // ---------- reduction --------------------
    // will never be necessary on this level
    // because K1_P_NUM_FU_R == 1 for OpenCL
    // ---------- end of reduction -------------
    
    // move results upwards in memory hierarchy if not already done by reduction
    #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL && (K1_P_NUM_FU_R_1 == 1 || P_CB_RES_DEST_LEVEL >= PRIVATE)
    #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 == 0 && (K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0)) < K1_L_NUM_FU_R_1
    if (K1_L_FU_ID_R_1 < (K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 + (K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_R_1 > 0)))
    #endif
    {
      K1_L_CB_RES_DEST() = min_max_op(K1_L_CB_RES_DEST(), K1_P_CB_RES_DEST());
    }
    #endif
    
    // wait for all WIs to finish computation on local cache block
    #if CACHE_L_CB != 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif  
    #endif
  }// end of post process single extra incomplete cache block in dimension R_1
  #endif
  
  #if K1_L_NUM_FU_R_1 > 1
  #if K1_L_NUM_CB_R_1 == 0 && (K1_L_NUM_EXTRA_CB_R_1 + (K1_L_NUM_EXTRA_ELEMS_R_1 > 0)) < K1_G_NUM_FU_R_1
  if (K1_G_FU_ID_R_1 < (K1_L_NUM_EXTRA_CB_R_1 + (K1_L_NUM_EXTRA_ELEMS_R_1 > 0)))
  #endif
  {
    // wait for all FUs to finish computation
    #if   L_CB_RES_DEST_LEVEL == LOCAL && CACHE_L_CB == 0
    barrier(CLK_LOCAL_MEM_FENCE);
    #elif L_CB_RES_DEST_LEVEL == GLOBAL
    barrier(CLK_GLOBAL_MEM_FENCE);
    #endif
    #if L_CB_RES_DEST_LEVEL < LOCAL
    K1_L_REDUCTION_MEM(0) = K1_L_CB_RES_DEST();
    // wait for all values to be copied into reduction memory
    barrier(CLK_LOCAL_MEM_FENCE);
    #endif
    #if (K1_L_NUM_FU_R_1 & (K1_L_NUM_FU_R_1 - 1)) != 0
    // pre processing: reduce number of values to largest possible power of 2
    size_t stride = pow(2.0f, floor(log2((float) K1_L_NUM_FU_R_1)));
    if (K1_L_FU_ID_R_1 < stride && K1_L_FU_ID_R_1 + stride < K1_L_NUM_FU_R_1) {
      #if L_CB_RES_DEST_LEVEL < LOCAL
      // reduction in separate memory location
      K1_L_REDUCTION_MEM(0) = min_max_op(K1_L_REDUCTION_MEM(0), K1_L_REDUCTION_MEM(stride));
      #elif L_CB_RES_DEST_LEVEL == LOCAL
      K1_RES_L_BUFFER(0, 0) = min_max_op(K1_RES_L_BUFFER(0, 0), K1_RES_L_BUFFER(stride, 0));
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      K1_RES_G_BUFFER(0, 0, 0) = min_max_op(K1_RES_G_BUFFER(0, 0, 0), K1_RES_G_BUFFER(0, stride, 0));
      #endif
    }
    stride /= 2;
    #if L_CB_RES_DEST_LEVEL <= LOCAL
    barrier(CLK_LOCAL_MEM_FENCE);
    #elif L_CB_RES_DEST_LEVEL == GLOBAL
    barrier(CLK_GLOBAL_MEM_FENCE);
    #endif
    #else
    size_t stride = K1_L_NUM_FU_R_1 / 2;
    #endif
    // perform reduction except for last step
    #if K1_L_NUM_FU_R_1 > 3
    for (; stride > 1; stride /= 2) {
      if (K1_L_FU_ID_R_1 < stride) {
        #if L_CB_RES_DEST_LEVEL < LOCAL
        // reduction in separate memory location
        K1_L_REDUCTION_MEM(0) = min_max_op(K1_L_REDUCTION_MEM(0), K1_L_REDUCTION_MEM(stride));
        #elif L_CB_RES_DEST_LEVEL == LOCAL
        K1_RES_L_BUFFER(0, 0) = min_max_op(K1_RES_L_BUFFER(0, 0), K1_RES_L_BUFFER(stride, 0));
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
        K1_RES_G_BUFFER(0, 0, 0) = min_max_op(K1_RES_G_BUFFER(0, 0, 0), K1_RES_G_BUFFER(0, stride, 0));
        #endif
      }
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
    }
    #endif
    // do last reduction step separately to prevent unnecessary copying
    if (K1_L_FU_ID_R_1 < stride) {
      #if L_CB_RES_DEST_LEVEL < LOCAL
      // reduction in separate memory location
      K1_G_CB_RES_DEST() = min_max_op(K1_L_REDUCTION_MEM(0), K1_L_REDUCTION_MEM(stride));
      #elif L_CB_RES_DEST_LEVEL == LOCAL
      K1_RES_L_BUFFER(0, 0) = min_max_op(K1_RES_L_BUFFER(0, 0), K1_RES_L_BUFFER(stride, 0));
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      K1_P_KERNEL_RES() = min_max_op(K1_RES_G_BUFFER(0, 0, 0), K1_RES_G_BUFFER(0, stride, 0));
      #endif
    }
    #if L_CB_RES_DEST_LEVEL <= LOCAL
    barrier(CLK_LOCAL_MEM_FENCE);
    #elif L_CB_RES_DEST_LEVEL == GLOBAL
    barrier(CLK_GLOBAL_MEM_FENCE);
    #endif
  }
  #endif
  
  // move results upwards in memory hierarchy if not already done by reduction
  #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL && (K1_L_NUM_FU_R_1 == 1 || L_CB_RES_DEST_LEVEL >= LOCAL)
  #if K1_L_NUM_CB_R_1 == 0 && (K1_L_NUM_EXTRA_CB_R_1 + (K1_L_NUM_EXTRA_ELEMS_R_1 > 0)) < K1_G_NUM_FU_R_1
  if (K1_L_FU_ID_R_1 == 0 && K1_G_FU_ID_R_1 < (K1_L_NUM_EXTRA_CB_R_1 + (K1_L_NUM_EXTRA_ELEMS_R_1 > 0))) {
  #else
  if (K1_L_FU_ID_R_1 == 0) {
  #endif
    K1_G_CB_RES_DEST() = K1_L_CB_RES_DEST();
  }
  #endif
}
// =============== end of kernel 1 ============================================