package tasks.training;

import org.lwjgl.opencl.CL10;
import tasks.routines.Backward;
import tasks.routines.Forward;
import tasks.routines.WeightUpdate;
import model.InputData;
import opencl.Kernel;
import opencl.OCLUtil;
import org.lwjgl.BufferUtils;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.util.Random;

public abstract class AbstractTraining {
    protected Random random;
    protected InputData input;

    // parameter of network
    protected int[] layer;
    protected float learningRate;
    protected float momentum;

    // pointer to gpu memory
    protected GpuTrainingPtr gpuPtr;

    // routines
    protected Forward forward;
    protected Backward backward;
    protected WeightUpdate weightUpdate;

    protected AbstractTraining(InputData input, int[] layer, float learningRate, float momentum, int seed) {
        this.input = input;
        this.layer = layer;
        this.learningRate = learningRate;
        this.momentum = momentum;
        this.random = new Random(seed);

        this.gpuPtr = new GpuTrainingPtr(layer);
    }

    public abstract void train(int epoch);

    protected void initializeWeights(int modelCount) {
        for(int i = 0; i < layer.length - 1; i++) {

            FloatBuffer weightsBuffer = BufferUtils.createFloatBuffer(modelCount * layer[i] * layer[i + 1]);
            for(int j = 0; j < modelCount * layer[i] * layer[i + 1]; j++) {
                //weightsBuffer.put((float) random.nextGaussian());
                weightsBuffer.put(random.nextFloat() * 0.1f - .05f);
                //weightsBuffer.put(random.nextFloat());
                //weightsBuffer.put(0);
            }
            weightsBuffer.flip();

            gpuPtr.setWeights(i, OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, OCLUtil.MemType.COPY, weightsBuffer));
        }
    }

    protected void initialize_S_Z_D(int modelCount) {
        for(int i = 0; i < layer.length - 1; i++) {
            gpuPtr.setS(i, OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, modelCount * layer[i + 1] * 4));
            gpuPtr.setZ(i, OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, modelCount * layer[i + 1] * 4));
            gpuPtr.setD(i, OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, modelCount * layer[i + 1] * 4));

            gpuPtr.setOldD(i, OCLUtil.createZeroFloatBuffer(Kernel.RWFlag.READ_WRITE, modelCount * layer[i + 1]));
            gpuPtr.setB(i, OCLUtil.createZeroFloatBuffer(Kernel.RWFlag.READ_WRITE, modelCount * layer[i + 1]));
        }
    }

    public void finish() {
        forward.finish();
        backward.finish();
        weightUpdate.finish();

        gpuPtr.release();
    }
}
