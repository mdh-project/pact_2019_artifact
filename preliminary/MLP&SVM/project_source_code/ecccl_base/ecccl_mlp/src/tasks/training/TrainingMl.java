package tasks.training;

import model.ModelMultiLabelMLP;
import tasks.routines.Backward;
import tasks.routines.Forward;
import tasks.routines.WeightUpdate;
import model.InputData;
import opencl.Kernel;
import opencl.OCLUtil;
import org.lwjgl.BufferUtils;
import org.lwjgl.opencl.CL10;

import java.nio.FloatBuffer;

public class TrainingMl extends AbstractTraining {

    public TrainingMl(InputData input, int[] layer, float learningRate, float momentum, int seed) {
        super(input, layer, learningRate, momentum, seed);

        initializeWeights(1);
        initialize_S_Z_D(1);

        forward = new Forward(1, layer, gpuPtr);
        backward = new Backward(1, layer, gpuPtr);
        weightUpdate = new WeightUpdate(1, layer, learningRate, momentum, gpuPtr);
    }

    public void train(int epoch) {
        int numInst = input.getNumInstances();

        // instances and label pointer to gpu memory
        long[] instances = new long[numInst];
        long[] label = new long[numInst];

        for(int inst = 0; inst < numInst; inst++) {
            instances[inst] = initInstance(inst);
            label[inst] = initLabel(inst);
        }

        for(int ep = 0; ep < epoch; ep++) {
            for(int inst = 0; inst < numInst; inst++) {

                // new forward step
                forward.execute(instances[inst], label[inst]);

                // new backward step
                backward.execute();

                // weight update step
                weightUpdate.execute(instances[inst]);
            }
        }

        // release memory of instances and label
        OCLUtil.releaseMem(instances);
        OCLUtil.releaseMem(label);
    }

    public ModelMultiLabelMLP getModel() {
        ModelMultiLabelMLP model = new ModelMultiLabelMLP(layer);

        for(int layerId = 0; layerId < layer.length - 1; layerId++) {

            // initialize buffer for weights between layer[layerId] and layer[layerId + 1] and bias
            FloatBuffer weightsBuffer = BufferUtils.createFloatBuffer(layer[layerId] * layer[layerId + 1]);
            FloatBuffer biasBuffer = BufferUtils.createFloatBuffer(layer[layerId + 1]);

            // read weights between layer[layerId] and layer[layerId + 1] and bias of layerId + 1
            OCLUtil.read(weightsBuffer, gpuPtr.weights(layerId), CL10.CL_TRUE);
            OCLUtil.read(biasBuffer, gpuPtr.B(layerId), CL10.CL_TRUE);

            // put values from buffer into array
            float[][] weightLayer = new float[layer[layerId]][layer[layerId + 1]];
            for (int i1 = 0; i1 < layer[layerId + 1]; i1++) {
                for (int j = 0; j < layer[layerId]; j++) {
                    weightLayer[j][i1] = weightsBuffer.get();
                }
            }

            float[] biasLayer = new float[layer[layerId + 1]];
            biasBuffer.get(biasLayer);

            // add weights and bias to model
            model.addWeightLayer(weightLayer);
            model.addBiasLayer(biasLayer);
        }

        return model;
    }

    private long initInstance(int id) {
        int numAttr = input.getNumAttributes();

        FloatBuffer instancesBuffer = BufferUtils.createFloatBuffer(numAttr);
        for(int i = 0; i < numAttr; i++) {
            instancesBuffer.put(input.getAttribute(id, i));
        }
        instancesBuffer.flip();

        return OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, instancesBuffer);
    }

    private long initLabel(int id) {
        int numLabel = input.getNumLabels();

        FloatBuffer labelBuffer = BufferUtils.createFloatBuffer(numLabel);
        for(int i = 0; i < numLabel; i++) {
            float label = input.getLabel(id, i);
            labelBuffer.put(label == -1 ? 0 : label);
        }
        labelBuffer.flip();

        return OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, labelBuffer);
    }
}
