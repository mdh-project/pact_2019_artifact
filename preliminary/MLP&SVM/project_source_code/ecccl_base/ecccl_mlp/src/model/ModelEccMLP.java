package model;

public class ModelEccMLP {
    private int labelCount;
    private int chainCount;
    private int[][] chainLabelOrder;
    private ModelChainMLP[] chainModels;

    public ModelEccMLP(int labelCount, int chainCount, int[][] chainLabelOrder) {
        this.labelCount = labelCount;
        this.chainCount = chainCount;
        this.chainLabelOrder = chainLabelOrder;
        this.chainModels = new ModelChainMLP[labelCount];
    }

    public void addModel(int id, ModelChainMLP model) {
        chainModels[id] = model;
    }

    public ModelChainMLP getModel(int id) {
        return chainModels[id];
    }

    public int[][] getChainLabelOrder() {
        return chainLabelOrder;
    }

    public int getLabelCount() {
        return labelCount;
    }

    public int getChainCount() {
        return chainCount;
    }
}
