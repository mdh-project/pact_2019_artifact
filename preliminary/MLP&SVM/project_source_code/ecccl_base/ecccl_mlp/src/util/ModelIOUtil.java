package util;

import model.ModelChainMLP;
import model.ModelEccMLP;

import java.io.*;

public class ModelIOUtil {

    public static void storeModel(ModelEccMLP model, String modelFilePath) {
        System.out.println("\nStoring model in: " + modelFilePath);

        File outFile = new File(modelFilePath);
        if(outFile.getParentFile() != null) {
            outFile.getParentFile().mkdirs();
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(modelFilePath), 8192 * 8);
            StringBuilder sb = new StringBuilder();

            // append meta data of model
            sb.append(model.getLabelCount()).append(System.lineSeparator());
            sb.append(model.getChainCount()).append(System.lineSeparator());
            for(int chainId = 0; chainId < model.getChainCount(); chainId++) {
                for(int labelId = 0; labelId < model.getLabelCount(); labelId++) {
                    sb.append(model.getChainLabelOrder()[chainId][labelId]).append(";");
                }
                sb.append(System.lineSeparator());
            }

            sb.append(System.lineSeparator());

            // append each model data
            for(int modelId = 0; modelId < model.getLabelCount(); modelId++) {
                ModelChainMLP modelChain = model.getModel(modelId);

                // append layer sizes of current model
                for(int layerSize : modelChain.getLayers()) {
                    sb.append(layerSize).append(";");
                }
                sb.append(System.lineSeparator());

                // append weights and bias of current model
                for(int layerId = 0; layerId < modelChain.getLayers().length - 1; layerId++) {
                    int[] layers = modelChain.getLayers();

                    // append bias
                    float[] bias = modelChain.getBiasLayer(layerId + 1);
                    for (int chainId = 0; chainId < model.getChainCount(); chainId++) {
                        for (int layer = 0; layer < layers[layerId + 1]; layer++) {
                            sb.append(bias[chainId * layers[layerId + 1] + layer]).append(";");
                        }
                        sb.append(System.lineSeparator());
                    }
                    sb.append(System.lineSeparator());

                    // append weights
                    float[] weights = modelChain.getWeightLayer(layerId);
                    for (int chainId = 0; chainId < model.getChainCount(); chainId++) {
                        for (int layer2 = 0; layer2 < layers[layerId + 1]; layer2++) {
                            for (int layer1 = 0; layer1 < layers[layerId]; layer1++) {
                                sb.append(weights[chainId * layers[layerId] * layers[layerId + 1]
                                        + layer2 * layers[layerId]
                                        + layer1]).append(";");
                            }
                            sb.append(System.lineSeparator());
                        }
                        sb.append(System.lineSeparator());
                    }
                    sb.append(System.lineSeparator());
                }
            }

            // store on file system
            writer.append(sb);
            writer.close();

        } catch (IOException e) {
            System.err.println("Error when storing model: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static ModelEccMLP restoreModel(String modelFilePath) throws IOException {
        System.out.println("\nRestoring model from: " + modelFilePath);

        try(BufferedReader br = new BufferedReader(new FileReader(modelFilePath))) {

            // read meta data
            int labelCount = Integer.parseInt(br.readLine());
            int chainCount = Integer.parseInt(br.readLine());

            // read order of labels in chains
            int[][] chainLabelOrder = new int[chainCount][labelCount];
            for(int chainId = 0; chainId < chainCount; chainId++) {
                String[] labelOrder = br.readLine().split(";");

                for(int labelId = 0; labelId < labelOrder.length; labelId++) {
                    chainLabelOrder[chainId][labelId] = Integer.parseInt(labelOrder[labelId]);
                }
            }
            br.readLine();

            // initialize model with meta data
            ModelEccMLP model = new ModelEccMLP(labelCount, chainCount, chainLabelOrder);

            // read model data for each label
            for(int modelId = 0; modelId < model.getLabelCount(); modelId++) {

                // read layer sizes of current model
                int[] layers = parseIntegerLine(br.readLine());

                // initialize model
                ModelChainMLP modelChainMLP = new ModelChainMLP(layers);

                // read weights of current model
                for(int layerId = 0; layerId < layers.length - 1; layerId++) {

                    // read bias
                    float[] bias = new float[chainCount * layers[layerId + 1]];
                    for (int chainId = 0; chainId < model.getChainCount(); chainId++) {
                        String[] lineBias = br.readLine().split(";");

                        for (int layer = 0; layer < layers[layerId + 1]; layer++) {
                            bias[chainId * layers[layerId + 1] + layer] = Float.parseFloat(lineBias[layer]);
                        }
                        br.readLine();
                    }
                    br.readLine();

                    modelChainMLP.addBiasLayer(bias);

                    // read weights
                    float[] weights = new float[chainCount * layers[layerId] * layers[layerId + 1]];
                    for (int chainId = 0; chainId < model.getChainCount(); chainId++) {
                        for (int layer2 = 0; layer2 < layers[layerId + 1]; layer2++) {
                            String[] lineWeights = br.readLine().split(";");

                            for (int layer1 = 0; layer1 < lineWeights.length; layer1++) {
                                weights[chainId * layers[layerId] * layers[layerId + 1] + layer2 * layers[layerId] + layer1]
                                        = Float.parseFloat(lineWeights[layer1]);
                            }
                        }
                        br.readLine();
                    }
                    br.readLine();

                    // add weight layer to model
                    modelChainMLP.addWeightLayer(weights);
                }

                // add model for current label to ecc model
                model.addModel(modelId, modelChainMLP);
            }

            // return resulting ecc model
            return model;

        } catch (FileNotFoundException e) {
            System.err.println("Error restoring model: File not found.");
            throw e;
        } catch (IOException e) {
            System.err.println("Error restoring model: " + e.getMessage());
            throw e;
        }
    }

    private static int[] parseIntegerLine(String line) {
        String[] splitLine = line.split(";");
        int[] result = new int[splitLine.length];

        for(int i = 0; i < result.length; i++) {
            result[i] = Integer.parseInt(splitLine[i]);
        }
        return result;
    }
}
