package reporter;

import model.LabelResults;
import tasks.classification.BatchClassificationMl;
import tasks.training.TrainingMl;
import tester.MultiLabelMLPTester;
import model.InputData;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class MultiLabelMLPReporter {
    private int seed;

    private int epoch;

    private int[] hiddenLayer;
    private float learningRate;
    private float momentum;

    private int batchSize;
    private float threshold;

    private MultiLabelMLPReporter(Builder builder) {
        this.seed = builder.seed;
        this.epoch = builder.epoch;

        this.hiddenLayer = builder.hiddenLayer;
        this.learningRate = builder.learningRate;
        this.momentum = builder.momentum;

        this.batchSize = builder.batchSize;
        this.threshold = builder.threshold;
    }

    public void reportAccuracies(InputData train, InputData test, String resultFilePath) {

        // initialize tester
        MultiLabelMLPTester tester = new MultiLabelMLPTester(batchSize, threshold, false);

        File outFile = new File(resultFilePath);
        if(outFile.exists()) outFile.delete();

        int[] layer = initializeLayer(train);

        // initialize handler for training
        TrainingMl handler = new TrainingMl(train, layer, learningRate, momentum, seed);

        // train with all instances in input epoch-times
        for(int ep = 0; ep < epoch; ep++) {
            handler.train(1);

            // report accuracy
            LabelResults resultsBatch = tester.classify(test, handler.getModel());
            float accuracy = tester.evaluate(test, resultsBatch);
            reportToFile(resultFilePath, accuracy);
        }

        handler.finish();
    }

    private int[] initializeLayer(InputData input) {
        int[] layer = new int[hiddenLayer.length + 2];
        layer[0] = input.getNumAttributes();
        layer[layer.length - 1] = input.getNumLabels();
        for(int j = 0; j < hiddenLayer.length; j++) {
            if (hiddenLayer[j] == -1) layer[j + 1] = (layer[j] + 1) / 2;
            else layer[j + 1] = hiddenLayer[j];
        }
        return layer;
    }

    private void reportToFile(String filePath, float accuracy) {
        File outFile = new File(filePath);
        if(outFile.getParentFile() != null)
            outFile.getParentFile().mkdirs();

        try {
            PrintWriter writer = new PrintWriter(new FileWriter(filePath,true));
            writer.println(accuracy);
            writer.close ();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class Builder {
        private int seed = 0;

        private int epoch;

        private int[] hiddenLayer;
        private float learningRate = 0.3f;
        private float momentum = 0.2f;

        private int batchSize = 32;
        private float threshold = 0.5f;

        public MultiLabelMLPReporter build() {
            return new MultiLabelMLPReporter(this);
        }

        public Builder(int[] hiddenLayer, int epoch)  {
            this.hiddenLayer = hiddenLayer;
            this.epoch = epoch;
        }

        public Builder setSeed(int seed) {
            this.seed = seed;
            return this;
        }

        public Builder setLearningRate(float learningRate) {
            this.learningRate = learningRate;
            return this;
        }

        public Builder setMomentum(float momentum) {
            this.momentum = momentum;
            return this;
        }

        public Builder setBatchSize(int batchSize) {
            this.batchSize = batchSize;
            return this;
        }

        public Builder setThreshold(float threshold) {
            this.threshold = threshold;
            return this;
        }
    }
}
