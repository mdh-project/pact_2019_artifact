package reporter;

import evaluator.Evaluator;
import model.LabelResults;
import model.ModelEccMLP;
import tasks.classification.BatchClassificationEcc;
import tasks.training.TrainingEcc;
import tester.EccMLPTester;
import model.InputData;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

public class EccMLPReporter {
    private Random random;
    private int seed;

    private int chainCount;
    private float subsetPercentage;
    private float thresholdECC;

    private int epoch;

    private int[] hiddenLayer;
    private float learningRate;
    private float momentum;

    private int batchSize;
    private float thresholdMLP;

    private EccMLPReporter(Builder builder) {
        this.chainCount = builder.chainCount;
        this.subsetPercentage = builder.subsetPercentage / 100;

        this.epoch = builder.epoch;
        this.learningRate = builder.learningRate;
        this.momentum = builder.momentum;
        this.hiddenLayer = builder.hiddenLayer;

        this.random = new Random(seed);
        this.seed = builder.seed;

        this.batchSize = builder.batchSize;
        this.thresholdECC = builder.thresholdECC;
        this.thresholdMLP = builder.thresholdMLP;
    }

    public void reportAccuracies(InputData input, InputData test, String resultFilePath) {
        int numLabels = input.getNumLabels();
        int numInstances = input.getNumInstances();

        // initialize tester
        EccMLPTester tester = new EccMLPTester.Builder()
                .setBatchSize(batchSize)
                .setThresholdMLP(thresholdMLP)
                .setThresholdECC(thresholdECC)
                .build();

        File outFile = new File(resultFilePath);
        if(outFile.exists()) outFile.delete();

        // initialize subset of data for each chain
        int subsetSize = (int) Math.floor(subsetPercentage * numInstances);
        int[][] chainInstances = fillRand(subsetSize, numInstances);

        // initialize order of labels for each chain
        int[][] chainLabelOrder = fillRand(numLabels, numLabels);

        // initialize layer sizes
        int[] layer = new int[hiddenLayer.length + 2];
        layer[layer.length - 1] = 1;

        // iterate label [== number of classifiers in each chain]
        ModelEccMLP model = new ModelEccMLP(numLabels, chainCount, chainLabelOrder);
        TrainingEcc[] handler = new TrainingEcc[numLabels];

        for (int labelId = 0; labelId < numLabels; labelId++) {
            // update layer sizes
            updateLayerSizes(input, layer, labelId);

            // initialize handler for training
            handler[labelId] = new TrainingEcc(input, layer.clone(), learningRate, momentum, seed, subsetSize,
                    chainCount, labelId, chainInstances, chainLabelOrder);
            handler[labelId].loadInput(subsetSize);
        }

        for(int ep = 0; ep < epoch; ep++) {
            for (int labelId = 0; labelId < numLabels; labelId++) {
                //handler[labelId].loadInput(subsetSize);
                handler[labelId].train(1);
                //handler[labelId].releaseInput();
                model.addModel(labelId, handler[labelId].getModel());
            }

            LabelResults resultsBatch = tester.classify(test, model);
            float accuracy = Evaluator.evaluate(test, resultsBatch, "./outputs/");
            reportToFile(resultFilePath, accuracy);
        }

        for (int labelId = 0; labelId < numLabels; labelId++) {
            handler[labelId].releaseInput();
            handler[labelId].finish();
        }
    }

    private void reportToFile(String filePath, float accuracy) {
        File outFile = new File(filePath);
        if(outFile.getParentFile() != null)
            outFile.getParentFile().mkdirs();

        try {
            PrintWriter writer = new PrintWriter(new FileWriter(filePath,true));
            writer.println(accuracy);
            writer.close ();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateLayerSizes(InputData input, int[] layer, int i) {
        layer[0] = input.getNumAttributes() + i;
        for(int j = 0; j < hiddenLayer.length; j++) {
            if (hiddenLayer[j] == -1) layer[j + 1] = (layer[j] + 1) / 2;
            else layer[j + 1] = hiddenLayer[j];
        }
    }

    private int[][] fillRand(int subsetSize, int length) {
        int[][] chainInstances = new int[chainCount][subsetSize];
        for (int i = 0; i < chainCount; i++) {
            chainInstances[i] = random.ints(0, length).distinct().limit(subsetSize).toArray();
        }
        return chainInstances;
    }

    public static class Builder {
        private int seed = 0;

        private int chainCount = 10;
        private float subsetPercentage = 100;

        private int epoch;
        private float learningRate = 0.3f;
        private float momentum = 0.2f;
        private int[] hiddenLayer;

        private int batchSize = 32;
        private float thresholdMLP = 0.5f;
        private float thresholdECC = 0.5f;

        public EccMLPReporter build() {
            return new EccMLPReporter(this);
        }

        public Builder(int[] hiddenLayer, int epoch) {
            this.hiddenLayer = hiddenLayer;
            this.epoch = epoch;
        }

        public Builder setSeed(int seed) {
            this.seed = seed;
            return this;
        }

        public Builder setChainCount(int chainCount) {
            this.chainCount = chainCount;
            return this;
        }

        public Builder setSubsetPercentage(int percentage) {
            this.subsetPercentage = percentage;
            return this;
        }

        public Builder setLearningRate(float learningRate) {
            this.learningRate = learningRate;
            return this;
        }

        public Builder setMomentum(float momentum) {
            this.momentum = momentum;
            return this;
        }

        public Builder setBatchSize(int batchSize) {
            this.batchSize = batchSize;
            return this;
        }

        public Builder setThresholdMLP(float thresholdMLP) {
            this.thresholdMLP = thresholdMLP;
            return this;
        }

        public Builder setThresholdECC(float thresholdECC) {
            this.thresholdECC = thresholdECC;
            return this;
        }
    }
}
