package classifier;

import model.ModelEccMLP;
import opencl.PlatformUtil;
import tasks.training.TrainingEcc;
import model.InputData;
import util.Config;
import util.MLPBenchmark;

import java.util.Random;

public class EccMLP extends AbstractMLP{
    private Random rand;

    private int chainCount;
    private float subsetPercentage;

    private EccMLP(Builder builder) {
        super(builder);
        this.chainCount = builder.chainCount;
        this.subsetPercentage = builder.subsetPercentage / 100;
        this.rand = new Random(seed);
    }

    public ModelEccMLP train(InputData input) {
        if(Config.PRINT_LEVEL > 0) System.out.print("\nTraining: ");

        int numLabels = input.getNumLabels();

        // initialize subset of data for each chain
        int subsetSize = (int) Math.floor(subsetPercentage * input.length());
        int[][] chainInstances = fillRand(subsetSize, input.length());

        // initialize order of labels for each chain
        int[][] chainLabelOrder = fillRand(numLabels, numLabels);

        // initialize layer sizes
        int[] layer = new int[hiddenLayer.length + 2];
        layer[layer.length - 1] = 1;

        // initialize model
        ModelEccMLP model = new ModelEccMLP(numLabels, chainCount, chainLabelOrder);

        // iterate label [== number of classifiers in each chain]
        for (int labelId = 0; labelId < numLabels; labelId++) {

            // update layer sizes
            updateLayerSizes(input, layer, labelId);

            // initialize handler for training
            TrainingEcc handler = new TrainingEcc(input, layer, learningRate, momentum, seed, subsetSize,
                    chainCount, labelId, chainInstances, chainLabelOrder);

            // load instances to device
            handler.loadInput(subsetSize);

            // train subsets of chains epoch-times
            handler.train(epoch);

            // release instances from device
            handler.releaseInput();

            // save model
            model.addModel(labelId, handler.getModel());

            handler.finish();

            if(Config.PRINT_LEVEL > 0) System.out.print(".");
        }
        if(Config.PRINT_LEVEL > 0) System.out.println();

        if(Config.PRINT_LEVEL > 1 && PlatformUtil.PROFILING_ENABLED) {
            System.out.println("\nForward: Build " + MLPBenchmark.forwardBuild / 1000000+ " ms, Kernel: " +
                    MLPBenchmark.forwardKernelTime / 1000000 + " ms");
            System.out.println("Activation Build: " + MLPBenchmark.forwardActivationBuild / 1000000 + " ms, Kernel: " +
                    MLPBenchmark.forwardActivationKernelTime / 1000000 + " ms");
            System.out.println("Backprop Build: " + MLPBenchmark.backwardBuild / 1000000 + " ms, Kernel: " +
                    MLPBenchmark.backwardKernelTime / 1000000 + " ms");
            System.out.println("Weight Build: " + MLPBenchmark.weightUpdateBuild / 1000000 + " ms, Kernel: " +
                    MLPBenchmark.weightUpdateKernelTime / 1000000 + " ms");
        }

        return model;
    }

    private void updateLayerSizes(InputData input, int[] layer, int i) {
        layer[0] = input.getNumAttributes() + i;
        for(int j = 0; j < hiddenLayer.length; j++) {
            if (hiddenLayer[j] == -1) layer[j + 1] = (layer[j] + 1) / 2;
            else layer[j + 1] = hiddenLayer[j];
        }
    }

    private int[][] fillRand(int subsetSize, int length) {
        int[][] chainInstances = new int[chainCount][subsetSize];
        for (int i = 0; i < chainCount; i++) {
            chainInstances[i] = rand.ints(0, length).distinct().limit(subsetSize).toArray();
        }
        return chainInstances;
    }

    public static class Builder extends AbstractBuilder<Builder> {
        private int chainCount = 10;
        private float subsetPercentage = 100;

        public Builder(int[] hiddenLayer, int epoch) {
            super(hiddenLayer, epoch);
        }

        @Override
        protected Builder getInstance() {
            return this;
        }

        public EccMLP build() {
            return new EccMLP(this);
        }

        public Builder setChainCount(int chainCount) {
            this.chainCount = chainCount;
            return this;
        }

        public Builder setSubsetPercentage(int percentage) {
            this.subsetPercentage = percentage;
            return this;
        }
    }
}
