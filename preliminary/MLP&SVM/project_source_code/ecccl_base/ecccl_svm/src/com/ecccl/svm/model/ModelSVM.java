package com.ecccl.svm.model;

import model.ChainData;
import com.ecccl.svm.model.kernel.LocalKernel;

public class ModelSVM {

    // model data
    private float[] alphas;
    private float[] supportVectors;
    private float[] label;
    private float b;
    private LocalKernel kernel;

    // meta data of model
    private int numAttributes;
    private int numSupportVectors;
    private int labelIndex;

    public ModelSVM(float b, LocalKernel kernel, float[] sv, float[] label, float[] alphas,
                    int numAttributes, int numInstances, int labelIndex) {
        this.b = b;
        this.kernel = kernel;
        this.supportVectors = sv;
        this.label = label;
        this.alphas = alphas;

        this.numAttributes = numAttributes;
        this.numSupportVectors = numInstances;
        this.labelIndex = labelIndex;
    }

    public int getLabelIndex() {
        return labelIndex;
    }

    public int getNumSupportVectors() {
        return numSupportVectors;
    }

    public int getNumAttributes() {
        return numAttributes;
    }

    public LocalKernel getKernel() {
        return kernel;
    }

    public float[] getAlphas() {
        return alphas;
    }

    public float getAlpha(int id) {
        return alphas[id];
    }

    public float getB() {
        return b;
    }

    public float[] getSupportVectors() {
        return supportVectors;
    }

    public float[] getLabel() {
        return label;
    }

    public float getLabel(int id) {
        return label[id];
    }

    public static ModelSVM createModel(float[] alphas, ChainData input, float b, LocalKernel kernel) {
        int svCount = 0;
        for(float alpha : alphas) if(alpha > 0) svCount++;

        float[] resultAlphas = new float[svCount];
        float[] sv = new float[svCount * input.getNumAttributes()];
        float[] label = new float[svCount];

        int instanceId = 0;
        for(int i = 0; i < alphas.length; i++) {

            // support vectors: only instances with alpha > 0
            if (alphas[i] > 0){

                // set alphas
                resultAlphas[instanceId] = alphas[i];

                // set support vectors to array [first attributes, then labels as attributes]
                System.arraycopy(input.getAttributes(i), 0, sv, instanceId * input.getNumAttributes(),
                        input.getInitNumAttributes());

                System.arraycopy(input.getLabel(i), 0, sv,
                        instanceId * input.getNumAttributes() + input.getInitNumAttributes(),
                        input.getLabelAsAttrCount());

                // set label
                label[instanceId] = input.getCurrentLabel(i);

                instanceId++;
            }
        }

        return new ModelSVM(b, kernel, sv, label, resultAlphas, input.getNumAttributes(), svCount, input.getCurrentLabelIndex());
    }
}
