package com.ecccl.svm.util;

import com.ecccl.svm.EccSVM;
import com.ecccl.svm.model.kernel.LinearKernel;
import com.ecccl.svm.model.kernel.LocalKernel;
import com.ecccl.svm.model.kernel.RBFKernel;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import util.CliUtil;

public class CliSVMUtil extends CliUtil {
    public static final int DEFAULT_KERNEL = 0;
    public static final float DEFAULT_COST = 1;
    public static final float DEFAULT_GAMMA = 0.01f;
    public static final int DEFAULT_MAXIMUM_CACHE = 1000;
    public static final float DEFAULT_EPSILON = 0.001f;
    public static final int DEFAULT_NUM_CC = 10;
    public static final int DEFAULT_BAG_SIZE = 100;
    public static final int DEFAULT_SEED = 0;
    public static final float DEFAULT_THRESHOLD_ECC = 0.5f;

    public static final String ORDER_INPUT_TRAINING = "tloq";
    public static final String ORDER_TRAINING = "tcgmenbs";

    public static final String ORDER_INPUT_CLASSIFIER = "tlmoq";
    public static final String ORDER_CLASSIFIER = "t";

    public static final String ORDER_INPUT_EVALUATION = "tloq";
    public static final String ORDER_EVALUATION = "tcgmenbs";

    public static Options getPlatformOptions() {
        Options optionsPlatform = new Options();
        optionsPlatform.addOption(new Option("pid", true, "platform id (default 0)"));
        optionsPlatform.addOption(new Option("did", true, "device id (default 0)"));
        optionsPlatform.addOption(new Option("dtype", true, "device type: gpu | cpu | acc (default gpu)"));
        return optionsPlatform;
    }

    public static Options getSVMOptions() {
        Options optionsParameter = new Options();
        optionsParameter.addOption("t", "type" ,true, "type of kernel function (default " + DEFAULT_KERNEL + ")\n " +
                "-- 0 = radial basis function (rbf): exp(-gamma*|u-v|^2)\n" +
                "-- 1 = linear: u*v");
        optionsParameter.addOption("c", "cost", true, "parameter C (cost) of svm (default " + DEFAULT_COST + ")");
        optionsParameter.addOption("g", "gamma", true, "gamma in rbf kernel (default " + DEFAULT_GAMMA + ")");
        optionsParameter.addOption("m", "cache", true, "set maximum of cache memory on GPU in MB (default " + DEFAULT_MAXIMUM_CACHE + ")");
        optionsParameter.addOption("e", "eps", true, "tolerance of termination criteria (default " + DEFAULT_EPSILON + ")");
        optionsParameter.addOption("n", "num", true, "number of classifier chain models (default " + DEFAULT_NUM_CC + ")");
        optionsParameter.addOption("b", "bag", true, "size of samples in each model in percentage (default " + DEFAULT_BAG_SIZE + ")");
        optionsParameter.addOption("s", "seed", true, "seeding of random number generator (default " + DEFAULT_SEED + ")");
        return optionsParameter;
    }

    public static EccSVM getSVMFromOptions(CommandLine cmd) {
        // parse SVM parameter
        float gamma = CliSVMUtil.parseFloat("g", cmd, DEFAULT_GAMMA);
        float C = CliSVMUtil.parseFloat("c", cmd, DEFAULT_COST);
        int maximumCache = CliSVMUtil.parseInt("m", cmd, DEFAULT_MAXIMUM_CACHE);
        float epsilon = CliSVMUtil.parseFloat("e", cmd, DEFAULT_EPSILON);

        int kernelType = CliSVMUtil.parseInt("t", cmd, DEFAULT_KERNEL);
        LocalKernel kernel = kernelType == 0 ? new RBFKernel(gamma) : new LinearKernel();

        int numCC = CliSVMUtil.parseInt("n", cmd, DEFAULT_NUM_CC);
        int bagSize = CliSVMUtil.parseInt("b", cmd, DEFAULT_BAG_SIZE);
        int seed = CliSVMUtil.parseInt("s", cmd, DEFAULT_SEED);

        return new EccSVM.Builder()
                .setC(C)
                .setTol(epsilon)
                .setKernel(kernel)
                .setSizeCacheMB(maximumCache)
                .setSeed(seed)
                .setChainCount(numCC)
                .setSubsetPercentage(bagSize)
                .build();
    }

    public static float parseFloat(String option, CommandLine cmd, float defaultVal) {
        return cmd.hasOption(option) ? Float.parseFloat(cmd.getOptionValue(option)) : defaultVal;
    }

    public static int parseInt(String option, CommandLine cmd, int defaultVal) {
        return cmd.hasOption(option) ? Integer.parseInt(cmd.getOptionValue(option)) : defaultVal;
    }
}
