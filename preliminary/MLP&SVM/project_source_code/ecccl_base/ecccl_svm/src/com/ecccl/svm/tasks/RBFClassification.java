package com.ecccl.svm.tasks;

import model.InputData;
import model.ChainResults;
import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.executor.classifier.EvalKernelRBFExecutor;
import com.ecccl.svm.executor.classifier.EvalScoreRBFExecutor;
import com.ecccl.svm.steps.KernelBatchEvaluator;
import com.ecccl.svm.steps.ScoreEvaluator;
import opencl.params.ParamsL1L2R1;

/**
 * This class manages the OpenCL executors to calculate the scores
 * of multiple instances for classifying.
 *
 * @author
 */
public class RBFClassification {

    // steps
    private KernelBatchEvaluator kernelEvaluator;
    private ScoreEvaluator scoreEvaluator;

    private float b;
    private int numInstances;
    private int batchSize;

    public RBFClassification(InputData testInput, ChainResults chainResults, ModelSVM model){
        b = model.getB();
        numInstances = testInput.getNumInstances();
        batchSize = ParamsL1L2R1.getParamsRbf().RANGE;

        if(b == Float.POSITIVE_INFINITY || b == Float.NEGATIVE_INFINITY) return;

        kernelEvaluator = new KernelBatchEvaluator(testInput, model, batchSize, chainResults);
        scoreEvaluator = new ScoreEvaluator(model, batchSize, kernelEvaluator.getResultBatch());
    }

    /**
     * Executes the kernels to calculate the outputs of the classification.
     *
     * @return outputs of the model for all instances
     */
    public float[] getOutputs() {
        float[] results = new float[numInstances];

        // handle special cases where all value of training set were the same
        if (b == Float.POSITIVE_INFINITY) {
            for(int i = 0; i < numInstances; i++) results[i] = 0f;
        } else if (b == Float.NEGATIVE_INFINITY) {
            for(int i = 0; i < numInstances; i++) results[i] = 1.0f;
        } else {
            int runs = numInstances / batchSize;
            int lastBatch = numInstances % batchSize;

            for (int i = 0; i <= runs; i++) {
                int batch = (i == runs) ? lastBatch : batchSize;
                float[] classes = getOutputs(i * batchSize, batch);
                System.arraycopy(classes, 0, results, i * batchSize, batch);
            }
        }

        return results;
    }


    private float[] getOutputs(int index, int count) {
        kernelEvaluator.execute(index);
        return scoreEvaluator.execute(count, b);
    }

    public void release() {
        if(kernelEvaluator != null) kernelEvaluator.release();
        if(scoreEvaluator != null) scoreEvaluator.release();
    }

    /**
     * Releases the OpenCL programs of the executors.
     */
    public static void releaseKernel() {
        EvalKernelRBFExecutor.releaseKernel();
        EvalScoreRBFExecutor.releaseKernel();
    }
}
