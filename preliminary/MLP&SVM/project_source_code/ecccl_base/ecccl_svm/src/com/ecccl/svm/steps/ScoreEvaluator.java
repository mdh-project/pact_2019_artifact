package com.ecccl.svm.steps;

import com.ecccl.svm.model.ModelSVM;
import com.ecccl.svm.executor.classifier.EvalScoreRBFExecutor;
import com.ecccl.svm.model.kernel.RBFKernel;
import com.ecccl.svm.steps.util.PaddingUtil;
import opencl.Kernel;
import opencl.OCLUtil;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

public class ScoreEvaluator {
    private EvalScoreRBFExecutor scoreExecutor;

    private long alpha;
    private long label;

    public ScoreEvaluator(ModelSVM model, int batchSize, long resultBatch) {

        // get meta data from model
        int numSupportVectors = model.getNumSupportVectors();
        float gamma = ((RBFKernel) model.getKernel()).getGamma();

        // calculate padding to make sure md_hom can execute step
        int numSVPadding = PaddingUtil.getPaddingSV(numSupportVectors);

        // load data to device

        loadAlphasToDevice(model.getAlphas(), numSVPadding, numSupportVectors);
        loadLabelsToDevice(model.getLabel(), numSVPadding, numSupportVectors);

        scoreExecutor = EvalScoreRBFExecutor.getInstance(batchSize);
        scoreExecutor.updateArgs(numSVPadding, gamma, resultBatch, alpha, label);
    }

    /**
     * Executes the kernels to calculate the scores of {@code count} instances
     *
     * @param count count of instances
     * @return scores of the instances
     */
    public float[] execute(int count, float b) {
        return scoreExecutor.execute(count, b);
    }

    public void release() {
        if(alpha != 0L) OCLUtil.releaseMem(alpha);
        if(label != 0L) OCLUtil.releaseMem(label);
    }

    private void loadAlphasToDevice(float[] alphas, int numSvPadding, int numSv) {
        FloatBuffer alphaBuffer = BufferUtils.createFloatBuffer(numSvPadding);

        // load alpha values of support vectors
        alphaBuffer.put(alphas);

        // fill padding space with 0
        alphaBuffer.put(new float[numSvPadding - alphas.length]);

        alphaBuffer.flip();
        alpha = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, alphaBuffer);
    }

    private void loadLabelsToDevice(float[] label, int numSvPadding, int numSv) {
        FloatBuffer labelBuffer = BufferUtils.createFloatBuffer(numSvPadding);

        // load labels of support vectors
        labelBuffer.put(label);

        // fill padding space with 0
        labelBuffer.put(new float[numSvPadding - numSv]);

        labelBuffer.flip();
        this.label = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, OCLUtil.MemType.COPY, labelBuffer);
    }
}
