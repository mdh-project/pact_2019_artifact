package com.ecccl.svm.executor.training;

import com.ecccl.svm.util.SVMBenchmark;
import opencl.Kernel;
import opencl.PlatformUtil;
import opencl.params.ParamsL1R1;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class RBFKernelExecutor extends AbstractKernelExecutor {
    private static RBFKernelExecutor instance = null;

    // kernels
    private static Kernel kernel;
    private static Kernel kernel2;
    private static ParamsL1R1 mainParams;

    // buffer for kernels
    private IntBuffer indexBuffer;
    private IntBuffer cacheIndexBuffer;
    private FloatBuffer gammaBuffer;

    // buffer for kernel dimensions
    private IntBuffer numInstancesBuffer;
    private IntBuffer numAttributesBuffer;
    private IntBuffer K2_R1_SIZE_Buffer;

    public interface K1_ARGS {
        int SIZE = 7;
        int INDEX = 0, CACHE_INDEX = 1, GAMMA = 2, INPUT = 3, RESULT = 4;
        int L1 = 5, R1 = 6;
    }

    public interface K2_ARGS {
        int SIZE = 6;
        int K1_RES = 0, CACHE_INDEX = 1, GAMMA = 2, RESULT = 3;
        int L1 = 4, R1 = 5;
    }

    public static RBFKernelExecutor getInstance(int numInstances, int numAttributes, float gamma, long input, long cache) {
        if (instance == null) {
            mainParams = new ParamsL1R1(ParamsL1R1.TUNING_FILE_RBF);
            instance = new RBFKernelExecutor(numInstances, numAttributes, gamma, true);
        }
        instance.updateArgs(numInstances, numAttributes, input, cache);
        return instance;
    }

    public static RBFKernelExecutor getInstance(ParamsL1R1 params, int numInstances, int numAttributes, float gamma, long input, long cache) {
        if (instance == null) {
            mainParams = params;
            instance = new RBFKernelExecutor(numInstances, numAttributes, gamma);
        }
        instance.updateArgs(numInstances, numAttributes, input, cache);
        return instance;
    }

    private RBFKernelExecutor(int numInstances, int numAttributes, float gamma) {
        buildMainKernel(false);
        createBuffers(gamma);
        initArgs(numInstances, numAttributes);
    }

    private RBFKernelExecutor(int numInstances, int numAttributes, float gamma, boolean useBuildCache) {

        long start = System.nanoTime();
        buildMainKernel(useBuildCache);
        long end = System.nanoTime();
        SVMBenchmark.buildKernel += end - start;

        createBuffers(gamma);
        initArgs(numInstances, numAttributes);
    }

    private void buildMainKernel(boolean useBuildCache) {
        String options = (mainParams.NUM_WG_R_1 != 1) ? mainParams.getOptions() : mainParams.getOptions() + " -D SINGLE_KERNEL";

        long program = PlatformUtil.buildProgram("ocl_kernel/svm/svm_rbf_kernel", options, useBuildCache);

        kernel = new Kernel(program, "compute_rbf_kernel_1");
        kernel.setLocalSize(mainParams.LOCAL_SIZE_DIM_0, mainParams.LOCAL_SIZE_DIM_1);
        kernel.setGlobalSize(mainParams.GLOBAL_SIZE_DIM_0, mainParams.GLOBAL_SIZE_DIM_1);
        kernel.setWorkDim(mainParams.WORK_DIM);

        if(mainParams.NUM_WG_R_1 > 1) {
            buildMainKernel2(useBuildCache);
        }
    }

    private void buildMainKernel2(boolean useBuildCache) {
        long program = PlatformUtil.buildProgram("ocl_kernel/svm/svm_rbf_kernel_2",
                mainParams.getOptions(), useBuildCache);

        kernel2 = new Kernel(program, "compute_rbf_kernel_2");
        kernel2.setLocalSize(mainParams.K2_LOCAL_SIZE_DIM_0, mainParams.K2_LOCAL_SIZE_DIM_1);
        kernel2.setGlobalSize(mainParams.K2_GLOBAL_SIZE_DIM_0, mainParams.K2_GLOBAL_SIZE_DIM_1);
        kernel2.setWorkDim(mainParams.WORK_DIM);
    }

    private void createBuffers(float gamma) {

        // create buffer for index of row to be computed && compute line on device
        indexBuffer = BufferUtils.createIntBuffer(1);
        cacheIndexBuffer = BufferUtils.createIntBuffer(1);
        gammaBuffer = BufferUtils.createFloatBuffer(1);
        gammaBuffer.put(gamma).flip();

        // create buffer for kernel dimensions
        numInstancesBuffer = BufferUtils.createIntBuffer(1);
        numAttributesBuffer = BufferUtils.createIntBuffer(1);
        K2_R1_SIZE_Buffer = BufferUtils.createIntBuffer(1);
    }

    private void updateArgs(int numInstances, int numAttributes, long input, long cache) {
        numInstancesBuffer.put(numInstances).flip();
        numAttributesBuffer.put(numAttributes).flip();
        K2_R1_SIZE_Buffer.put(mainParams.K2_G_CB_SIZE_R_1(numAttributes)).flip();

        kernel.setArg(K1_ARGS.INPUT, input, Kernel.TypeFlag.SHARED);

        if(mainParams.NUM_WG_R_1 > 1) {
            kernel2.setArg(K2_ARGS.RESULT, cache, Kernel.TypeFlag.SHARED);
            kernel.setArgs();
            kernel2.setArgs();
        } else {
            kernel.setArg(K1_ARGS.RESULT, cache, Kernel.TypeFlag.SHARED);
            kernel.setArgs();
        }
    }

    private void initArgs(int numInstances, int numAttributes) {

        // update values for dimensions of kernel/kernel2
        numInstancesBuffer.put(numInstances).flip();
        numAttributesBuffer.put(numAttributes).flip();
        K2_R1_SIZE_Buffer.put(mainParams.K2_G_CB_SIZE_R_1(numAttributes)).flip();

        // set arguments for main kernel
        kernel.initArgs(K1_ARGS.SIZE);
        kernel.setInputArg(K1_ARGS.INDEX, indexBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(K1_ARGS.CACHE_INDEX, cacheIndexBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(K1_ARGS.GAMMA, gammaBuffer, Kernel.TypeFlag.WRITE_SCALAR);

        // set dimensions for main kernel
        kernel.setInputArg(K1_ARGS.L1, numInstancesBuffer, Kernel.TypeFlag.WRITE_SCALAR);
        kernel.setInputArg(K1_ARGS.R1, numAttributesBuffer, Kernel.TypeFlag.WRITE_SCALAR);

        // set arguments for kernel2
        if(mainParams.NUM_WG_R_1 > 1) {
            long k1_result = kernel.setIOArg(K1_ARGS.RESULT, numInstances * mainParams.NUM_WG_R_1 * 4, Kernel.TypeFlag.SHARED);

            kernel2.initArgs(K2_ARGS.SIZE);
            kernel2.setArg(K2_ARGS.K1_RES, k1_result, Kernel.TypeFlag.SHARED);
            kernel2.setInputArg(K2_ARGS.CACHE_INDEX, cacheIndexBuffer, Kernel.TypeFlag.WRITE_SCALAR);
            kernel2.setInputArg(K2_ARGS.GAMMA, gammaBuffer, Kernel.TypeFlag.WRITE_SCALAR);
            kernel2.setInputArg(K2_ARGS.L1, numInstancesBuffer, Kernel.TypeFlag.WRITE_SCALAR);
            kernel2.setInputArg(K2_ARGS.R1, K2_R1_SIZE_Buffer, Kernel.TypeFlag.WRITE_SCALAR);
        }
    }

    public void execute(int i, int cacheIndex){

        // set number of row to be computed
        indexBuffer.put(i).flip();
        cacheIndexBuffer.put(cacheIndex).flip();

        // execute kernel
        if(PlatformUtil.PROFILING_ENABLED) {
            kernel.setScalarArgs();
            SVMBenchmark.runKernel += kernel.executeKernelProfile();

            if(mainParams.NUM_WG_R_1 > 1) {
                kernel2.setScalarArgs();
                SVMBenchmark.runKernel += kernel2.executeKernelProfile();
            }
        } else {
            kernel.setScalarArgs();
            kernel.executeKernel();

            if(mainParams.NUM_WG_R_1 > 1) {
                kernel2.setScalarArgs();
                kernel2.executeKernel();
            }
        }
    }

    public long profile(int i, int cacheIndex) {
        // set number of row to be computed
        indexBuffer.put(i).flip();
        cacheIndexBuffer.put(cacheIndex).flip();

        long runtime = 0;
        kernel.setScalarArgs();
        runtime += kernel.executeKernelProfile();

        if(mainParams.NUM_WG_R_1 > 1) {
            kernel2.setScalarArgs();
            runtime += kernel2.executeKernelProfile();
        }

        return runtime;
    }

    public static void releaseKernel() {
        if (kernel != null) {
            kernel.releaseArgs();
            kernel.releaseKernel();
        }
        if (kernel2 != null) {
            kernel2.releaseArgs();
            kernel2.releaseKernel();
        }
    }
}
