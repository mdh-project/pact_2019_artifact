package svm;

import com.ecccl.svm.executor.training.RBFKernelExecutor;
import main.Tuning;
import opencl.Kernel;
import opencl.OCLUtil;
import opencl.PlatformUtil;
import opencl.params.ParamsL1R1;
import util.JSONUtil;

public class RBFKernelTuning {
    private final static int WARM_UP_RUNS = 3;
    private final static int LABEL_RUNS = 20;

    public static void main(String[] args) {
        Tuning.initPlatform(args[0], args[1], args[2]);
        boolean err = false;

        // read Args
        int numInstances = Integer.parseInt(args[3]);
        int numAttributes = Integer.parseInt(args[4]);
        int numLabels = Integer.parseInt(args[5]);
        String tpsJSON = args[6];

        float step = numLabels < LABEL_RUNS ? 1 : (float) numLabels / LABEL_RUNS;
        int iterations = numLabels < LABEL_RUNS ? numLabels : LABEL_RUNS;

        try {
            // init TPs
            ParamsL1R1 mainParams = tpsJSON.equals("") ? new ParamsL1R1("/tuning_results/svm/rbf_main_kernel_tp.json")
                    : new ParamsL1R1(JSONUtil.readArgs(tpsJSON));

            for(int i = 0; i < WARM_UP_RUNS; i++) {
                getRuntime(numInstances, numAttributes, mainParams);
            }

            long runtime = 0;
            for(int i = 0; i < iterations; i++) {
                int numAttr = numAttributes + (int) step * i;
                runtime += getRuntime(numInstances, numAttr, mainParams);
            }
            runtime /= iterations;

            System.out.println(runtime);
        } catch (Exception e) {
            err = true;
            e.printStackTrace();
        } finally {
            PlatformUtil.requestShutdown();
            if(err) System.exit(1);
        }
    }

    private static long getRuntime(int numInstances, int numAttributes, ParamsL1R1 mainParams) {
        // init memory on device
        long input = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, numInstances * numAttributes * 4);
        long cache = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, numInstances * 4);

        // init executor
        RBFKernelExecutor exec = RBFKernelExecutor.getInstance(mainParams, numInstances, numAttributes, 0.01f, input, cache);

        // profile
        long runtime = exec.profile(0, 0);

        // release memory
        OCLUtil.releaseMem(input);
        OCLUtil.releaseMem(cache);

        return runtime;
    }
}
