package svm;

import com.ecccl.svm.executor.classifier.EvalKernelRBFExecutor;
import main.Tuning;
import opencl.Kernel;
import opencl.OCLUtil;
import opencl.PlatformUtil;
import opencl.params.ParamsL1L2R1;
import util.JSONUtil;

public class EvalRBFKernelTuning {
    private final static int WARM_UP_RUNS = 3;
    private final static int RUNS = 5;

    public static void main(String[] args) {
        Tuning.initPlatform(args[0], args[1], args[2]);
        boolean err = false;

        // read Args
        int numAttributes = Integer.parseInt(args[3]);
        int numSV = Integer.parseInt(args[4]);
        String tpsJSON = args[5];

        try {
            // init TPs
            ParamsL1L2R1 params = tpsJSON.equals("") ? new ParamsL1L2R1("/tuning_results/svm/eval_rbf_kernel_tp.json")
                    : new ParamsL1L2R1(JSONUtil.readArgs(tpsJSON));

            // init memory on device
            long testData = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, params.RANGE * numAttributes * 4);
            long inputSV = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, numSV * numAttributes * 4);
            long result = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, params.RANGE * numSV * 4);

            // init Executor
            EvalKernelRBFExecutor exec = EvalKernelRBFExecutor.getInstance(params);
            exec.updateArgs(numSV, numAttributes, params.RANGE, testData, inputSV, result);

            // warm up
            for(int i = 0; i < WARM_UP_RUNS; i++) exec.profile(0);

            // profile
            long runtime = 0;
            for(int i = 0; i < RUNS; i++) runtime += exec.profile(0);
            runtime /= RUNS;

            // release memory
            OCLUtil.releaseMem(testData);
            OCLUtil.releaseMem(inputSV);
            OCLUtil.releaseMem(result);

            System.out.println(runtime);
        } catch (Exception e) {
            err = true;
        } finally {
            PlatformUtil.requestShutdown();
            if(err) System.exit(1);
        }
    }
}
