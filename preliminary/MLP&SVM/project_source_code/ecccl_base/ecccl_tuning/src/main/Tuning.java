package main;

import opencl.PlatformUtil;

import java.security.InvalidParameterException;

public abstract class Tuning {

    public static void initPlatform(String platformId, String deviceId, String deviceType){
        PlatformUtil.DeviceType type;
        switch (deviceType) {
            case "cpu":
            case "CPU":
                type = PlatformUtil.DeviceType.CPU;
                break;
            case "gpu":
            case "GPU":
                type = PlatformUtil.DeviceType.GPU;
                break;
            case "acc":
            case "ACC":
            case "accelerator":
            case "ACCELERATOR":
                type = PlatformUtil.DeviceType.ACC;
                break;
            default:
                throw new InvalidParameterException("Could not recognize device type");
        }

        PlatformUtil.NATIVE_PATH = "../../native";
        PlatformUtil.PROFILING_ENABLED = true;
        PlatformUtil.requestInit(Integer.parseInt(platformId), Integer.parseInt(deviceId), type);
    }
}
