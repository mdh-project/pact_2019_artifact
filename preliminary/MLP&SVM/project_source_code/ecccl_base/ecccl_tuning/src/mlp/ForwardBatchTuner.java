package mlp;

import main.Tuning;
import opencl.Kernel;
import opencl.OCLUtil;
import opencl.PlatformUtil;
import opencl.params_dynamic.ParamsL1L2L3R1;
import org.lwjgl.opencl.CL10;
import executor.classification.ForwardBatchExecutor;
import util.JSONUtil;

public class ForwardBatchTuner {
    private final static int WARM_UP_RUNS = 3;
    private final static int RUNS = 5;

    public static void main(String[] args) {
        Tuning.initPlatform(args[0], args[1], args[2]);
        boolean err = false;

        // parse args
        int chainCount = Integer.parseInt(args[3]);
        int batchSize = Integer.parseInt(args[4]);
        String[] layersStr = args[5].split(";");
        String tpsJSON = args[6];

        int[] layers = new int[layersStr.length];
        for(int i = 0; i < layersStr.length; i++) layers[i] = Integer.parseInt(layersStr[i].trim());

        try {
            // init executor
            ForwardBatchExecutor[] exec = new ForwardBatchExecutor[layers.length - 1];
            for(int i = 0; i < layers.length - 1; i++) {

                ParamsL1L2L3R1 params;
                if(tpsJSON.equals("")) {
                    params = ParamsL1L2L3R1.getParamsForwardBatch(layers[i]);
                } else {
                    params = new ParamsL1L2L3R1(JSONUtil.readArgs(tpsJSON), layers[i]);
                }

                exec[i] = new ForwardBatchExecutor(chainCount, batchSize, layers[i + 1], layers[i], params);
            }

            long runtime = 0;

            // warm up
            for(int i = 0; i < WARM_UP_RUNS; i++) getRuntime(chainCount, batchSize, layers, exec);

            // profile
            for(int i = 0; i < RUNS; i++) runtime += getRuntime(chainCount, batchSize, layers, exec);
            runtime /= RUNS;

            System.out.println(runtime);

            for(int i = 0; i < layers.length - 1; i++) {
                exec[i].release();
            }
        } catch (Exception e) {
            err = true;
            e.printStackTrace();
        } finally {
            PlatformUtil.requestShutdown();
            if(err) System.exit(1);
        }
    }

    private static long getRuntime(int chainCount, int batchSize, int[] layers, ForwardBatchExecutor[] exec) {
        long runtime = 0;

        // profile forward step
        for(int i = 0; i < layers.length - 1; i++) {
            // init memory on device
            long Z = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, chainCount * batchSize * layers[i] * 4);
            long W = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[i] * layers[i + 1] * 4);
            long B = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, chainCount * layers[i + 1] * 4);
            long nextZ = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * batchSize * layers[i + 1] * 4);

            // profile
            runtime += exec[i].profile(Z, W, nextZ, B);

            // release memory
            CL10.clReleaseMemObject(Z);
            CL10.clReleaseMemObject(W);
            CL10.clReleaseMemObject(nextZ);
            CL10.clReleaseMemObject(B);
        }

        return runtime;
    }
}
