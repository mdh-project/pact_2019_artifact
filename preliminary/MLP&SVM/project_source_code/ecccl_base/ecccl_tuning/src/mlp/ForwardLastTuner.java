package mlp;

import main.Tuning;
import opencl.Kernel;
import opencl.OCLUtil;
import opencl.PlatformUtil;
import opencl.params_dynamic.ParamsL1L2R1;
import org.lwjgl.opencl.CL10;
import executor.training.ForwardLastExecutor;
import util.JSONUtil;

public class ForwardLastTuner {
    private final static int WARM_UP_RUNS = 3;
    private final static int RUNS = 5;

    public static void main(String[] args) {
        Tuning.initPlatform(args[0], args[1], args[2]);
        boolean err = false;

        // parse args
        int chainCount = Integer.parseInt(args[3]);
        String[] layersStr = args[4].split(";");
        String tpsJSON = args[5];

        int[] layers = new int[layersStr.length];
        for(int i = 0; i < layersStr.length; i++) layers[i] = Integer.parseInt(layersStr[i].trim());

        try {
            // init tuning parameter
            ParamsL1L2R1 params = tpsJSON.equals("") ? new ParamsL1L2R1(ParamsL1L2R1.TUNING_FILE_LAST_FORWARD_GEMM)
                    : new ParamsL1L2R1(JSONUtil.readArgs(tpsJSON));

            // init executor
            ForwardLastExecutor exec = new ForwardLastExecutor(chainCount, layers[layers.length - 1],
                    layers[layers.length - 2], params);

            long runtime = 0;

            // warm up
            for(int i = 0; i < WARM_UP_RUNS; i++) getRuntime(chainCount, layers, exec);

            // profile
            for(int i = 0; i < RUNS; i++) runtime += getRuntime(chainCount, layers, exec);
            runtime /= RUNS;

            System.out.println(runtime);

            exec.release();
        } catch (Exception e) {
            err = true;
        } finally {
            PlatformUtil.requestShutdown();
            if(err) System.exit(1);
        }
    }

    private static long getRuntime(int chainCount, int[] layers, ForwardLastExecutor exec) {
        long runtime = 0;

        // init memory on device
        long Z = OCLUtil.createBuffer(Kernel.RWFlag.READ_ONLY, chainCount * layers[layers.length - 2] * 4);
        long W = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[layers.length - 2] * layers[layers.length - 1] * 4);
        long B = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[layers.length - 1] * 4);
        long L = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[layers.length - 1] * 4);
        long D = OCLUtil.createBuffer(Kernel.RWFlag.READ_WRITE, chainCount * layers[layers.length - 1] * 4);

        // profile
        runtime += exec.profile(Z, W, L, D, B);

        // release memory
        CL10.clReleaseMemObject(Z);
        CL10.clReleaseMemObject(W);
        CL10.clReleaseMemObject(B);
        CL10.clReleaseMemObject(L);
        CL10.clReleaseMemObject(D);

        return runtime;
    }
}
