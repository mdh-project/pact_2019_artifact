#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" &&
cd $DIR/../tuner

PLATFORM_ID=`grep -oP "\"platform_id\"[ ]*:[ ]*\K[^ ,;\n]+" ../settings.json`
DEVICE_ID=`grep -oP "\"device_id\"[ ]*:[ ]*\K[^ ,;\n]+" ../settings.json`
DEVICE_TYPE=`grep -oP "\"device_type\"[ ]*:[ ]*\"\K[^ ,;\n\"]+" ../settings.json`

DEFAULTVALUE="inputs.json"
INPUT=${1:-$DEFAULTVALUE}

DEFAULT_OUTPUT="."
OUTPUT_PATH=${2:-$DEFAULT_OUTPUT}

TUNING_CONFIGS=`grep -oP "\"04_weight_tuning_configs\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
MAX_TIME=`grep -oP "\"04_weight_max_time\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
CHAINCOUNT=`grep -oP "\"chain_count\"[ ]*:[ ]*\K[^ ,;\n]+" ../$INPUT`
LAYERS=`grep -oP "\"layers\"[ ]*:[ ]*\"\K[^ ,\"\n]+" ../$INPUT`

echo "Starting Tuner: Weight Update Kernel"
echo "Tuning Configs: $TUNING_CONFIGS (Max: $MAX_TIME Minutes)"
echo "Number of chains: $CHAINCOUNT" 
echo "Layers: $LAYERS"

./tuning_eccmlp_weight_update $OUTPUT_PATH $PLATFORM_ID $DEVICE_ID $DEVICE_TYPE $TUNING_CONFIGS $MAX_TIME $CHAINCOUNT $LAYERS

$DIR/../clean.sh
