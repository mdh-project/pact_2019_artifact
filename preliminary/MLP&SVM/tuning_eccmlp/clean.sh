#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" &&
cd $DIR

cd ./tuner
rm -f -r ./opentuner.db
rm -f opentuner.log
rm -f runtime
rm -f ./profiler/hs_err_pid*.log
rm -f -r ./profiler/bin
