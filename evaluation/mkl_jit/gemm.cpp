#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <mkl.h>
#include <fstream>

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--input-size",  3, false);
    args.parse(static_cast<size_t>(argc), argv);
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    const size_t M = input_size[0], N = input_size[1], K = input_size[2];

    // prepare inputs
    auto *a = (float *) mkl_malloc(M * K * sizeof(float), 64); for (int i = 0; i < M * K; ++i) a[i] = (i % 100) + 1;
    auto *b = (float *) mkl_malloc(K * N * sizeof(float), 64); for (int i = 0; i < K * N; ++i) b[i] = (i % 100) + 1;
    auto *c = (float *) mkl_malloc(M * N * sizeof(float), 64); for (int i = 0; i < M * N; ++i) c[i] = 0;

    // prepare jitter
    void *jitter;
    auto status = mkl_jit_create_sgemm(&jitter,
                                       MKL_ROW_MAJOR, MKL_NOTRANS, MKL_NOTRANS,
                                       M, N, K,
                                       1,
                                       K,
                                       N,
                                       0,
                                       N);
    if (status != MKL_JIT_SUCCESS) {
        // JIT not available for this input size
        std::cout << "MKL JIT does not support this input size" << std::endl;
        exit(0);
    }

    // warm ups
    std::cout << std::endl << "benchmarking Intel MKL-JIT..." << std::endl;
    double runtime = std::numeric_limits<double>::max();
    for (int i = 0; i < 10; ++i) {
        // time warm_ups to prevent compiler optimization
        auto start = dsecnd();
        mkl_jit_get_sgemm_ptr(jitter)(jitter, a, b, c);
        auto end = dsecnd();
        runtime = std::min(runtime, (end - start) * 1000);
    }

    // evaluations
    runtime = std::numeric_limits<double>::max();
    for (int i = 0; i < 200; ++i) {
        auto start = dsecnd();
        mkl_jit_get_sgemm_ptr(jitter)(jitter, a, b, c);
        auto end = dsecnd();
        runtime = std::min(runtime, (end - start) * 1000);
    }
    mkl_free(a);
    mkl_free(b);
    mkl_free(c);

    // write runtime to file
    std::string runtime_file_name = std::getenv("ARTIFACT_ROOT");
    runtime_file_name.append("/results/cpu/mkl_jit/");
    runtime_file_name.append("gemm_");
    runtime_file_name.append(std::to_string(M)).append("x")
            .append(std::to_string(N)).append("x")
            .append(std::to_string(K));
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << runtime;
    runtime_file.close();
}