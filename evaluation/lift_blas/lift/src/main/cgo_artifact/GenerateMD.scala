package cgo_artifact

import apart.arithmetic.{PerformSimplification, SizeVar}
import ir._
import ir.ast._
import opencl.executor.Compile
import opencl.generator.{PerformBarrierElimination, PerformLoopOptimisation}
import opencl.ir._
import opencl.ir.pattern._
import rewriting.utils.Utils

object GenerateMD {

  def main(args: Array[String]): Unit = {

    val mdCompute = UserFun("updateF",
      Array("f", "ipos", "jpos", "cutsq", "lj1", "lj2"),
      """|{
        |  // Calculate distance
        |  float delx = ipos.x - jpos.x;
        |  float dely = ipos.y - jpos.y;
        |  float delz = ipos.z - jpos.z;
        |  float r2inv = delx*delx + dely*dely + delz*delz;
        |  // If distance is less than cutoff, calculate force
        |  if (r2inv < cutsq) {
        |    r2inv = 1.0f/r2inv;
        |    float r6inv = r2inv * r2inv * r2inv;
        |    float forceC = r2inv*r6inv*(lj1*r6inv - lj2);
        |    f.x += delx * forceC;
        |    f.y += dely * forceC;
        |    f.z += delz * forceC;
        |  }
        |  return f;
        |}
      """.stripMargin,
      Seq(Float4, Float4, Float4, Float, Float, Float),
      Float4)

    val N = SizeVar("N") // number of particles
    val M = SizeVar("M") // number of neighbors

    val shoc = fun(
      ArrayType(Float4, N),
      ArrayType(ArrayType(Int, N), M),
      Float,
      Float,
      Float,
      (particles, neighbourIds, cutsq, lj1, lj2) =>
        Zip(particles, Transpose() $ neighbourIds) :>>
          Split(128) :>>
          MapWrg(
            MapLcl( \(p =>
              p._0 :>> toPrivate(idF4) :>> Let(particle => {
                Filter(particles, p._1) :>>
                  ReduceSeq(\((force, n) =>
                    mdCompute(force, particle, n, cutsq, lj1, lj2)
                  ), Value(0.0f, Float4)) :>>
                  toGlobal(MapSeq(id.vectorize(4)))
              })
            ) )
          ) :>>
          Join()
    )

    val code = Compile(shoc, 128,1,1,N,1,1,collection.immutable.Map())

    val extension =
      if (PerformSimplification())
        ".cl"
      else if (!PerformLoopOptimisation() && !PerformBarrierElimination())
        "_no_opt.cl"
      else
        "_no_simpl.cl"

    val folder = "generated_kernels/md/"

    Utils.dumpToFile(code, "md" + extension, folder)

  }

}
