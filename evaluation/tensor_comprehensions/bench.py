import time
import tensor_comprehensions as tc
import torch
import os
import sys

sizes = dict()
O_dims = sys.argv[1]
for dim in O_dims:
	sizes[dim] = 0
L_dims = sys.argv[2]
for dim in L_dims:
	sizes[dim] = 0
R_dims = sys.argv[3]
for dim in R_dims:
	sizes[dim] = 0

i = 0
for k in sort(sizes.keys()):
	sizes[k] = int(sys.argv[4+i])
	i += 1
L_sizes = []
for dim in L_dims:
	L_sizes.append(sizes[dim])
R_sizes = []
for dim in R_dims:
	R_sizes.append(sizes[dim])

torch.cuda.device(os.environ['CUDA_GPU_DEVICE_ID'])
if (not os.path.isfile("{0}/results/gpu/tensor_comprehensions/tc_{1}_{2}_{3}.cuda".format(os.environ["ARTIFACT_ROOT"], O_dims, L_dims, R_dims))) or \
		(not os.path.isfile("{0}/results/gpu/tensor_comprehensions/tc_{1}_{2}_{3}.options".format(os.environ["ARTIFACT_ROOT"], O_dims, L_dims, R_dims))):
	print("Unable to benchmark Tensor Comprehensions. Has it not yet been tuned for this input size?")
	sys.exit(1)

lang = """
def tccg(float({0}) L, float({1}) R) -> (O) {
	O({2}) +=! L({3}) * R({4})
}
"""
lang.format(", ".join([x.upper() for x in L_dims]), ", ".join([x.upper() for x in R_dims]), ", ".join(O_dims), ", ".join(L_dims), ", ".join(R_dims))

matmul = tc.define(lang, cache="{0}/results/gpu/tensor_comprehensions/tc_{1}_{2}_{3}".format(os.environ["ARTIFACT_ROOT"], O_dims, L_dims, R_dims), name="tccg")
mat1, mat2 = torch.randn(sizes=L_sizes).cuda(), torch.randn(sizes=R_sizes).cuda()
out = matmul(mat1, mat2)
torch.cuda.synchronize()
# warm ups
for i in range(0, 10):
	out = matmul(mat1, mat2)
#evaluations
times = []
for i in range(0, 200):
	start = time.process_time()
	out = matmul(mat1, mat2)
	torch.cuda.synchronize()
	end = time.process_time()
	times.append(int((end-start) * 1000000000))
torch.cuda.synchronize()

print ("time: ", min(times))
with open("{0}/results/gpu/tensor_comprehensions/tc_{1}_{2}_{3}_runtime".format(os.environ["ARTIFACT_ROOT"], O_dims, L_dims, R_dims), 'w') as file:
	file.write(str(min(times) / 1000000.0))