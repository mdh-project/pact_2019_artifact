import time
import tensor_comprehensions as tc
import torch
import os
import sys

sizes = dict()
O_dims = sys.argv[1]
for dim in O_dims:
    sizes[dim] = 0
L_dims = sys.argv[2]
for dim in L_dims:
    sizes[dim] = 0
R_dims = sys.argv[3]
for dim in R_dims:
    sizes[dim] = 0

i = 0
for k in sort(sizes.keys()):
    sizes[k] = int(sys.argv[4+i])
    i += 1
L_sizes = []
for dim in L_dims:
    L_sizes.append(sizes[dim])
R_sizes = []
for dim in R_dims:
    R_sizes.append(sizes[dim])

torch.cuda.device(os.environ['CUDA_GPU_DEVICE_ID'])

lang = """
def tccg(float({0}) L, float({1}) R) -> (O) {
	O({2}) +=! L({3}) * R({4})
}
"""
lang.format(", ".join([x.upper() for x in L_dims]), ", ".join([x.upper() for x in R_dims]), ", ".join(O_dims), ", ".join(L_dims), ", ".join(R_dims))

matmul = tc.define(lang, cache="{0}/results/gpu/tensor_comprehensions/tc_{1}_{2}_{3}".format(os.environ["ARTIFACT_ROOT"], O_dims, L_dims, R_dims), name="tccg")
mat1, mat2 = torch.randn(sizes=L_sizes).cuda(), torch.randn(sizes=R_sizes).cuda()
settings = {
    "threads": 8, "generations": 1, "pop_size": 100, "number_elites": 10
}
torch.cuda.synchronize()
start = time.process_time()
matmul.autotune(mat1, mat2, cache="{0}/results/gpu/tensor_comprehensions/tc_{1}_{2}_{3}".format(os.environ["ARTIFACT_ROOT"], O_dims, L_dims, R_dims),  **settings)
torch.cuda.synchronize()
end = time.process_time()
print ("autotune: ", end - start)