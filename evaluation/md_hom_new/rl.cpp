#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <atf.h>
#include <json.hpp>

void tune(size_t platform_id, size_t device_id, atf::cf::device_info &device_info, const std::string &device_type, float tuning_time,
          size_t M, size_t N,
          const dbl8 &probM,
          const std::vector<long>      &n_id,
          const std::vector<str46>     &n_lastname1,
          const std::vector<str46>     &n_lastname2,
          const std::vector<str46>     &n_lastname3,
          const std::vector<str46>     &n_firstname1,
          const std::vector<str46>     &n_firstname2,
          const std::vector<str46>     &n_firstname3,
          const std::vector<long>      &n_firstname_group1,
          const std::vector<long>      &n_firstname_group2,
          const std::vector<long>      &n_firstname_group3,
          const std::vector<str46>     &n_birthname1,
          const std::vector<str46>     &n_birthname2,
          const std::vector<str46>     &n_birthname3,
          const std::vector<str46>     &n_birthday,
          const std::vector<chr2>      &n_gender,
          const std::vector<int>       &n_birthmonth,
          const std::vector<int>       &n_birthyear,
          const std::vector<int>       &n_cin,
          const std::vector<double>    &n_prob_lastname1,
          const std::vector<double>    &n_prob_lastname2,
          const std::vector<double>    &n_prob_lastname3,
          const std::vector<double>    &n_prob_firstname1,
          const std::vector<double>    &n_prob_firstname2,
          const std::vector<double>    &n_prob_firstname3,
          const std::vector<double>    &n_prob_birthname1,
          const std::vector<double>    &n_prob_birthname2,
          const std::vector<double>    &n_prob_birthname3,
          const std::vector<double>    &n_prob_birthday,
          const std::vector<double>    &n_prob_gender,
          const std::vector<double>    &n_prob_birthmonth,
          const std::vector<double>    &n_prob_birthyear,
          const std::vector<double>    &n_prob_cin,
          const std::vector<long>      &i_id,
          const std::vector<str46>     &i_lastname1,
          const std::vector<str46>     &i_lastname2,
          const std::vector<str46>     &i_lastname3,
          const std::vector<str46>     &i_firstname1,
          const std::vector<str46>     &i_firstname2,
          const std::vector<str46>     &i_firstname3,
          const std::vector<long>      &i_firstname_group1,
          const std::vector<long>      &i_firstname_group2,
          const std::vector<long>      &i_firstname_group3,
          const std::vector<str46>     &i_birthname1,
          const std::vector<str46>     &i_birthname2,
          const std::vector<str46>     &i_birthname3,
          const std::vector<str46>     &i_birthday,
          const std::vector<chr2>      &i_gender,
          const std::vector<int>       &i_birthmonth,
          const std::vector<int>       &i_birthyear,
          const std::vector<int>       &i_cin,
          const std::vector<double>    &i_prob_lastname1,
          const std::vector<double>    &i_prob_lastname2,
          const std::vector<double>    &i_prob_lastname3,
          const std::vector<double>    &i_prob_firstname1,
          const std::vector<double>    &i_prob_firstname2,
          const std::vector<double>    &i_prob_firstname3,
          const std::vector<double>    &i_prob_birthname1,
          const std::vector<double>    &i_prob_birthname2,
          const std::vector<double>    &i_prob_birthname3,
          const std::vector<double>    &i_prob_birthday,
          const std::vector<double>    &i_prob_gender,
          const std::vector<double>    &i_prob_birthmonth,
          const std::vector<double>    &i_prob_birthyear,
          const std::vector<double>    &i_prob_cin) {
    // get device information
    std::vector<size_t> max_wi_size;
    size_t max_wg_size = 0;
    device_info.get_device_limits(&max_wi_size, &max_wg_size);
    size_t combined_max_wi_size = 0;
    for (int i = 0; i < 2; ++i) {
        combined_max_wi_size = std::max(combined_max_wi_size, max_wi_size[i]);
    }
    
    // define search space
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {0, 1});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {0, 1});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {2});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {2, 1, 0}, atf::less_than_or_eq(G_CB_RES_DEST_LEVEL));
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {2, 1, 0}, atf::less_than_or_eq(L_CB_RES_DEST_LEVEL));

    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {M});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       atf::interval<int>(0, int(ceil(log2(M))), atf::pow_2), atf::less_than_or_eq(INPUT_SIZE_L_1));
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       atf::interval<int>(0, int(ceil(log2(M))), atf::pow_2), atf::less_than_or_eq(L_CB_SIZE_L_1));
    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {0, 1});
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          atf::interval<int>(0, int(ceil(log2(M))), atf::pow_2));
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          atf::interval<int>(0, int(ceil(log2(std::min(M, combined_max_wi_size)))), atf::pow_2), atf::less_than_or_eq(L_CB_SIZE_L_1) && atf::less_than_or_eq((INPUT_SIZE_L_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1));

    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {N});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       atf::interval<int>(0, int(ceil(log2(N))), atf::pow_2), atf::less_than_or_eq(INPUT_SIZE_R_1));
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       atf::interval<int>(0, int(ceil(log2(N))), atf::pow_2), atf::less_than_or_eq(L_CB_SIZE_R_1));
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {0, 1}, atf::unequal(OCL_DIM_L_1));
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          atf::interval<int>(0, int(ceil(log2(N))), atf::pow_2));
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          atf::interval<int>(0, int(ceil(log2(std::min(N, combined_max_wi_size)))), atf::pow_2), atf::less_than_or_eq(L_CB_SIZE_R_1) && atf::less_than_or_eq((INPUT_SIZE_R_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1));

    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {1});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {0});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {1});

    // prepare tuner
    auto tuner = atf::open_tuner(new atf::cond::duration<std::chrono::seconds>(tuning_time * 3600));
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);


    // prepare kernel
    auto is_valid = [&](atf::configuration &config) -> bool {
        return config["NUM_WI_L_1"].value().int_val() <= max_wi_size[config["OCL_DIM_L_1"].value().int_val()] &&
               config["NUM_WI_R_1"].value().int_val() <= max_wi_size[config["OCL_DIM_R_1"].value().int_val()] &&
               config["NUM_WI_L_1"].value().int_val() * config["NUM_WI_R_1"].value().int_val() <= max_wg_size;
    };
    std::vector<char> int_res(M);
    std::vector<long> res_match_id(M);
    std::vector<double> res_match_weight(M);
    std::vector<int> res_id_measure(M);
    auto res_g_size = [&](unsigned int kernel, atf::configuration &config) -> size_t {
        size_t size = M * sizeof(char);
        if (kernel == 1) {
            if (config[G_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[NUM_WG_R_1.name()].value().size_t_val();
            }
            if (config[L_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[NUM_WI_R_1.name()].value().size_t_val();
            }
        } else {
            if (config[L_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[NUM_WI_R_1.name()].value().size_t_val();
            }
        }
        if (config[P_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
            size *= 1;
        }
        return size;
    };
    auto int_res_size = [&](atf::configuration &config) -> size_t {
        size_t size = M * config[NUM_WG_R_1.name()].value().size_t_val() * sizeof(char);
        return size;
    };
    auto needs_second_kernel = [&](atf::configuration &config) -> bool {
        return config[NUM_WG_R_1.name()].value().size_t_val() > 1;
    };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0)                * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1)                * (NUM_WI_R_1)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::LOCAL,
            "../../extern/ATF_RL/ocl_md_hom_new_process_wrapper [flags]",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "kernel/rl_1.cl", "rl_1", ""},
            atf::inputs(atf::scalar<dbl8>(probM),
                        atf::buffer(n_id),
                        atf::buffer(n_lastname1),
                        atf::buffer(n_lastname2),
                        atf::buffer(n_lastname3),
                        atf::buffer(n_firstname1),
                        atf::buffer(n_firstname2),
                        atf::buffer(n_firstname3),
                        atf::buffer(n_firstname_group1),
                        atf::buffer(n_firstname_group2),
                        atf::buffer(n_firstname_group3),
                        atf::buffer(n_birthname1),
                        atf::buffer(n_birthname2),
                        atf::buffer(n_birthname3),
                        atf::buffer(n_birthday),
                        atf::buffer(n_gender),
                        atf::buffer(n_birthmonth),
                        atf::buffer(n_birthyear),
                        atf::buffer(n_cin),
                        atf::buffer(n_prob_lastname1),
                        atf::buffer(n_prob_lastname2),
                        atf::buffer(n_prob_lastname3),
                        atf::buffer(n_prob_firstname1),
                        atf::buffer(n_prob_firstname2),
                        atf::buffer(n_prob_firstname3),
                        atf::buffer(n_prob_birthname1),
                        atf::buffer(n_prob_birthname2),
                        atf::buffer(n_prob_birthname3),
                        atf::buffer(n_prob_birthday),
                        atf::buffer(n_prob_gender),
                        atf::buffer(n_prob_birthmonth),
                        atf::buffer(n_prob_birthyear),
                        atf::buffer(n_prob_cin),

                        atf::buffer(i_id),
                        atf::buffer(i_lastname1),
                        atf::buffer(i_lastname2),
                        atf::buffer(i_lastname3),
                        atf::buffer(i_firstname1),
                        atf::buffer(i_firstname2),
                        atf::buffer(i_firstname3),
                        atf::buffer(i_firstname_group1),
                        atf::buffer(i_firstname_group2),
                        atf::buffer(i_firstname_group3),
                        atf::buffer(i_birthname1),
                        atf::buffer(i_birthname2),
                        atf::buffer(i_birthname3),
                        atf::buffer(i_birthday),
                        atf::buffer(i_gender),
                        atf::buffer(i_birthmonth),
                        atf::buffer(i_birthyear),
                        atf::buffer(i_cin),
                        atf::buffer(i_prob_lastname1),
                        atf::buffer(i_prob_lastname2),
                        atf::buffer(i_prob_lastname3),
                        atf::buffer(i_prob_firstname1),
                        atf::buffer(i_prob_firstname2),
                        atf::buffer(i_prob_firstname3),
                        atf::buffer(i_prob_birthname1),
                        atf::buffer(i_prob_birthname2),
                        atf::buffer(i_prob_birthname3),
                        atf::buffer(i_prob_birthday),
                        atf::buffer(i_prob_gender),
                        atf::buffer(i_prob_birthmonth),
                        atf::buffer(i_prob_birthyear),
                        atf::buffer(i_prob_cin)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "kernel/rl_2.cl", "rl_2"},
            atf::inputs(atf::buffer(res_match_id), atf::buffer(res_match_weight), atf::buffer(res_id_measure)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            5, 3,
            false,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );

    // tune
    auto best_configuration = tuner(kernel);

    // store best found configuration if valid
    if (tuner.number_of_valid_evaluated_configs() > 0) {
        std::map<std::string, int> plain_config;
        for (const auto &tp : best_configuration) {
            plain_config[tp.first] = tp.second.value().int_val();
        }
        std::string config_file_dir = std::getenv("ARTIFACT_ROOT");
        config_file_dir.append("/results/")
                .append(device_type).append("/")
                .append(std::to_string(platform_id)).append("/")
                .append(std::to_string(device_id)).append("/md_hom_new/");
        std::string config_file_name = config_file_dir;
        config_file_name.append("rl_batch_config.json");
        int ret = system(std::string("mkdir -p ").append(config_file_dir).c_str());
        if (ret != 0) {
            std::cerr << "Failed to create results directory." << std::endl;
            exit(EXIT_FAILURE);
        }
        std::ofstream config_file(config_file_name, std::ios::out | std::ios::trunc);
        config_file << std::setw(4) << nlohmann::json(plain_config);
        config_file.close();
    } else {
        std::cerr << "Tuning failed." << std::endl;
        exit(EXIT_FAILURE);
    }
}

void bench(size_t platform_id, size_t device_id, atf::cf::device_info &device_info, const std::string &device_type,
           size_t M, size_t N,
           const dbl8 &probM,
           const std::vector<long>      &n_id,
           const std::vector<str46>     &n_lastname1,
           const std::vector<str46>     &n_lastname2,
           const std::vector<str46>     &n_lastname3,
           const std::vector<str46>     &n_firstname1,
           const std::vector<str46>     &n_firstname2,
           const std::vector<str46>     &n_firstname3,
           const std::vector<long>      &n_firstname_group1,
           const std::vector<long>      &n_firstname_group2,
           const std::vector<long>      &n_firstname_group3,
           const std::vector<str46>     &n_birthname1,
           const std::vector<str46>     &n_birthname2,
           const std::vector<str46>     &n_birthname3,
           const std::vector<str46>     &n_birthday,
           const std::vector<chr2>      &n_gender,
           const std::vector<int>       &n_birthmonth,
           const std::vector<int>       &n_birthyear,
           const std::vector<int>       &n_cin,
           const std::vector<double>    &n_prob_lastname1,
           const std::vector<double>    &n_prob_lastname2,
           const std::vector<double>    &n_prob_lastname3,
           const std::vector<double>    &n_prob_firstname1,
           const std::vector<double>    &n_prob_firstname2,
           const std::vector<double>    &n_prob_firstname3,
           const std::vector<double>    &n_prob_birthname1,
           const std::vector<double>    &n_prob_birthname2,
           const std::vector<double>    &n_prob_birthname3,
           const std::vector<double>    &n_prob_birthday,
           const std::vector<double>    &n_prob_gender,
           const std::vector<double>    &n_prob_birthmonth,
           const std::vector<double>    &n_prob_birthyear,
           const std::vector<double>    &n_prob_cin,
           const std::vector<long>      &i_id,
           const std::vector<str46>     &i_lastname1,
           const std::vector<str46>     &i_lastname2,
           const std::vector<str46>     &i_lastname3,
           const std::vector<str46>     &i_firstname1,
           const std::vector<str46>     &i_firstname2,
           const std::vector<str46>     &i_firstname3,
           const std::vector<long>      &i_firstname_group1,
           const std::vector<long>      &i_firstname_group2,
           const std::vector<long>      &i_firstname_group3,
           const std::vector<str46>     &i_birthname1,
           const std::vector<str46>     &i_birthname2,
           const std::vector<str46>     &i_birthname3,
           const std::vector<str46>     &i_birthday,
           const std::vector<chr2>      &i_gender,
           const std::vector<int>       &i_birthmonth,
           const std::vector<int>       &i_birthyear,
           const std::vector<int>       &i_cin,
           const std::vector<double>    &i_prob_lastname1,
           const std::vector<double>    &i_prob_lastname2,
           const std::vector<double>    &i_prob_lastname3,
           const std::vector<double>    &i_prob_firstname1,
           const std::vector<double>    &i_prob_firstname2,
           const std::vector<double>    &i_prob_firstname3,
           const std::vector<double>    &i_prob_birthname1,
           const std::vector<double>    &i_prob_birthname2,
           const std::vector<double>    &i_prob_birthname3,
           const std::vector<double>    &i_prob_birthday,
           const std::vector<double>    &i_prob_gender,
           const std::vector<double>    &i_prob_birthmonth,
           const std::vector<double>    &i_prob_birthyear,
           const std::vector<double>    &i_prob_cin) {
    // read configuration
    std::string config_file_dir = std::getenv("ARTIFACT_ROOT");
    config_file_dir.append("/results/")
            .append(device_type).append("/")
            .append(std::to_string(platform_id)).append("/")
            .append(std::to_string(device_id)).append("/md_hom_new/");
    std::string config_file_name = config_file_dir;
    config_file_name.append("rl_batch_config.json");
    std::ifstream config_file(config_file_name, std::ios::in);
    if (config_file.fail()) {
        std::cerr << "Unable to open configuration file. Has md_hom not yet been tuned for this input size?" << std::endl;
        exit(EXIT_FAILURE);
    }
    auto plain_config = nlohmann::json::parse(config_file);

    // set configuration for kernel execution
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {plain_config.at("CACHE_L_CB").get<int>()});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {plain_config.at("CACHE_P_CB").get<int>()});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {plain_config.at("G_CB_RES_DEST_LEVEL").get<int>()});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {plain_config.at("L_CB_RES_DEST_LEVEL").get<int>()});
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {plain_config.at("P_CB_RES_DEST_LEVEL").get<int>()});
    auto INPUT_SIZE_L_1      = atf::tp("INPUT_SIZE_L_1",      {plain_config.at("INPUT_SIZE_L_1").get<int>()});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       {plain_config.at("L_CB_SIZE_L_1").get<int>()});
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       {plain_config.at("P_CB_SIZE_L_1").get<int>()});
    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {plain_config.at("OCL_DIM_L_1").get<int>()});
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          {plain_config.at("NUM_WG_L_1").get<int>()});
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          {plain_config.at("NUM_WI_L_1").get<int>()});
    auto INPUT_SIZE_R_1      = atf::tp("INPUT_SIZE_R_1",      {plain_config.at("INPUT_SIZE_R_1").get<int>()});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       {plain_config.at("L_CB_SIZE_R_1").get<int>()});
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       {plain_config.at("P_CB_SIZE_R_1").get<int>()});
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {plain_config.at("OCL_DIM_R_1").get<int>()});
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          {plain_config.at("NUM_WG_R_1").get<int>()});
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          {plain_config.at("NUM_WI_R_1").get<int>()});
    auto L_REDUCTION         = atf::tp("L_REDUCTION",         {plain_config.at("L_REDUCTION").get<int>()});
    auto P_WRITE_BACK        = atf::tp("P_WRITE_BACK",        {plain_config.at("P_WRITE_BACK").get<int>()});
    auto L_WRITE_BACK        = atf::tp("L_WRITE_BACK",        {plain_config.at("L_WRITE_BACK").get<int>()});
    auto tuner = atf::exhaustive();
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_R_1)
            (INPUT_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (INPUT_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1)
            (L_REDUCTION)(P_WRITE_BACK)(L_WRITE_BACK);

    // prepare kernel
    auto is_valid = [&](atf::configuration &config) -> bool {
        return true;
    };
    std::vector<char> int_res(M);
    std::vector<long> res_match_id(M);
    std::vector<double> res_match_weight(M);
    std::vector<int> res_id_measure(M);
    auto res_g_size = [&](unsigned int kernel, atf::configuration &config) -> size_t {
        size_t size = M * sizeof(char);
        if (kernel == 1) {
            if (config[G_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[NUM_WG_R_1.name()].value().size_t_val();
            }
            if (config[L_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[NUM_WI_R_1.name()].value().size_t_val();
            }
        } else {
            if (config[L_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
                size *= config[NUM_WI_R_1.name()].value().size_t_val();
            }
        }
        if (config[P_CB_RES_DEST_LEVEL.name()].value().size_t_val() == 2) {
            size *= 1;
        }
        return size;
    };
    auto int_res_size = [&](atf::configuration &config) -> size_t {
        size_t size = M * config[NUM_WG_R_1.name()].value().size_t_val() * sizeof(char);
        return size;
    };
    auto needs_second_kernel = [&](atf::configuration &config) -> bool {
        return config[NUM_WG_R_1.name()].value().size_t_val() > 1;
    };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0)                * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1)                * (NUM_WI_R_1)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::NONE,
            "none",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "kernel/rl_1.cl", "rl_1", ""},
            atf::inputs(atf::scalar<dbl8>(probM),
                        atf::buffer(n_id),
                        atf::buffer(n_lastname1),
                        atf::buffer(n_lastname2),
                        atf::buffer(n_lastname3),
                        atf::buffer(n_firstname1),
                        atf::buffer(n_firstname2),
                        atf::buffer(n_firstname3),
                        atf::buffer(n_firstname_group1),
                        atf::buffer(n_firstname_group2),
                        atf::buffer(n_firstname_group3),
                        atf::buffer(n_birthname1),
                        atf::buffer(n_birthname2),
                        atf::buffer(n_birthname3),
                        atf::buffer(n_birthday),
                        atf::buffer(n_gender),
                        atf::buffer(n_birthmonth),
                        atf::buffer(n_birthyear),
                        atf::buffer(n_cin),
                        atf::buffer(n_prob_lastname1),
                        atf::buffer(n_prob_lastname2),
                        atf::buffer(n_prob_lastname3),
                        atf::buffer(n_prob_firstname1),
                        atf::buffer(n_prob_firstname2),
                        atf::buffer(n_prob_firstname3),
                        atf::buffer(n_prob_birthname1),
                        atf::buffer(n_prob_birthname2),
                        atf::buffer(n_prob_birthname3),
                        atf::buffer(n_prob_birthday),
                        atf::buffer(n_prob_gender),
                        atf::buffer(n_prob_birthmonth),
                        atf::buffer(n_prob_birthyear),
                        atf::buffer(n_prob_cin),

                        atf::buffer(i_id),
                        atf::buffer(i_lastname1),
                        atf::buffer(i_lastname2),
                        atf::buffer(i_lastname3),
                        atf::buffer(i_firstname1),
                        atf::buffer(i_firstname2),
                        atf::buffer(i_firstname3),
                        atf::buffer(i_firstname_group1),
                        atf::buffer(i_firstname_group2),
                        atf::buffer(i_firstname_group3),
                        atf::buffer(i_birthname1),
                        atf::buffer(i_birthname2),
                        atf::buffer(i_birthname3),
                        atf::buffer(i_birthday),
                        atf::buffer(i_gender),
                        atf::buffer(i_birthmonth),
                        atf::buffer(i_birthyear),
                        atf::buffer(i_cin),
                        atf::buffer(i_prob_lastname1),
                        atf::buffer(i_prob_lastname2),
                        atf::buffer(i_prob_lastname3),
                        atf::buffer(i_prob_firstname1),
                        atf::buffer(i_prob_firstname2),
                        atf::buffer(i_prob_firstname3),
                        atf::buffer(i_prob_birthname1),
                        atf::buffer(i_prob_birthname2),
                        atf::buffer(i_prob_birthname3),
                        atf::buffer(i_prob_birthday),
                        atf::buffer(i_prob_gender),
                        atf::buffer(i_prob_birthmonth),
                        atf::buffer(i_prob_birthyear),
                        atf::buffer(i_prob_cin)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "kernel/rl_2.cl", "rl_2"},
            atf::inputs(atf::buffer(res_match_id), atf::buffer(res_match_weight), atf::buffer(res_id_measure)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            1, 0, // 0 warm ups and 1 evaluation because proper measurement is done below
            true,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );

    // measure runtime
    std::cout << "measuring runtime... " << std::endl;
    std::cout.setstate(std::ios_base::failbit);
    auto best_config = tuner(kernel);
    std::cout.clear();
    kernel.warm_ups(10);
    kernel.evaluations(200);
    std::vector<std::vector<unsigned long long>> runtimes;
    kernel(best_config, &runtimes);

    // write runtime to file
    unsigned long long runtime = 0;
    for (const auto &times : runtimes) {
        if (!times.empty())
            runtime += *std::min_element(times.begin(), times.end());
    }
    std::string runtime_file_name = config_file_dir;
    runtime_file_name.append("rl_");
    runtime_file_name.append(std::to_string(M)).append("x")
            .append(std::to_string(N));
    runtime_file_name.append("_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << (runtime / 1000000.0f);
    runtime_file.close();
}

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--mode",        1, false);
    args.addArgument("--platform-id", 1, false);
    args.addArgument("--device-id",   1, false);
    args.addArgument("--input-size",  2, false);
    args.addArgument("--tuning-time", 1, true);
    args.parse(static_cast<size_t>(argc), argv);
    std::string         mode        = args.retrieve_string("mode");
    size_t              platform_id = args.retrieve_size_t("platform-id");
    size_t              device_id   = args.retrieve_size_t("device-id");
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    float               tuning_time;
    try {
        tuning_time = args.retrieve_float("tuning-time");
    } catch(...) {
        tuning_time = 5.0f;
    }
    const size_t M = input_size[0], N = input_size[1];

    // get device
    atf::cf::device_info device_info(static_cast<const int &>(platform_id), static_cast<const int &>(device_id));
    device_info.initialize();
    std::string device_type;
    cl_device_type type;
    atf::cf::check_error(device_info.device().getInfo(CL_DEVICE_TYPE, &type));
    switch (type) {
        case CL_DEVICE_TYPE_CPU:
            device_type = "cpu";
            break;
        case CL_DEVICE_TYPE_GPU:
            device_type = "gpu";
            break;
        case CL_DEVICE_TYPE_ACCELERATOR:
            device_type = "accelerator";
            break;
        case CL_DEVICE_TYPE_CUSTOM:
            device_type = "custom";
            break;
        default:
            std::cerr << "unknown device type." << std::endl;
            exit(EXIT_FAILURE);
    }

    // prepare inputs
    const dbl8 probM =
            {{
                     0.99439781, // lastname
                     0.98247962, // firstname
                     0.98772576, // birthname
                     0.99949391, // birthday
                     0.9995371,  // birthmonth
                     0.99971322, // birthyear
                     0.99999304, // gender
                     0.9915867   // cid
             }};

    std::vector<long>      n_id(M);
    std::vector<str46>     n_lastname1(M);
    std::vector<str46>     n_lastname2(M);
    std::vector<str46>     n_lastname3(M);
    std::vector<str46>     n_firstname1(M);
    std::vector<str46>     n_firstname2(M);
    std::vector<str46>     n_firstname3(M);
    std::vector<long>      n_firstname_group1(M);
    std::vector<long>      n_firstname_group2(M);
    std::vector<long>      n_firstname_group3(M);
    std::vector<str46>     n_birthname1(M);
    std::vector<str46>     n_birthname2(M);
    std::vector<str46>     n_birthname3(M);
    std::vector<str46>     n_birthday(M);
    std::vector<chr2>      n_gender(M);
    std::vector<int>       n_birthmonth(M);
    std::vector<int>       n_birthyear(M);
    std::vector<int>       n_cin(M);
    std::vector<double>    n_prob_lastname1(M);
    std::vector<double>    n_prob_lastname2(M);
    std::vector<double>    n_prob_lastname3(M);
    std::vector<double>    n_prob_firstname1(M);
    std::vector<double>    n_prob_firstname2(M);
    std::vector<double>    n_prob_firstname3(M);
    std::vector<double>    n_prob_birthname1(M);
    std::vector<double>    n_prob_birthname2(M);
    std::vector<double>    n_prob_birthname3(M);
    std::vector<double>    n_prob_birthday(M);
    std::vector<double>    n_prob_gender(M);
    std::vector<double>    n_prob_birthmonth(M);
    std::vector<double>    n_prob_birthyear(M);
    std::vector<double>    n_prob_cin(M);

    std::string n_file_name = "../ekr/data/" + std::to_string(M) + ".csv";
    std::cout << "loading new reports from " << n_file_name << std::endl;
    std::unordered_map<std::string, long> firstname_groups;
    std::ifstream n_csv_file(n_file_name);
    std::string line;
    int li = 0;
    while (std::getline(n_csv_file, line)) {
        std::istringstream s(line);
        std::string field;
        int fi = 0;
        while (std::getline(s, field, ';')) {
            std::stringstream ss(field);
            switch (fi) {
                case 0:
                    if (field.empty() || !(ss >> n_id[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 1:
                    if (!field.empty() && !memcpy(n_lastname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 2:
                    if (!field.empty() && !(ss >> n_prob_lastname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 3:
                    if (!field.empty() && !memcpy(n_lastname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 4:
                    if (!field.empty() && !(ss >> n_prob_lastname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 5:
                    if (!field.empty() && !memcpy(n_lastname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 6:
                    if (!field.empty() && !(ss >> n_prob_lastname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 7:
                    if (!field.empty() && !memcpy(n_firstname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 8:
                    if (!field.empty() && !(ss >> n_prob_firstname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 9:
                    if (!field.empty() && !memcpy(n_firstname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 10:
                    if (!field.empty() && !(ss >> n_prob_firstname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 11:
                    if (!field.empty() && !memcpy(n_firstname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 12:
                    if (!field.empty() && !(ss >> n_prob_firstname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 13:
                    if (!field.empty() && !memcpy(n_birthname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 14:
                    if (!field.empty() && !(ss >> n_prob_birthname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 15:
                    if (!field.empty() && !memcpy(n_birthname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 16:
                    if (!field.empty() && !(ss >> n_prob_birthname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 17:
                    if (!field.empty() && !memcpy(n_birthname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 18:
                    if (!field.empty() && !(ss >> n_prob_birthname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 19:
                case 20:
                    // pcfields are not used
                    break;
                case 21:
                    if (!field.empty() && !memcpy(n_birthday[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 22:
                    if (!field.empty() && !(ss >> n_prob_birthday[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 23:
                    if (!field.empty() && !(ss >> n_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 24:
                    if (!field.empty() && !(ss >> n_prob_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 25:
                    if (!field.empty() && !(ss >> n_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 26:
                    if (!field.empty() && !(ss >> n_prob_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 27:
                    if (!field.empty() && !memcpy(n_gender[li].values, field.c_str(), 2)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 28:
                    if (!field.empty() && !(ss >> n_prob_gender[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 29:
                    if (!field.empty() && !(ss >> n_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 30:
                    if (!field.empty() && !(ss >> n_prob_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                default:
                    std::cerr << "unexpected values" << std::endl;
                    exit(EXIT_FAILURE);
            }
            ++fi;
        }
        if (n_prob_firstname1[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname1[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname1[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group1[li] = firstname_groups[std::string(n_firstname1[li].str, 46)];
        }
        if (n_prob_firstname2[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname2[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname2[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group2[li] = firstname_groups[std::string(n_firstname2[li].str, 46)];
        }
        if (n_prob_firstname3[li] != 0) {
            if (firstname_groups.find(std::string(n_firstname3[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(n_firstname3[li].str, 46)] = firstname_groups.size();
            }
            n_firstname_group3[li] = firstname_groups[std::string(n_firstname3[li].str, 46)];
        }
        n_prob_birthyear[li] = 1.0E-7;
        n_prob_gender[li] = 1.0E-7;
        n_prob_cin[li] = 1.0E-7;
        ++li;
    }
    assert(li == M);
    n_csv_file.close();

    std::vector<long>      i_id(N);
    std::vector<str46>     i_lastname1(N);
    std::vector<str46>     i_lastname2(N);
    std::vector<str46>     i_lastname3(N);
    std::vector<str46>     i_firstname1(N);
    std::vector<str46>     i_firstname2(N);
    std::vector<str46>     i_firstname3(N);
    std::vector<long>      i_firstname_group1(N);
    std::vector<long>      i_firstname_group2(N);
    std::vector<long>      i_firstname_group3(N);
    std::vector<str46>     i_birthname1(N);
    std::vector<str46>     i_birthname2(N);
    std::vector<str46>     i_birthname3(N);
    std::vector<str46>     i_birthday(N);
    std::vector<chr2>      i_gender(N);
    std::vector<int>       i_birthmonth(N);
    std::vector<int>       i_birthyear(N);
    std::vector<int>       i_cin(N);
    std::vector<double>    i_prob_lastname1(N);
    std::vector<double>    i_prob_lastname2(N);
    std::vector<double>    i_prob_lastname3(N);
    std::vector<double>    i_prob_firstname1(N);
    std::vector<double>    i_prob_firstname2(N);
    std::vector<double>    i_prob_firstname3(N);
    std::vector<double>    i_prob_birthname1(N);
    std::vector<double>    i_prob_birthname2(N);
    std::vector<double>    i_prob_birthname3(N);
    std::vector<double>    i_prob_birthday(N);
    std::vector<double>    i_prob_gender(N);
    std::vector<double>    i_prob_birthmonth(N);
    std::vector<double>    i_prob_birthyear(N);
    std::vector<double>    i_prob_cin(N);

    std::string i_file_name = "../ekr/data/" + std::to_string(N) + ".csv";
    std::cout << "loading inventory reports from " << i_file_name << std::endl;
    std::ifstream i_csv_file(i_file_name);
    li = 0;
    while (std::getline(i_csv_file, line)) {
        std::istringstream s(line);
        std::string field;
        int fi = 0;
        while (std::getline(s, field, ';')) {
            std::stringstream ss(field);
            switch (fi) {
                case 0:
                    if (field.empty() || !(ss >> i_id[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 1:
                    if (!field.empty() && !memcpy(i_lastname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 2:
                    if (!field.empty() && !(ss >> i_prob_lastname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 3:
                    if (!field.empty() && !memcpy(i_lastname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 4:
                    if (!field.empty() && !(ss >> i_prob_lastname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 5:
                    if (!field.empty() && !memcpy(i_lastname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 6:
                    if (!field.empty() && !(ss >> i_prob_lastname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 7:
                    if (!field.empty() && !memcpy(i_firstname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 8:
                    if (!field.empty() && !(ss >> i_prob_firstname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 9:
                    if (!field.empty() && !memcpy(i_firstname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 10:
                    if (!field.empty() && !(ss >> i_prob_firstname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 11:
                    if (!field.empty() && !memcpy(i_firstname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 12:
                    if (!field.empty() && !(ss >> i_prob_firstname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 13:
                    if (!field.empty() && !memcpy(i_birthname1[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 14:
                    if (!field.empty() && !(ss >> i_prob_birthname1[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 15:
                    if (!field.empty() && !memcpy(i_birthname2[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 16:
                    if (!field.empty() && !(ss >> i_prob_birthname2[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 17:
                    if (!field.empty() && !memcpy(i_birthname3[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 18:
                    if (!field.empty() && !(ss >> i_prob_birthname3[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 19:
                case 20:
                    // pcfields are not used
                    break;
                case 21:
                    if (!field.empty() && !memcpy(i_birthday[li].str, field.c_str(), 46)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 22:
                    if (!field.empty() && !(ss >> i_prob_birthday[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 23:
                    if (!field.empty() && !(ss >> i_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 24:
                    if (!field.empty() && !(ss >> i_prob_birthmonth[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 25:
                    if (!field.empty() && !(ss >> i_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 26:
                    if (!field.empty() && !(ss >> i_prob_birthyear[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 27:
                    if (!field.empty() && !memcpy(i_gender[li].values, field.c_str(), 2)) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 28:
                    if (!field.empty() && !(ss >> i_prob_gender[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 29:
                    if (!field.empty() && !(ss >> i_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                case 30:
                    if (!field.empty() && !(ss >> i_prob_cin[li])) {
                        std::cerr << "unexpected values in line " << li << " column " << fi << ": " << field << std::endl;
                        exit(EXIT_FAILURE);
                    }
                    break;
                default:
                    std::cerr << "unexpected values" << std::endl;
                    exit(EXIT_FAILURE);
            }
            ++fi;
        }
        if (i_prob_firstname1[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname1[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname1[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group1[li] = firstname_groups[std::string(i_firstname1[li].str, 46)];
        }
        if (i_prob_firstname2[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname2[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname2[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group2[li] = firstname_groups[std::string(i_firstname2[li].str, 46)];
        }
        if (i_prob_firstname3[li] != 0) {
            if (firstname_groups.find(std::string(i_firstname3[li].str, 46)) == firstname_groups.end()) {
                firstname_groups[std::string(i_firstname3[li].str, 46)] = firstname_groups.size();
            }
            i_firstname_group3[li] = firstname_groups[std::string(i_firstname3[li].str, 46)];
        }
        i_prob_birthyear[li] = 1.0E-7;
        i_prob_gender[li] = 1.0E-7;
        i_prob_cin[li] = 1.0E-7;
        ++li;
    }
    assert(li == N);
    i_csv_file.close();

    if (mode == "tune") {
        tune(platform_id, device_id, device_info, device_type, tuning_time, M, N,
             probM,
             n_id,
             n_lastname1,
             n_lastname2,
             n_lastname3,
             n_firstname1,
             n_firstname2,
             n_firstname3,
             n_firstname_group1,
             n_firstname_group2,
             n_firstname_group3,
             n_birthname1,
             n_birthname2,
             n_birthname3,
             n_birthday,
             n_gender,
             n_birthmonth,
             n_birthyear,
             n_cin,
             n_prob_lastname1,
             n_prob_lastname2,
             n_prob_lastname3,
             n_prob_firstname1,
             n_prob_firstname2,
             n_prob_firstname3,
             n_prob_birthname1,
             n_prob_birthname2,
             n_prob_birthname3,
             n_prob_birthday,
             n_prob_gender,
             n_prob_birthmonth,
             n_prob_birthyear,
             n_prob_cin,

             i_id,
             i_lastname1,
             i_lastname2,
             i_lastname3,
             i_firstname1,
             i_firstname2,
             i_firstname3,
             i_firstname_group1,
             i_firstname_group2,
             i_firstname_group3,
             i_birthname1,
             i_birthname2,
             i_birthname3,
             i_birthday,
             i_gender,
             i_birthmonth,
             i_birthyear,
             i_cin,
             i_prob_lastname1,
             i_prob_lastname2,
             i_prob_lastname3,
             i_prob_firstname1,
             i_prob_firstname2,
             i_prob_firstname3,
             i_prob_birthname1,
             i_prob_birthname2,
             i_prob_birthname3,
             i_prob_birthday,
             i_prob_gender,
             i_prob_birthmonth,
             i_prob_birthyear,
             i_prob_cin);
    } else if (mode == "bench") {
        bench(platform_id, device_id, device_info, device_type, M, N,
              probM,
              n_id,
              n_lastname1,
              n_lastname2,
              n_lastname3,
              n_firstname1,
              n_firstname2,
              n_firstname3,
              n_firstname_group1,
              n_firstname_group2,
              n_firstname_group3,
              n_birthname1,
              n_birthname2,
              n_birthname3,
              n_birthday,
              n_gender,
              n_birthmonth,
              n_birthyear,
              n_cin,
              n_prob_lastname1,
              n_prob_lastname2,
              n_prob_lastname3,
              n_prob_firstname1,
              n_prob_firstname2,
              n_prob_firstname3,
              n_prob_birthname1,
              n_prob_birthname2,
              n_prob_birthname3,
              n_prob_birthday,
              n_prob_gender,
              n_prob_birthmonth,
              n_prob_birthyear,
              n_prob_cin,

              i_id,
              i_lastname1,
              i_lastname2,
              i_lastname3,
              i_firstname1,
              i_firstname2,
              i_firstname3,
              i_firstname_group1,
              i_firstname_group2,
              i_firstname_group3,
              i_birthname1,
              i_birthname2,
              i_birthname3,
              i_birthday,
              i_gender,
              i_birthmonth,
              i_birthyear,
              i_cin,
              i_prob_lastname1,
              i_prob_lastname2,
              i_prob_lastname3,
              i_prob_firstname1,
              i_prob_firstname2,
              i_prob_firstname3,
              i_prob_birthname1,
              i_prob_birthname2,
              i_prob_birthname3,
              i_prob_birthday,
              i_prob_gender,
              i_prob_birthmonth,
              i_prob_birthyear,
              i_prob_cin);
    } else {
        std::cerr << "unknown mode! possible values: tune, bench" << std::endl;
        exit(EXIT_FAILURE);
    }
}