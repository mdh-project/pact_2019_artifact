// helper macros
#if   OCL_DIM_L_1 == 1
  #define CONCAT_IN_DESCENDING_OCL_ORDER_1(i, j) i

  #define CONCAT_IN_DESCENDING_OCL_ORDER_0(i, j) j
#elif OCL_DIM_L_2 == 1
  #define CONCAT_IN_DESCENDING_OCL_ORDER_1(i, j) j

  #define CONCAT_IN_DESCENDING_OCL_ORDER_0(i, j) i
#endif
#define CONCAT_IN_DESCENDING_OCL_ORDER(i, j) CONCAT_IN_DESCENDING_OCL_ORDER_1(i, j)CONCAT_IN_DESCENDING_OCL_ORDER_0(i, j)

#if   OCL_DIM_L_1 == 1
  #define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_1(i_id, j_id, i_size, j_size) i_id

  #define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(i_id, j_id, i_size, j_size) (FLAT_INDEX_IN_DESCENDING_OCL_ORDER_1(i_id, j_id, i_size, j_size)) * (j_size) + (j_id)
#elif OCL_DIM_L_2 == 1
  #define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_1(i_id, j_id, i_size, j_size) j_id

  #define FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(i_id, j_id, i_size, j_size) (FLAT_INDEX_IN_DESCENDING_OCL_ORDER_1(i_id, j_id, i_size, j_size)) * (i_size) + (i_id)
#endif
#define FLAT_INDEX_IN_DESCENDING_OCL_ORDER(i_id, j_id, i_size, j_size) FLAT_INDEX_IN_DESCENDING_OCL_ORDER_0(i_id, j_id, i_size, j_size)

#if   OCL_DIM_L_1 == 1
  #define DESCENDING_L_DIMS_1(i, j) i

  #define DESCENDING_L_DIMS_0(i, j) j
#elif OCL_DIM_L_2 == 1
  #define DESCENDING_L_DIMS_1(i, j) j

  #define DESCENDING_L_DIMS_0(i, j) i
#endif

#define GET_GLOBAL_ID_L_1   get_global_id(OCL_DIM_L_1)
#define GET_LOCAL_ID_L_1    get_local_id(OCL_DIM_L_1)
#define GET_GROUP_ID_L_1    get_group_id(OCL_DIM_L_1)
#define GET_GLOBAL_SIZE_L_1 get_global_size(OCL_DIM_L_1)
#define GET_LOCAL_SIZE_L_1  get_local_size(OCL_DIM_L_1)
#define GET_GLOBAL_ID_L_2   get_global_id(OCL_DIM_L_2)
#define GET_LOCAL_ID_L_2    get_local_id(OCL_DIM_L_2)
#define GET_GROUP_ID_L_2    get_group_id(OCL_DIM_L_2)
#define GET_GLOBAL_SIZE_L_2 get_global_size(OCL_DIM_L_2)
#define GET_LOCAL_SIZE_L_2  get_local_size(OCL_DIM_L_2)

#define PRIVATE 0
#define LOCAL   1
#define GLOBAL  2

struct WEIGHTS {
    TYPE_T values[5][5];
};

// =============== macro definitions per dimension ============================
// -------------------- L_1 --------------------

// cache block sizes
#define K1_G_CB_SIZE_L_1 G_CB_SIZE_L_1
#define K1_L_CB_SIZE_L_1 L_CB_SIZE_L_1
#define K1_P_CB_SIZE_L_1 P_CB_SIZE_L_1

// functional unit ids
#define K1_G_FU_ID_L_1 i_wg_l_1
#define K1_L_FU_ID_L_1 i_wi_l_1
#define K1_P_FU_ID_L_1 0

// number of functional units
#define K1_G_NUM_FU_L_1 NUM_WG_L_1
#define K1_L_NUM_FU_L_1 NUM_WI_L_1
#define K1_P_NUM_FU_L_1 1

// number of cache blocks per functional unit
#define K1_G_NUM_CB_L_1 1 // == (M_1 / K1_G_CB_SIZE_L_1 / 1)
#define K1_L_NUM_CB_L_1 (K1_G_CB_SIZE_L_1 / K1_L_CB_SIZE_L_1 / K1_G_NUM_FU_L_1)
#define K1_P_NUM_CB_L_1 (K1_L_CB_SIZE_L_1 / K1_P_CB_SIZE_L_1 / K1_L_NUM_FU_L_1)

// number of extra cache blocks
#define K1_G_NUM_EXTRA_CB_L_1 0 // == (M_1 / K1_G_CB_SIZE_L_1 % 1)
#define K1_L_NUM_EXTRA_CB_L_1 (K1_G_CB_SIZE_L_1 / K1_L_CB_SIZE_L_1 % K1_G_NUM_FU_L_1)
#define K1_P_NUM_EXTRA_CB_L_1 (K1_L_CB_SIZE_L_1 / K1_P_CB_SIZE_L_1 % K1_L_NUM_FU_L_1)

// number of extra elements
#define K1_G_NUM_EXTRA_ELEMS_L_1 0 // == (M_1 % K1_G_CB_SIZE_L_1)
#define K1_L_NUM_EXTRA_ELEMS_L_1 (K1_G_CB_SIZE_L_1 % K1_L_CB_SIZE_L_1)
#define K1_P_NUM_EXTRA_ELEMS_L_1 (K1_L_CB_SIZE_L_1 % K1_P_CB_SIZE_L_1)

// size of incomplete cache blocks
#define K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 (K1_L_CB_SIZE_L_1 % K1_P_CB_SIZE_L_1)
#define K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 (K1_G_CB_SIZE_L_1 % K1_L_CB_SIZE_L_1 % K1_P_CB_SIZE_L_1)
#define K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 (K1_G_CB_SIZE_L_1 % K1_L_CB_SIZE_L_1)

// number of cache blocks in incomplete parent cache block per functional unit
#define K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 / K1_P_CB_SIZE_L_1 / K1_L_NUM_FU_L_1)

// number of extra cache blocks in incomplete parent cache block
#define K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 ((K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 / K1_P_CB_SIZE_L_1) % K1_L_NUM_FU_L_1)

// number of extra elements in incomplete parent cache block
#define K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 % K1_P_CB_SIZE_L_1)

// cache block offsets
#define K1_G_CB_OFFSET_L_1 0 // == (K1_G_CB_SIZE_L_1 * (0 + i_g_cb_L_1 * 1))
#define K1_L_CB_OFFSET_L_1 (K1_L_CB_SIZE_L_1 * (K1_G_FU_ID_L_1 + i_l_cb_l_1 * K1_G_NUM_FU_L_1))
#define K1_P_CB_OFFSET_L_1 (K1_P_CB_SIZE_L_1 * (K1_L_FU_ID_L_1 + i_p_cb_l_1 * K1_L_NUM_FU_L_1))


// -------------------- L_2 --------------------

// cache block sizes
#define K1_G_CB_SIZE_L_2 G_CB_SIZE_L_2
#define K1_L_CB_SIZE_L_2 L_CB_SIZE_L_2
#define K1_P_CB_SIZE_L_2 P_CB_SIZE_L_2

// functional unit ids
#define K1_G_FU_ID_L_2 i_wg_l_2
#define K1_L_FU_ID_L_2 i_wi_l_2
#define K1_P_FU_ID_L_2 0

// number of functional units
#define K1_G_NUM_FU_L_2 NUM_WG_L_2
#define K1_L_NUM_FU_L_2 NUM_WI_L_2
#define K1_P_NUM_FU_L_2 1

// number of cache blocks per functional unit
#define K1_G_NUM_CB_L_2 1 // == (M_2 / K1_G_CB_SIZE_L_2 / 1)
#define K1_L_NUM_CB_L_2 (K1_G_CB_SIZE_L_2 / K1_L_CB_SIZE_L_2 / K1_G_NUM_FU_L_2)
#define K1_P_NUM_CB_L_2 (K1_L_CB_SIZE_L_2 / K1_P_CB_SIZE_L_2 / K1_L_NUM_FU_L_2)

// number of extra cache blocks
#define K1_G_NUM_EXTRA_CB_L_2 0 // == (M_2 / K1_G_CB_SIZE_L_2 % 1)
#define K1_L_NUM_EXTRA_CB_L_2 (K1_G_CB_SIZE_L_2 / K1_L_CB_SIZE_L_2 % K1_G_NUM_FU_L_2)
#define K1_P_NUM_EXTRA_CB_L_2 (K1_L_CB_SIZE_L_2 / K1_P_CB_SIZE_L_2 % K1_L_NUM_FU_L_2)

// number of extra elements
#define K1_G_NUM_EXTRA_ELEMS_L_2 0 // == (M_2 % K1_G_CB_SIZE_L_2)
#define K1_L_NUM_EXTRA_ELEMS_L_2 (K1_G_CB_SIZE_L_2 % K1_L_CB_SIZE_L_2)
#define K1_P_NUM_EXTRA_ELEMS_L_2 (K1_L_CB_SIZE_L_2 % K1_P_CB_SIZE_L_2)

// size of incomplete cache blocks
#define K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 (K1_L_CB_SIZE_L_2 % K1_P_CB_SIZE_L_2)
#define K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 (K1_G_CB_SIZE_L_2 % K1_L_CB_SIZE_L_2 % K1_P_CB_SIZE_L_2)
#define K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 (K1_G_CB_SIZE_L_2 % K1_L_CB_SIZE_L_2)

// number of cache blocks in incomplete parent cache block per functional unit
#define K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 / K1_P_CB_SIZE_L_2 / K1_L_NUM_FU_L_2)

// number of extra cache blocks in incomplete parent cache block
#define K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 ((K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 / K1_P_CB_SIZE_L_2) % K1_L_NUM_FU_L_2)

// number of extra elements in incomplete parent cache block
#define K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 % K1_P_CB_SIZE_L_2)

// cache block offsets
#define K1_G_CB_OFFSET_L_2 0 // == (K1_G_CB_SIZE_L_2 * (0 + i_g_cb_L_2 * 1))
#define K1_L_CB_OFFSET_L_2 (K1_L_CB_SIZE_L_2 * (K1_G_FU_ID_L_2 + i_l_cb_l_2 * K1_G_NUM_FU_L_2))
#define K1_P_CB_OFFSET_L_2 (K1_P_CB_SIZE_L_2 * (K1_L_FU_ID_L_2 + i_p_cb_l_2 * K1_L_NUM_FU_L_2))


// -------------------- combined over dimensions --------------------

// flat WI ids
#define K1_G_FLAT_WI_ID (FLAT_INDEX_IN_DESCENDING_OCL_ORDER(GET_GLOBAL_ID_L_1, GET_GLOBAL_ID_L_2, GET_GLOBAL_SIZE_L_1, GET_GLOBAL_SIZE_L_2))
#define K1_L_FLAT_WI_ID (FLAT_INDEX_IN_DESCENDING_OCL_ORDER(GET_LOCAL_ID_L_1, GET_LOCAL_ID_L_2, GET_LOCAL_SIZE_L_1, GET_LOCAL_SIZE_L_2))
#define K1_P_FLAT_WI_ID (0)

// flat number of WIs
#define K1_G_FLAT_NUM_WI (K1_G_NUM_FU_L_1 * K1_L_NUM_FU_L_1 * K1_G_NUM_FU_L_2 * K1_L_NUM_FU_L_2)
#define K1_L_FLAT_NUM_WI (K1_L_NUM_FU_L_1 * K1_L_NUM_FU_L_2)
#define K1_P_FLAT_NUM_WI (1)
// =============== end of macro definitions per dimension =====================

// =============== macro definitions per buffer ===============================
// -------------------- buffer IN --------------------

// buffer abstraction
#if   OCL_DIM_L_1 == 1
  #define BUFFER_IN_INDEX_1(i, j) i
  #define BUFFER_IN_G_SIZE_1 K1_G_CB_SIZE_L_1
  #define BUFFER_IN_L_SIZE_1 (2 + K1_L_CB_SIZE_L_1 + 2)
  #define BUFFER_IN_P_SIZE_1 (2 + K1_P_CB_SIZE_L_1 + 2)

  #define BUFFER_IN_INDEX_0(i, j) j
  #define BUFFER_IN_G_SIZE_0 K1_G_CB_SIZE_L_2
  #define BUFFER_IN_L_SIZE_0 (2 + K1_L_CB_SIZE_L_2 + 2)
  #define BUFFER_IN_P_SIZE_0 (2 + K1_P_CB_SIZE_L_2 + 2)
#elif OCL_DIM_L_2 == 1
  #define BUFFER_IN_INDEX_1(i, j) j
  #define BUFFER_IN_G_SIZE_1 K1_G_CB_SIZE_L_2
  #define BUFFER_IN_L_SIZE_1 (2 + K1_L_CB_SIZE_L_2 + 2)
  #define BUFFER_IN_P_SIZE_1 (2 + K1_P_CB_SIZE_L_2 + 2)

  #define BUFFER_IN_INDEX_0(i, j) i
  #define BUFFER_IN_G_SIZE_0 K1_G_CB_SIZE_L_1
  #define BUFFER_IN_L_SIZE_0 (2 + K1_L_CB_SIZE_L_1 + 2)
  #define BUFFER_IN_P_SIZE_0 (2 + K1_P_CB_SIZE_L_1 + 2)
#endif
#define K1_G_BUFFER_IN(i, j) in[((2 + i)) * (2 + K1_G_CB_SIZE_L_2 + 2) + ((2 + j))]
#define K1_L_BUFFER_IN(i, j) cb_l_in[(BUFFER_IN_INDEX_1(i + 2, j + 2))][(BUFFER_IN_INDEX_0(i + 2, j + 2))]
#define K1_P_BUFFER_IN(i, j) cb_p_in[(BUFFER_IN_INDEX_1(i + 2, j + 2))][(BUFFER_IN_INDEX_0(i + 2, j + 2))]

// partitioning and cache usage
#define K1_G_MEM_IN(i, j) K1_G_BUFFER_IN(i, j)
#if CACHE_L_CB != 0
#define K1_L_MEM_IN(i, j) K1_L_BUFFER_IN(i, j)
#else
#define K1_L_MEM_IN(i, j) K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + (i), K1_L_CB_OFFSET_L_2 + (j))
#endif
#if CACHE_P_CB != 0
#define K1_P_MEM_IN(i, j) K1_P_BUFFER_IN(i, j)
#else
#define K1_P_MEM_IN(i, j) K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + (i), K1_P_CB_OFFSET_L_2 + (j))
#endif

// cache block sizes
#define K1_G_CB_SIZE_IN (K1_G_CB_SIZE_L_1 * K1_G_CB_SIZE_L_2)
#define K1_L_CB_SIZE_IN ((2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_SIZE_L_2 + 2))
#define K1_P_CB_SIZE_IN ((2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_SIZE_L_2 + 2))


// -------------------- buffer WEIGHTS --------------------

// buffer abstraction

#define K1_G_BUFFER_WEIGHTS() weights[0]
#define K1_L_BUFFER_WEIGHTS() cb_l_weights[0]
#define K1_P_BUFFER_WEIGHTS() cb_p_weights[0]

// partitioning and cache usage
#define K1_G_MEM_WEIGHTS(i, j) K1_G_BUFFER_WEIGHTS()
#if CACHE_L_CB != 0
#define K1_L_MEM_WEIGHTS(i, j) K1_L_BUFFER_WEIGHTS()
#else
#define K1_L_MEM_WEIGHTS(i, j) K1_G_MEM_WEIGHTS(K1_L_CB_OFFSET_L_1 + (i), K1_L_CB_OFFSET_L_2 + (j))
#endif
#if CACHE_P_CB != 0
#define K1_P_MEM_WEIGHTS(i, j) K1_P_BUFFER_WEIGHTS()
#else
#define K1_P_MEM_WEIGHTS(i, j) K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + (i), K1_P_CB_OFFSET_L_2 + (j))
#endif

// cache block sizes
#define K1_G_CB_SIZE_WEIGHTS ()
#define K1_L_CB_SIZE_WEIGHTS ()
#define K1_P_CB_SIZE_WEIGHTS ()


// -------------------- result buffer --------------------

// check which levels are used
#if G_CB_RES_DEST_LEVEL == PRIVATE || L_CB_RES_DEST_LEVEL == PRIVATE || P_CB_RES_DEST_LEVEL == PRIVATE
#define K1_P_LEVEL_HAS_RESULTS
#endif
#if G_CB_RES_DEST_LEVEL == LOCAL || L_CB_RES_DEST_LEVEL == LOCAL || P_CB_RES_DEST_LEVEL == LOCAL
#define K1_L_LEVEL_HAS_RESULTS
#endif
#if G_CB_RES_DEST_LEVEL == GLOBAL || L_CB_RES_DEST_LEVEL == GLOBAL || P_CB_RES_DEST_LEVEL == GLOBAL
#define K1_G_LEVEL_HAS_RESULTS
#endif

// ------ PRIVATE ------
#ifdef K1_P_LEVEL_HAS_RESULTS
// construct prefix for res_p
#if G_CB_RES_DEST_LEVEL == PRIVATE
#define K1_RES_P_BUFFER_G_PREFIX_L_1() [i_l_cb_l_1]
#define K1_RES_P_BUFFER_G_PREFIX_L_2() [i_l_cb_l_2]
#define K1_RES_P_BUFFER_DEF_G_PREFIX_L_1() [K1_L_NUM_CB_L_1 + ((K1_L_NUM_EXTRA_CB_L_1 + K1_L_NUM_EXTRA_ELEMS_L_1) > 0)]
#define K1_RES_P_BUFFER_DEF_G_PREFIX_L_2() [K1_L_NUM_CB_L_2 + ((K1_L_NUM_EXTRA_CB_L_2 + K1_L_NUM_EXTRA_ELEMS_L_2) > 0)]
#else
#define K1_RES_P_BUFFER_G_PREFIX_L_1()
#define K1_RES_P_BUFFER_G_PREFIX_L_2()
#define K1_RES_P_BUFFER_DEF_G_PREFIX_L_1()
#define K1_RES_P_BUFFER_DEF_G_PREFIX_L_2()
#endif
#if L_CB_RES_DEST_LEVEL == PRIVATE
#define K1_RES_P_BUFFER_L_PREFIX_L_1() [i_p_cb_l_1]
#define K1_RES_P_BUFFER_L_PREFIX_L_2() [i_p_cb_l_2]
#define K1_RES_P_BUFFER_DEF_L_PREFIX_L_1() [K1_P_NUM_CB_L_1 + ((K1_P_NUM_EXTRA_CB_L_1 + K1_P_NUM_EXTRA_ELEMS_L_1) > 0)]
#define K1_RES_P_BUFFER_DEF_L_PREFIX_L_2() [K1_P_NUM_CB_L_2 + ((K1_P_NUM_EXTRA_CB_L_2 + K1_P_NUM_EXTRA_ELEMS_L_2) > 0)]
#else
#define K1_RES_P_BUFFER_L_PREFIX_L_1()
#define K1_RES_P_BUFFER_L_PREFIX_L_2()
#define K1_RES_P_BUFFER_DEF_L_PREFIX_L_1()
#define K1_RES_P_BUFFER_DEF_L_PREFIX_L_2()
#endif
// buffer abstraction for res_p
#define K1_RES_P_BUFFER_NAME() res_p
#define K1_RES_P_BUFFER_DEF K1_RES_P_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_P_BUFFER_DEF_G_PREFIX_L_1()K1_RES_P_BUFFER_DEF_L_PREFIX_L_1()[K1_P_CB_SIZE_L_1], K1_RES_P_BUFFER_DEF_G_PREFIX_L_2()K1_RES_P_BUFFER_DEF_L_PREFIX_L_2()[K1_P_CB_SIZE_L_2])
#define K1_RES_P_BUFFER(i, j) K1_RES_P_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_P_BUFFER_G_PREFIX_L_1()K1_RES_P_BUFFER_L_PREFIX_L_1()[i], K1_RES_P_BUFFER_G_PREFIX_L_2()K1_RES_P_BUFFER_L_PREFIX_L_2()[j])
#endif

// ------ LOCAL ------
#ifdef K1_L_LEVEL_HAS_RESULTS
// construct prefix for res_l
#if G_CB_RES_DEST_LEVEL == LOCAL
#define K1_RES_L_BUFFER_G_PREFIX_L_1() [i_l_cb_l_1]
#define K1_RES_L_BUFFER_G_PREFIX_L_2() [i_l_cb_l_2]
#define K1_RES_L_BUFFER_DEF_G_PREFIX_L_1() [K1_L_NUM_CB_L_1 + ((K1_L_NUM_EXTRA_CB_L_1 + K1_L_NUM_EXTRA_ELEMS_L_1) > 0)]
#define K1_RES_L_BUFFER_DEF_G_PREFIX_L_2() [K1_L_NUM_CB_L_2 + ((K1_L_NUM_EXTRA_CB_L_2 + K1_L_NUM_EXTRA_ELEMS_L_2) > 0)]
#else
#define K1_RES_L_BUFFER_G_PREFIX_L_1()
#define K1_RES_L_BUFFER_G_PREFIX_L_2()
#define K1_RES_L_BUFFER_DEF_G_PREFIX_L_1()
#define K1_RES_L_BUFFER_DEF_G_PREFIX_L_2()
#endif
// buffer abstraction for res_l
#define K1_RES_L_BUFFER_NAME() res_l
#define K1_RES_L_BUFFER_DEF K1_RES_L_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_L_BUFFER_DEF_G_PREFIX_L_1()[K1_L_CB_SIZE_L_1], K1_RES_L_BUFFER_DEF_G_PREFIX_L_2()[K1_L_CB_SIZE_L_2])
#define K1_RES_L_BUFFER(i, j) K1_RES_L_BUFFER_NAME()CONCAT_IN_DESCENDING_OCL_ORDER(K1_RES_L_BUFFER_G_PREFIX_L_1()[i], K1_RES_L_BUFFER_G_PREFIX_L_2()[j])
#endif

// ------ GLOBAL ------
#ifdef K1_G_LEVEL_HAS_RESULTS
// buffer abstraction for res_g
#define K1_RES_G_BUFFER_NAME() int_res
#define K1_RES_G_BUFFER(i, j) K1_RES_G_BUFFER_NAME()[(i) * K1_G_CB_SIZE_L_2 + (j)]
#endif

// determine memory destination for results
#if   P_CB_RES_DEST_LEVEL == PRIVATE
#define K1_P_CB_RES_DEST(i, j) K1_RES_P_BUFFER(i, j)
#elif P_CB_RES_DEST_LEVEL == LOCAL
#define K1_P_CB_RES_DEST(i, j) K1_RES_L_BUFFER(K1_P_CB_OFFSET_L_1 + (i), K1_P_CB_OFFSET_L_2 + (j))
#elif P_CB_RES_DEST_LEVEL == GLOBAL
#define K1_P_CB_RES_DEST(i, j) K1_RES_G_BUFFER(K1_P_CB_OFFSET_L_1 + (K1_L_CB_OFFSET_L_1 + (i)), K1_P_CB_OFFSET_L_2 + (K1_L_CB_OFFSET_L_2 + (j)))
#endif

#if   L_CB_RES_DEST_LEVEL == PRIVATE
#define K1_L_CB_RES_DEST(i, j) K1_RES_P_BUFFER(i, j)
#elif L_CB_RES_DEST_LEVEL == LOCAL
#define K1_L_CB_RES_DEST(i, j) K1_RES_L_BUFFER(K1_P_CB_OFFSET_L_1 + (i), K1_P_CB_OFFSET_L_2 + (j))
#elif L_CB_RES_DEST_LEVEL == GLOBAL
#define K1_L_CB_RES_DEST(i, j) K1_RES_G_BUFFER(K1_P_CB_OFFSET_L_1 + (K1_L_CB_OFFSET_L_1 + (i)), K1_P_CB_OFFSET_L_2 + (K1_L_CB_OFFSET_L_2 + (j)))
#endif

#if   G_CB_RES_DEST_LEVEL == PRIVATE
#define K1_G_CB_RES_DEST(i, j) K1_RES_P_BUFFER(i, j)
#elif G_CB_RES_DEST_LEVEL == LOCAL
#define K1_G_CB_RES_DEST(i, j) K1_RES_L_BUFFER(K1_P_CB_OFFSET_L_1 + (i), K1_P_CB_OFFSET_L_2 + (j))
#elif G_CB_RES_DEST_LEVEL == GLOBAL
#define K1_G_CB_RES_DEST(i, j) K1_RES_G_BUFFER(K1_P_CB_OFFSET_L_1 + (K1_L_CB_OFFSET_L_1 + (i)), K1_P_CB_OFFSET_L_2 + (K1_L_CB_OFFSET_L_2 + (j)))
#endif


// buffer abstraction for kernel_res buffer
#define K1_KERNEL_RES_BUFFER(i, j) int_res[(i) * K1_G_CB_SIZE_L_2 + (j)]

#define K1_G_KERNEL_RES(i, j) K1_KERNEL_RES_BUFFER(K1_G_CB_OFFSET_L_1 + (i), K1_G_CB_OFFSET_L_2 + (j))
#define K1_L_KERNEL_RES(i, j) K1_G_KERNEL_RES(K1_L_CB_OFFSET_L_1 + (i), K1_L_CB_OFFSET_L_2 + (j))
#define K1_P_KERNEL_RES(i, j) K1_L_KERNEL_RES(K1_P_CB_OFFSET_L_1 + (i), K1_P_CB_OFFSET_L_2 + (j))
// =============== end of macro definitions per buffer ========================

// =============== scalar function ============================================
inline TYPE_TS f(const TYPE_T in_val_l1_m2_l2_m2, const TYPE_T in_val_l1_m2_l2_m1, const TYPE_T in_val_l1_m2, const TYPE_T in_val_l1_m2_l2_p1, const TYPE_T in_val_l1_m2_l2_p2, const TYPE_T in_val_l1_m1_l2_m2, const TYPE_T in_val_l1_m1_l2_m1, const TYPE_T in_val_l1_m1, const TYPE_T in_val_l1_m1_l2_p1, const TYPE_T in_val_l1_m1_l2_p2, const TYPE_T in_val_l2_m2, const TYPE_T in_val_l2_m1, const TYPE_T in_val, const TYPE_T in_val_l2_p1, const TYPE_T in_val_l2_p2, const TYPE_T in_val_l1_p1_l2_m2, const TYPE_T in_val_l1_p1_l2_m1, const TYPE_T in_val_l1_p1, const TYPE_T in_val_l1_p1_l2_p1, const TYPE_T in_val_l1_p1_l2_p2, const TYPE_T in_val_l1_p2_l2_m2, const TYPE_T in_val_l1_p2_l2_m1, const TYPE_T in_val_l1_p2, const TYPE_T in_val_l1_p2_l2_p1, const TYPE_T in_val_l1_p2_l2_p2, const struct WEIGHTS weights_val) {
  
  return (weights_val.values[0][0] * in_val_l1_m2_l2_m2 + weights_val.values[0][1] * in_val_l1_m2_l2_m1 + weights_val.values[0][2] * in_val_l1_m2 + weights_val.values[0][3] * in_val_l1_m2_l2_p1 + weights_val.values[0][4] * in_val_l1_m2_l2_p2 +
          weights_val.values[1][0] * in_val_l1_m1_l2_m2 + weights_val.values[1][1] * in_val_l1_m1_l2_m1 + weights_val.values[1][2] * in_val_l1_m1 + weights_val.values[1][3] * in_val_l1_m1_l2_p1 + weights_val.values[1][4] * in_val_l1_m1_l2_p2 +
          weights_val.values[2][0] * in_val_l2_m2       + weights_val.values[2][1] * in_val_l2_m1       + weights_val.values[2][2] * in_val       + weights_val.values[2][3] * in_val_l2_p1       + weights_val.values[2][4] * in_val_l2_p2       +
          weights_val.values[3][0] * in_val_l1_p1_l2_m2 + weights_val.values[3][1] * in_val_l1_p1_l2_m1 + weights_val.values[3][2] * in_val_l1_p1 + weights_val.values[3][3] * in_val_l1_p1_l2_p1 + weights_val.values[3][4] * in_val_l1_p1_l2_p2 +
          weights_val.values[4][0] * in_val_l1_p2_l2_m2 + weights_val.values[4][1] * in_val_l1_p2_l2_m1 + weights_val.values[4][2] * in_val_l1_p2 + weights_val.values[4][3] * in_val_l1_p2_l2_p1 + weights_val.values[4][4] * in_val_l1_p2_l2_p2);
}
// =============== end of scalar function =====================================

// =============== kernel 1 ===================================================
__kernel void gaussian_1(__global TYPE_T const * const restrict in, __global struct WEIGHTS const * const restrict weights, __global TYPE_TS * const restrict res_g, __global TYPE_TS * const restrict int_res) {
  // map md_hom dimensions to OpenCL dimensions
  const size_t i_wg_l_1 = GET_GROUP_ID_L_1;
  const size_t i_wi_l_1 = GET_LOCAL_ID_L_1;
  
  const size_t i_wg_l_2 = GET_GROUP_ID_L_2;
  const size_t i_wi_l_2 = GET_LOCAL_ID_L_2;
  
  // declare variables for caching inputs
  #if CACHE_L_CB != 0
  __local TYPE_T cb_l_in[BUFFER_IN_L_SIZE_1][BUFFER_IN_L_SIZE_0];
  __local struct WEIGHTS cb_l_weights[1];
  #endif
  #if CACHE_P_CB != 0
  __private TYPE_T cb_p_in[BUFFER_IN_P_SIZE_1][BUFFER_IN_P_SIZE_0];
  __private struct WEIGHTS cb_p_weights[1];
  #endif

  // declare variables for result memory
  // ------ LOCAL ------
  #ifdef K1_L_LEVEL_HAS_RESULTS
  __local TYPE_TS K1_RES_L_BUFFER_DEF;
  #endif
  // ------ PRIVATE ------
  #ifdef K1_P_LEVEL_HAS_RESULTS
  __private TYPE_TS K1_RES_P_BUFFER_DEF;
  #endif

  #if K1_L_NUM_CB_L_1 > 0
  for (size_t i_l_cb_l_1 = 0; i_l_cb_l_1 < K1_L_NUM_CB_L_1; ++i_l_cb_l_1) {
    #if K1_L_NUM_CB_L_2 > 0
    for (size_t i_l_cb_l_2 = 0; i_l_cb_l_2 < K1_L_NUM_CB_L_2; ++i_l_cb_l_2) {
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI; ++step) {
        const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      #if K1_L_CB_SIZE_IN % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_IN % K1_L_FLAT_NUM_WI) {
        const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      if (l_cb_l_1 == 0 && l_cb_l_2 == 0) {
        K1_L_MEM_WEIGHTS(0, 0) = K1_G_MEM_WEIGHTS(K1_L_CB_OFFSET_L_1 + 0, K1_L_CB_OFFSET_L_2 + 0);
      
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      
      // move results upwards in memory hierarchy
      #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
      #if L_CB_RES_DEST_LEVEL < LOCAL
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      #else
      #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI) {
        const size_t flat_index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
      #endif
    } // end of "i_l_cb_l_2"-loop
    #endif
    // post process whole extra cache blocks in dimension L_2
    #if K1_L_NUM_EXTRA_CB_L_2 > 0
    if (K1_G_FU_ID_L_2 < K1_L_NUM_EXTRA_CB_L_2) {  
      const size_t i_l_cb_l_2 = K1_L_NUM_CB_L_2;
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI; ++step) {
        const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      #if K1_L_CB_SIZE_IN % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_IN % K1_L_FLAT_NUM_WI) {
        const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      if (l_cb_l_1 == 0 && l_cb_l_2 == 0) {
        K1_L_MEM_WEIGHTS(0, 0) = K1_G_MEM_WEIGHTS(K1_L_CB_OFFSET_L_1 + 0, K1_L_CB_OFFSET_L_2 + 0);
      
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      
      // move results upwards in memory hierarchy
      #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
      #if L_CB_RES_DEST_LEVEL < LOCAL
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      #else
      #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI) {
        const size_t flat_index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
      #endif
    } // end of post processing whole extra cache blocks in dimension L_2
    #endif
    // post process single extra incomplete cache block in dimension L_2
    #if K1_L_NUM_EXTRA_ELEMS_L_2 > 0
    if (K1_G_FU_ID_L_2 == K1_L_NUM_EXTRA_CB_L_2 % K1_G_NUM_FU_L_2) {  
      const size_t i_l_cb_l_2 = K1_L_NUM_CB_L_2;
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      #if (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_L_FLAT_NUM_WI) {
        const size_t index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      if (l_cb_l_1 == 0 && l_cb_l_2 == 0) {
        K1_L_MEM_WEIGHTS(0, 0) = K1_G_MEM_WEIGHTS(K1_L_CB_OFFSET_L_1 + 0, K1_L_CB_OFFSET_L_2 + 0);
      
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      
      // move results upwards in memory hierarchy
      #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
      #if L_CB_RES_DEST_LEVEL < LOCAL
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      #else
      #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) % K1_L_FLAT_NUM_WI) {
        const size_t flat_index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
      #endif
    }// end of post process single extra incomplete cache block in dimension L_2
    #endif
  } // end of "i_l_cb_l_1"-loop
  #endif
  // post process whole extra cache blocks in dimension L_1
  #if K1_L_NUM_EXTRA_CB_L_1 > 0
  if (K1_G_FU_ID_L_1 < K1_L_NUM_EXTRA_CB_L_1) {  
    const size_t i_l_cb_l_1 = K1_L_NUM_CB_L_1;
    #if K1_L_NUM_CB_L_2 > 0
    for (size_t i_l_cb_l_2 = 0; i_l_cb_l_2 < K1_L_NUM_CB_L_2; ++i_l_cb_l_2) {
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI; ++step) {
        const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      #if K1_L_CB_SIZE_IN % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_IN % K1_L_FLAT_NUM_WI) {
        const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      if (l_cb_l_1 == 0 && l_cb_l_2 == 0) {
        K1_L_MEM_WEIGHTS(0, 0) = K1_G_MEM_WEIGHTS(K1_L_CB_OFFSET_L_1 + 0, K1_L_CB_OFFSET_L_2 + 0);
      
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      
      // move results upwards in memory hierarchy
      #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
      #if L_CB_RES_DEST_LEVEL < LOCAL
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      #else
      #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI) {
        const size_t flat_index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
      #endif
    } // end of "i_l_cb_l_2"-loop
    #endif
    // post process whole extra cache blocks in dimension L_2
    #if K1_L_NUM_EXTRA_CB_L_2 > 0
    if (K1_G_FU_ID_L_2 < K1_L_NUM_EXTRA_CB_L_2) {  
      const size_t i_l_cb_l_2 = K1_L_NUM_CB_L_2;
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI; ++step) {
        const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      #if K1_L_CB_SIZE_IN % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < K1_L_CB_SIZE_IN % K1_L_FLAT_NUM_WI) {
        const size_t index = K1_L_FLAT_WI_ID + (K1_L_CB_SIZE_IN / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      if (l_cb_l_1 == 0 && l_cb_l_2 == 0) {
        K1_L_MEM_WEIGHTS(0, 0) = K1_G_MEM_WEIGHTS(K1_L_CB_OFFSET_L_1 + 0, K1_L_CB_OFFSET_L_2 + 0);
      
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      
      // move results upwards in memory hierarchy
      #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
      #if L_CB_RES_DEST_LEVEL < LOCAL
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      #else
      #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI) {
        const size_t flat_index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
      #endif
    } // end of post processing whole extra cache blocks in dimension L_2
    #endif
    // post process single extra incomplete cache block in dimension L_2
    #if K1_L_NUM_EXTRA_ELEMS_L_2 > 0
    if (K1_G_FU_ID_L_2 == K1_L_NUM_EXTRA_CB_L_2 % K1_G_NUM_FU_L_2) {  
      const size_t i_l_cb_l_2 = K1_L_NUM_CB_L_2;
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      #if (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_L_FLAT_NUM_WI) {
        const size_t index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      if (l_cb_l_1 == 0 && l_cb_l_2 == 0) {
        K1_L_MEM_WEIGHTS(0, 0) = K1_G_MEM_WEIGHTS(K1_L_CB_OFFSET_L_1 + 0, K1_L_CB_OFFSET_L_2 + 0);
      
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      
      // move results upwards in memory hierarchy
      #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
      #if L_CB_RES_DEST_LEVEL < LOCAL
      #if K1_P_NUM_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      #else
      #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if (K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) % K1_L_FLAT_NUM_WI) {
        const size_t flat_index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_SIZE_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
      #endif
    }// end of post process single extra incomplete cache block in dimension L_2
    #endif
  } // end of post processing whole extra cache blocks in dimension L_1
  #endif
  // post process single extra incomplete cache block in dimension L_1
  #if K1_L_NUM_EXTRA_ELEMS_L_1 > 0
  if (K1_G_FU_ID_L_1 == K1_L_NUM_EXTRA_CB_L_1 % K1_G_NUM_FU_L_1) {  
    const size_t i_l_cb_l_1 = K1_L_NUM_CB_L_1;
    #if K1_L_NUM_CB_L_2 > 0
    for (size_t i_l_cb_l_2 = 0; i_l_cb_l_2 < K1_L_NUM_CB_L_2; ++i_l_cb_l_2) {
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      #if (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_L_FLAT_NUM_WI) {
        const size_t index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      if (l_cb_l_1 == 0 && l_cb_l_2 == 0) {
        K1_L_MEM_WEIGHTS(0, 0) = K1_G_MEM_WEIGHTS(K1_L_CB_OFFSET_L_1 + 0, K1_L_CB_OFFSET_L_2 + 0);
      
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      
      // move results upwards in memory hierarchy
      #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
      #if L_CB_RES_DEST_LEVEL < LOCAL
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      #else
      #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI) {
        const size_t flat_index = K1_L_FLAT_WI_ID + ((K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
      #endif
    } // end of "i_l_cb_l_2"-loop
    #endif
    // post process whole extra cache blocks in dimension L_2
    #if K1_L_NUM_EXTRA_CB_L_2 > 0
    if (K1_G_FU_ID_L_2 < K1_L_NUM_EXTRA_CB_L_2) {  
      const size_t i_l_cb_l_2 = K1_L_NUM_CB_L_2;
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      #if (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_L_FLAT_NUM_WI) {
        const size_t index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_SIZE_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_SIZE_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      if (l_cb_l_1 == 0 && l_cb_l_2 == 0) {
        K1_L_MEM_WEIGHTS(0, 0) = K1_G_MEM_WEIGHTS(K1_L_CB_OFFSET_L_1 + 0, K1_L_CB_OFFSET_L_2 + 0);
      
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      
      // move results upwards in memory hierarchy
      #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
      #if L_CB_RES_DEST_LEVEL < LOCAL
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_L_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      #else
      #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_SIZE_L_2) % K1_L_FLAT_NUM_WI) {
        const size_t flat_index = K1_L_FLAT_WI_ID + ((K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_SIZE_L_2) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_SIZE_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_SIZE_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_SIZE_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_SIZE_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
      #endif
    } // end of post processing whole extra cache blocks in dimension L_2
    #endif
    // post process single extra incomplete cache block in dimension L_2
    #if K1_L_NUM_EXTRA_ELEMS_L_2 > 0
    if (K1_G_FU_ID_L_2 == K1_L_NUM_EXTRA_CB_L_2 % K1_G_NUM_FU_L_2) {  
      const size_t i_l_cb_l_2 = K1_L_NUM_CB_L_2;
      // ---------- L caching --------------------
      #if CACHE_L_CB != 0
      #if (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      #if (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_L_FLAT_NUM_WI) {
        const size_t index = K1_L_FLAT_WI_ID + ((K1_L_CB_SIZE_IN / (2 + K1_L_CB_SIZE_L_1 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_L_CB_SIZE_L_2 + 2) * (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        const size_t i_l_elem_l_1 = flat_index / ((2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
        const size_t i_l_elem_l_2 = flat_index % (2 + K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
        K1_L_MEM_IN(i_l_elem_l_1 - 2, i_l_elem_l_2 - 2) = K1_G_MEM_IN(K1_L_CB_OFFSET_L_1 + i_l_elem_l_1 - 2, K1_L_CB_OFFSET_L_2 + i_l_elem_l_2 - 2);
      }
      #endif
      if (l_cb_l_1 == 0 && l_cb_l_2 == 0) {
        K1_L_MEM_WEIGHTS(0, 0) = K1_G_MEM_WEIGHTS(K1_L_CB_OFFSET_L_1 + 0, K1_L_CB_OFFSET_L_2 + 0);
      
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      // ---------- end of L caching -------------
      
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < K1_P_CB_SIZE_IN % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + (K1_P_CB_SIZE_IN / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_SIZE_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_SIZE_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          // ---------- P caching --------------------
          #if CACHE_P_CB != 0
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI > 0
          for (size_t step = 0; step < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI; ++step) {
            const size_t index = K1_P_FLAT_WI_ID + step * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          #if (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI > 0
          if (K1_P_FLAT_WI_ID < (K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) % K1_P_FLAT_NUM_WI) {
            const size_t index = K1_P_FLAT_WI_ID + ((K1_P_CB_SIZE_IN / (2 + K1_P_CB_SIZE_L_1 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 + 2) / (2 + K1_P_CB_SIZE_L_2 + 2) * (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2)) / K1_P_FLAT_NUM_WI) * K1_P_FLAT_NUM_WI;
            #if CACHE_L_CB != 0
            const size_t BUFFER_IN_INDEX_1(i_p_elem_l_1, i_p_elem_l_2) = flat_index / (BUFFER_IN_P_SIZE_0);
            const size_t BUFFER_IN_INDEX_0(i_p_elem_l_1, i_p_elem_l_2) = flat_index % BUFFER_IN_P_SIZE_0;
            #else
            const size_t i_p_elem_l_1 = flat_index / ((2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2));
            const size_t i_p_elem_l_2 = flat_index % (2 + K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2 + 2);
            #endif
            K1_P_MEM_IN(i_p_elem_l_1 - 2, i_p_elem_l_2 - 2) = K1_L_MEM_IN(K1_P_CB_OFFSET_L_1 + i_p_elem_l_1 - 2, K1_P_CB_OFFSET_L_2 + i_p_elem_l_2 - 2);
          }
          #endif
          if (p_cb_l_1 == 0 && p_cb_l_2 == 0) {
            K1_P_MEM_WEIGHTS(0, 0) = K1_L_MEM_WEIGHTS(K1_P_CB_OFFSET_L_1 + 0, K1_P_CB_OFFSET_L_2 + 0);
          
          }
          #endif
          // ---------- end of P caching -------------
          
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              // process one mda element
              K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = f(
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -2, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + -1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 0, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 1, i_p_elem_l_2 + 2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -2),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + -1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 0),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 1),
                  K1_P_MEM_IN(i_p_elem_l_1 + 2, i_p_elem_l_2 + 2),
                  K1_P_MEM_WEIGHTS(i_p_elem_l_1, i_p_elem_l_2)
              );
            
            }
          }
          
          // move results upwards in memory hierarchy
          #if L_CB_RES_DEST_LEVEL > P_CB_RES_DEST_LEVEL
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_P_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
          
          #endif
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      // wait for all WIs to finish computation on local cache block
      #if CACHE_L_CB != 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #endif
      
      // move results upwards in memory hierarchy
      #if G_CB_RES_DEST_LEVEL > L_CB_RES_DEST_LEVEL
      #if L_CB_RES_DEST_LEVEL < LOCAL
      #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      for (size_t i_p_cb_l_1 = 0; i_p_cb_l_1 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1; ++i_p_cb_l_1) {
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of "i_p_cb_l_1"-loop
      #endif
      // post process whole extra cache blocks in dimension L_1
      #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_SIZE_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      } // end of post processing whole extra cache blocks in dimension L_1
      #endif
      // post process single extra incomplete cache block in dimension L_1
      #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 > 0
      if (K1_L_FU_ID_L_1 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1 % K1_L_NUM_FU_L_1) {  
        const size_t i_p_cb_l_1 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_1;
        #if K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        for (size_t i_p_cb_l_2 = 0; i_p_cb_l_2 < K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2; ++i_p_cb_l_2) {
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of "i_p_cb_l_2"-loop
        #endif
        // post process whole extra cache blocks in dimension L_2
        #if K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 < K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_SIZE_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        } // end of post processing whole extra cache blocks in dimension L_2
        #endif
        // post process single extra incomplete cache block in dimension L_2
        #if K1_P_NUM_EXTRA_ELEMS_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 > 0
        if (K1_L_FU_ID_L_2 == K1_P_NUM_EXTRA_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2 % K1_L_NUM_FU_L_2) {  
          const size_t i_p_cb_l_2 = K1_P_NUM_CB_IN_INCOMPLETE_L_CB_IN_COMPLETE_G_CB_L_2;
          for (size_t i_p_elem_l_1 = 0; i_p_elem_l_1 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1; ++i_p_elem_l_1) {
            for (size_t i_p_elem_l_2 = 0; i_p_elem_l_2 < K1_P_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2; ++i_p_elem_l_2) {
              K1_G_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2) = K1_L_CB_RES_DEST(i_p_elem_l_1, i_p_elem_l_2);
            }
          }
        }// end of post process single extra incomplete cache block in dimension L_2
        #endif
      }// end of post process single extra incomplete cache block in dimension L_1
      #endif
      
      #else
      #if L_CB_RES_DEST_LEVEL <= LOCAL && CACHE_L_CB == 0
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #if (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) / K1_L_FLAT_NUM_WI > 0
      for (size_t step = 0; step < (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) / K1_L_FLAT_NUM_WI; ++step) {
        const size_t flat_index = K1_L_FLAT_WI_ID + step * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) % K1_L_FLAT_NUM_WI > 0
      if (K1_L_FLAT_WI_ID < (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) % K1_L_FLAT_NUM_WI) {
        const size_t flat_index = K1_L_FLAT_WI_ID + ((K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1 * K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2) / K1_L_FLAT_NUM_WI) * K1_L_FLAT_NUM_WI;
        #if K2_P_NUM_FU_L_1 == 1
        const size_t l_index_l_1 = flat_index / (K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        const size_t l_index_l_2 = flat_index % K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2;
        #else
        const size_t DESCENDING_L_DIMS_1(l_index_l_1, l_index_l_2) = flat_index / (DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2));
        const size_t DESCENDING_L_DIMS_0(l_index_l_1, l_index_l_2) = flat_index % DESCENDING_L_DIMS_0(K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_1, K1_L_CB_REDUCED_SIZE_IN_COMPLETE_G_CB_L_2);
        #endif
        K1_L_KERNEL_RES(l_index_l_1, l_index_l_2) =
        #if L_CB_RES_DEST_LEVEL < LOCAL
          K1_L_REDUCTION_MEM(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == LOCAL
          K1_RES_L_BUFFER(l_index_l_1, l_index_l_2);
        #elif L_CB_RES_DEST_LEVEL == GLOBAL
          K1_RES_G_BUFFER(K1_L_CB_OFFSET_L_1 + l_index_l_1, K1_L_CB_OFFSET_L_2 + l_index_l_2);
        #endif
      }
      #endif
      #if L_CB_RES_DEST_LEVEL <= LOCAL
      barrier(CLK_LOCAL_MEM_FENCE);
      #elif L_CB_RES_DEST_LEVEL == GLOBAL
      barrier(CLK_GLOBAL_MEM_FENCE);
      #endif
      #endif
      #endif
    }// end of post process single extra incomplete cache block in dimension L_2
    #endif
  }// end of post process single extra incomplete cache block in dimension L_1
  #endif
}
// =============== end of kernel 1 ============================================