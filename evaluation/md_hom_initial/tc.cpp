#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <atf.h>
#include <json.hpp>

void tune(size_t platform_id, size_t device_id, atf::cf::device_info &device_info, const std::string &device_type, float tuning_time,
          const std::string &a_dims, const std::string &b_dims,
          size_t M_1, size_t M_2, size_t M_3, size_t M_4, size_t M_5, size_t M_6, size_t N_1,
          const std::vector<float> &a, const std::vector<float> &b, const std::vector<float> &c) {
    // get device information
    std::vector<size_t> max_wi_size;
    size_t max_wg_size = 0;
    device_info.get_device_limits(&max_wi_size, &max_wg_size);

    // define search space
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {int(0)});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {int(0)});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {int(2)});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {int(1)}, atf::less_than_or_eq(G_CB_RES_DEST_LEVEL));
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {int(0)}, atf::less_than_or_eq(L_CB_RES_DEST_LEVEL));

    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {int(6)});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {int(5)}, atf::unequal(OCL_DIM_L_1));
    auto OCL_DIM_L_3         = atf::tp("OCL_DIM_L_3",         {int(4)}, atf::unequal(OCL_DIM_L_1) && atf::unequal(OCL_DIM_L_2));
    auto OCL_DIM_L_4         = atf::tp("OCL_DIM_L_4",         {int(3)}, atf::unequal(OCL_DIM_L_1) && atf::unequal(OCL_DIM_L_2) && atf::unequal(OCL_DIM_L_3));
    auto OCL_DIM_L_5         = atf::tp("OCL_DIM_L_5",         {int(2)}, atf::unequal(OCL_DIM_L_1) && atf::unequal(OCL_DIM_L_2) && atf::unequal(OCL_DIM_L_3) && atf::unequal(OCL_DIM_L_4));
    auto OCL_DIM_L_6         = atf::tp("OCL_DIM_L_6",         {int(1)}, atf::unequal(OCL_DIM_L_1) && atf::unequal(OCL_DIM_L_2) && atf::unequal(OCL_DIM_L_3) && atf::unequal(OCL_DIM_L_4) && atf::unequal(OCL_DIM_L_5));
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {int(0)}, atf::unequal(OCL_DIM_L_1) && atf::unequal(OCL_DIM_L_2) && atf::unequal(OCL_DIM_L_3) && atf::unequal(OCL_DIM_L_4) && atf::unequal(OCL_DIM_L_5) && atf::unequal(OCL_DIM_L_6));

    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          atf::interval<int>(1, M_1), atf::divides(M_1));
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          atf::interval<int>(1, M_1), atf::divides(M_1 / NUM_WG_L_1) && atf::less_than_or_eq(M_1 + NUM_WG_L_1 - 1 / NUM_WG_L_1));
    auto G_CB_SIZE_L_1       = atf::tp("G_CB_SIZE_L_1",       {int(M_1)});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       atf::interval<int>(1, M_1), atf::equal((M_1 + NUM_WG_L_1 - 1) / NUM_WG_L_1));
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       {int(1)});

    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          atf::interval<int>(1, M_2), atf::divides(M_2));
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          atf::interval<int>(1, M_2), atf::divides(M_2 / NUM_WG_L_2) && atf::less_than_or_eq(M_2 + NUM_WG_L_2 - 1 / NUM_WG_L_2));
    auto G_CB_SIZE_L_2       = atf::tp("G_CB_SIZE_L_2",       {int(M_2)});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       atf::interval<int>(1, M_2), atf::equal((M_2 + NUM_WG_L_2 - 1) / NUM_WG_L_2));
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       {int(1)});

    auto NUM_WG_L_3          = atf::tp("NUM_WG_L_3",          atf::interval<int>(1, M_3), atf::divides(M_3));
    auto NUM_WI_L_3          = atf::tp("NUM_WI_L_3",          atf::interval<int>(1, M_3), atf::divides(M_3 / NUM_WG_L_3) && atf::less_than_or_eq(M_3 + NUM_WG_L_3 - 1 / NUM_WG_L_3));
    auto G_CB_SIZE_L_3       = atf::tp("G_CB_SIZE_L_3",       {int(M_3)});
    auto L_CB_SIZE_L_3       = atf::tp("L_CB_SIZE_L_3",       atf::interval<int>(1, M_3), atf::equal((M_3 + NUM_WG_L_3 - 1) / NUM_WG_L_3));
    auto P_CB_SIZE_L_3       = atf::tp("P_CB_SIZE_L_3",       {int(1)});

    auto NUM_WG_L_4          = atf::tp("NUM_WG_L_4",          atf::interval<int>(1, M_4), atf::divides(M_4));
    auto NUM_WI_L_4          = atf::tp("NUM_WI_L_4",          atf::interval<int>(1, M_4), atf::divides(M_4 / NUM_WG_L_4) && atf::less_than_or_eq(M_4 + NUM_WG_L_4 - 1 / NUM_WG_L_4));
    auto G_CB_SIZE_L_4       = atf::tp("G_CB_SIZE_L_4",       {int(M_4)});
    auto L_CB_SIZE_L_4       = atf::tp("L_CB_SIZE_L_4",       atf::interval<int>(1, M_4), atf::equal((M_4 + NUM_WG_L_4 - 1) / NUM_WG_L_4));
    auto P_CB_SIZE_L_4       = atf::tp("P_CB_SIZE_L_4",       {int(1)});

    auto NUM_WG_L_5          = atf::tp("NUM_WG_L_5",          atf::interval<int>(1, M_5), atf::divides(M_5));
    auto NUM_WI_L_5          = atf::tp("NUM_WI_L_5",          atf::interval<int>(1, M_5), atf::divides(M_5 / NUM_WG_L_5) && atf::less_than_or_eq(M_5 + NUM_WG_L_5 - 1 / NUM_WG_L_5));
    auto G_CB_SIZE_L_5       = atf::tp("G_CB_SIZE_L_5",       {int(M_5)});
    auto L_CB_SIZE_L_5       = atf::tp("L_CB_SIZE_L_5",       atf::interval<int>(1, M_5), atf::equal((M_5 + NUM_WG_L_5 - 1) / NUM_WG_L_5));
    auto P_CB_SIZE_L_5       = atf::tp("P_CB_SIZE_L_5",       {int(1)});

    auto NUM_WG_L_6          = atf::tp("NUM_WG_L_6",          atf::interval<int>(1, M_6), atf::divides(M_6));
    auto NUM_WI_L_6          = atf::tp("NUM_WI_L_6",          atf::interval<int>(1, M_6), atf::divides(M_6 / NUM_WG_L_6) && atf::less_than_or_eq(M_6 + NUM_WG_L_6 - 1 / NUM_WG_L_6));
    auto G_CB_SIZE_L_6       = atf::tp("G_CB_SIZE_L_6",       {int(M_6)});
    auto L_CB_SIZE_L_6       = atf::tp("L_CB_SIZE_L_6",       atf::interval<int>(1, M_6), atf::equal((M_6 + NUM_WG_L_6 - 1) / NUM_WG_L_6));
    auto P_CB_SIZE_L_6       = atf::tp("P_CB_SIZE_L_6",       {int(1)});

    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          atf::interval<int>(1, 1), atf::divides(N_1));
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          atf::interval<int>(1, N_1), atf::divides(N_1 / NUM_WG_R_1) && atf::less_than_or_eq(N_1 + NUM_WG_R_1 - 1 / NUM_WG_R_1));
    auto G_CB_SIZE_R_1       = atf::tp("G_CB_SIZE_R_1",       {int(N_1)});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       atf::interval<int>(1, N_1), atf::equal((N_1 + NUM_WG_R_1 - 1) / NUM_WG_R_1));
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       {int(1)});

    // prepare tuner
    auto tuner = atf::open_tuner(new atf::cond::duration<std::chrono::seconds>(tuning_time * 3600));
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_L_3, OCL_DIM_L_4, OCL_DIM_L_5, OCL_DIM_L_6, OCL_DIM_R_1)
            (NUM_WG_L_1, NUM_WI_L_1, G_CB_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1)
            (NUM_WG_L_2, NUM_WI_L_2, G_CB_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2)
            (NUM_WG_L_3, NUM_WI_L_3, G_CB_SIZE_L_3, L_CB_SIZE_L_3, P_CB_SIZE_L_3)
            (NUM_WG_L_4, NUM_WI_L_4, G_CB_SIZE_L_4, L_CB_SIZE_L_4, P_CB_SIZE_L_4)
            (NUM_WG_L_5, NUM_WI_L_5, G_CB_SIZE_L_5, L_CB_SIZE_L_5, P_CB_SIZE_L_5)
            (NUM_WG_L_6, NUM_WI_L_6, G_CB_SIZE_L_6, L_CB_SIZE_L_6, P_CB_SIZE_L_6)
            (NUM_WG_R_1, NUM_WI_R_1, G_CB_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1);


    // prepare kernel
    auto is_valid = [&](atf::configuration &config) -> bool {
        size_t num_wi[] = {1, 1, 1};
        if (config[OCL_DIM_L_1.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_L_1.name()].value().int_val();
        }
        if (config[OCL_DIM_L_2.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_L_2.name()].value().int_val();
        }
        if (config[OCL_DIM_L_3.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_L_3.name()].value().int_val();
        }
        if (config[OCL_DIM_L_4.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_L_4.name()].value().int_val();
        }
        if (config[OCL_DIM_L_5.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_L_5.name()].value().int_val();
        }
        if (config[OCL_DIM_L_6.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_L_6.name()].value().int_val();
        }
        if (config[OCL_DIM_R_1.name()].value().int_val() == 0) {
            num_wi[0] *= config[NUM_WI_R_1.name()].value().int_val();
        }
        if (config[OCL_DIM_L_1.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_L_1.name()].value().int_val();
        }
        if (config[OCL_DIM_L_2.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_L_2.name()].value().int_val();
        }
        if (config[OCL_DIM_L_3.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_L_3.name()].value().int_val();
        }
        if (config[OCL_DIM_L_4.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_L_4.name()].value().int_val();
        }
        if (config[OCL_DIM_L_5.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_L_5.name()].value().int_val();
        }
        if (config[OCL_DIM_L_6.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_L_6.name()].value().int_val();
        }
        if (config[OCL_DIM_R_1.name()].value().int_val() == 1) {
            num_wi[1] *= config[NUM_WI_R_1.name()].value().int_val();
        }
        if (config[OCL_DIM_L_1.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_L_1.name()].value().int_val();
        }
        if (config[OCL_DIM_L_2.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_L_2.name()].value().int_val();
        }
        if (config[OCL_DIM_L_3.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_L_3.name()].value().int_val();
        }
        if (config[OCL_DIM_L_4.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_L_4.name()].value().int_val();
        }
        if (config[OCL_DIM_L_5.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_L_5.name()].value().int_val();
        }
        if (config[OCL_DIM_L_6.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_L_6.name()].value().int_val();
        }
        if (config[OCL_DIM_R_1.name()].value().int_val() >= 2) {
            num_wi[2] *= config[NUM_WI_R_1.name()].value().int_val();
        }
        return num_wi[0] <= max_wi_size[0] && num_wi[1] <= max_wi_size[1] && num_wi[2] <= max_wi_size[2] && num_wi[0] * num_wi[1] * num_wi[2] <= max_wg_size;
    };
    size_t res_g_l_size = M_1 * M_2 * M_3 * M_4 * M_5 * M_6 * sizeof(float);
    auto res_g_size =
            [&](unsigned int kernel, atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                if (kernel == 1) {
                    if (config[G_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WG_R_1.name()].value().int_val();
                    }
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                } else {
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= std::min(config[NUM_WI_R_1.name()].value().int_val(), atf::ceil_lambda(std::min(config[L_CB_SIZE_R_1.name()].value().int_val(), std::min(config[NUM_WG_R_1.name()].value().int_val(), atf::ceil_lambda(config[G_CB_SIZE_R_1.name()].value().int_val(), config[L_CB_SIZE_R_1.name()].value().int_val()))), std::min(config[P_CB_SIZE_R_1.name()].value().int_val(), std::min(config[L_CB_SIZE_R_1.name()].value().int_val(), std::min(config[NUM_WG_R_1.name()].value().int_val(), atf::ceil_lambda(config[G_CB_SIZE_R_1.name()].value().int_val(), config[L_CB_SIZE_R_1.name()].value().int_val()))))));
                    }
                }
                if (config[P_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                    size *= 1;
                }
                return size;
            };
    auto int_res_size =
            [&](atf::configuration &config) -> size_t {
                size_t size = res_g_l_size * config[NUM_WG_R_1.name()].value().int_val();
                return size;
            };
    std::vector<float> int_res(res_g_l_size / sizeof(float)); for (int i = 0; i < int_res.size(); ++i) int_res[i] = 0;
    auto needs_second_kernel =
            [&](atf::configuration &config) -> bool {
                return config[NUM_WG_R_1.name()].value().int_val() > 1;
            };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WG_L_5) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WG_L_6) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WG_R_1) * (NUM_WI_R_1), 1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WG_L_5) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WG_L_6) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))), 1)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))), 1)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::LOCAL,
            "../../extern/ATF/ocl_md_hom_process_wrapper_float",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "kernel/tc_abcdef_" + a_dims + "_" + b_dims + "_1.cl", "tc_1", " -DTYPE_T=float -DTYPE_TS=float"},
            atf::inputs(atf::buffer(a), atf::buffer(b)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "kernel/tc_abcdef_" + a_dims + "_" + b_dims + "_2.cl", "tc_2"},
            atf::inputs(atf::buffer(c)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            5, 3,
            false,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );

    // tune
    auto best_configuration = tuner(kernel);

    // store best found configuration if valid
    if (tuner.number_of_valid_evaluated_configs() > 0) {
        std::map<std::string, int> plain_config;
        for (const auto &tp : best_configuration) {
            plain_config[tp.first] = tp.second.value().int_val();
        }
        std::string config_file_dir = std::getenv("ARTIFACT_ROOT");
        config_file_dir.append("/results/")
                .append(device_type).append("/")
                .append(std::to_string(platform_id)).append("/")
                .append(std::to_string(device_id)).append("/md_hom_initial/");
        std::string config_file_name = config_file_dir;
        config_file_name.append("tc_abcdef_" + a_dims + "_" + b_dims + "_config.json");
        int ret = system(std::string("mkdir -p ").append(config_file_dir).c_str());
        if (ret != 0) {
            std::cerr << "Failed to create results directory." << std::endl;
            exit(EXIT_FAILURE);
        }
        std::ofstream config_file(config_file_name, std::ios::out | std::ios::trunc);
        config_file << std::setw(4) << nlohmann::json(plain_config);
        config_file.close();
    } else {
        std::cerr << "Tuning failed." << std::endl;
        exit(EXIT_FAILURE);
    }
}

void bench(size_t platform_id, size_t device_id, atf::cf::device_info &device_info, const std::string &device_type,
           const std::string &a_dims, const std::string &b_dims,
           size_t M_1, size_t M_2, size_t M_3, size_t M_4, size_t M_5, size_t M_6, size_t N_1,
           const std::vector<float> &a, const std::vector<float> &b, const std::vector<float> &c) {
    // read configuration
    std::string config_file_dir = std::getenv("ARTIFACT_ROOT");
    config_file_dir.append("/results/")
            .append(device_type).append("/")
            .append(std::to_string(platform_id)).append("/")
            .append(std::to_string(device_id)).append("/md_hom_initial/");
    std::string config_file_name = config_file_dir;
    config_file_name.append("tc_abcdef_" + a_dims + "_" + b_dims + "_config.json");
    std::ifstream config_file(config_file_name, std::ios::in);
    if (config_file.fail()) {
        std::cerr << "Unable to open configuration file. Has md_hom not yet been tuned for this input size?" << std::endl;
        exit(EXIT_FAILURE);
    }
    auto plain_config = nlohmann::json::parse(config_file);

    // set configuration for kernel execution
    auto CACHE_L_CB          = atf::tp("CACHE_L_CB",          {plain_config.at("CACHE_L_CB").get<int>()});
    auto CACHE_P_CB          = atf::tp("CACHE_P_CB",          {plain_config.at("CACHE_P_CB").get<int>()});
    auto G_CB_RES_DEST_LEVEL = atf::tp("G_CB_RES_DEST_LEVEL", {plain_config.at("G_CB_RES_DEST_LEVEL").get<int>()});
    auto L_CB_RES_DEST_LEVEL = atf::tp("L_CB_RES_DEST_LEVEL", {plain_config.at("L_CB_RES_DEST_LEVEL").get<int>()});
    auto P_CB_RES_DEST_LEVEL = atf::tp("P_CB_RES_DEST_LEVEL", {plain_config.at("P_CB_RES_DEST_LEVEL").get<int>()});
    auto OCL_DIM_L_1         = atf::tp("OCL_DIM_L_1",         {plain_config.at("OCL_DIM_L_1").get<int>()});
    auto OCL_DIM_L_2         = atf::tp("OCL_DIM_L_2",         {plain_config.at("OCL_DIM_L_2").get<int>()});
    auto OCL_DIM_L_3         = atf::tp("OCL_DIM_L_3",         {plain_config.at("OCL_DIM_L_3").get<int>()});
    auto OCL_DIM_L_4         = atf::tp("OCL_DIM_L_4",         {plain_config.at("OCL_DIM_L_4").get<int>()});
    auto OCL_DIM_L_5         = atf::tp("OCL_DIM_L_5",         {plain_config.at("OCL_DIM_L_5").get<int>()});
    auto OCL_DIM_L_6         = atf::tp("OCL_DIM_L_6",         {plain_config.at("OCL_DIM_L_6").get<int>()});
    auto OCL_DIM_R_1         = atf::tp("OCL_DIM_R_1",         {plain_config.at("OCL_DIM_R_1").get<int>()});
    auto G_CB_SIZE_L_1       = atf::tp("G_CB_SIZE_L_1",       {plain_config.at("G_CB_SIZE_L_1").get<int>()});
    auto L_CB_SIZE_L_1       = atf::tp("L_CB_SIZE_L_1",       {plain_config.at("L_CB_SIZE_L_1").get<int>()});
    auto P_CB_SIZE_L_1       = atf::tp("P_CB_SIZE_L_1",       {plain_config.at("P_CB_SIZE_L_1").get<int>()});
    auto NUM_WG_L_1          = atf::tp("NUM_WG_L_1",          {plain_config.at("NUM_WG_L_1").get<int>()});
    auto NUM_WI_L_1          = atf::tp("NUM_WI_L_1",          {plain_config.at("NUM_WI_L_1").get<int>()});
    auto G_CB_SIZE_L_2       = atf::tp("G_CB_SIZE_L_2",       {plain_config.at("G_CB_SIZE_L_2").get<int>()});
    auto L_CB_SIZE_L_2       = atf::tp("L_CB_SIZE_L_2",       {plain_config.at("L_CB_SIZE_L_2").get<int>()});
    auto P_CB_SIZE_L_2       = atf::tp("P_CB_SIZE_L_2",       {plain_config.at("P_CB_SIZE_L_2").get<int>()});
    auto NUM_WG_L_2          = atf::tp("NUM_WG_L_2",          {plain_config.at("NUM_WG_L_2").get<int>()});
    auto NUM_WI_L_2          = atf::tp("NUM_WI_L_2",          {plain_config.at("NUM_WI_L_2").get<int>()});
    auto G_CB_SIZE_L_3       = atf::tp("G_CB_SIZE_L_3",       {plain_config.at("G_CB_SIZE_L_3").get<int>()});
    auto L_CB_SIZE_L_3       = atf::tp("L_CB_SIZE_L_3",       {plain_config.at("L_CB_SIZE_L_3").get<int>()});
    auto P_CB_SIZE_L_3       = atf::tp("P_CB_SIZE_L_3",       {plain_config.at("P_CB_SIZE_L_3").get<int>()});
    auto NUM_WG_L_3          = atf::tp("NUM_WG_L_3",          {plain_config.at("NUM_WG_L_3").get<int>()});
    auto NUM_WI_L_3          = atf::tp("NUM_WI_L_3",          {plain_config.at("NUM_WI_L_3").get<int>()});
    auto G_CB_SIZE_L_4       = atf::tp("G_CB_SIZE_L_4",       {plain_config.at("G_CB_SIZE_L_4").get<int>()});
    auto L_CB_SIZE_L_4       = atf::tp("L_CB_SIZE_L_4",       {plain_config.at("L_CB_SIZE_L_4").get<int>()});
    auto P_CB_SIZE_L_4       = atf::tp("P_CB_SIZE_L_4",       {plain_config.at("P_CB_SIZE_L_4").get<int>()});
    auto NUM_WG_L_4          = atf::tp("NUM_WG_L_4",          {plain_config.at("NUM_WG_L_4").get<int>()});
    auto NUM_WI_L_4          = atf::tp("NUM_WI_L_4",          {plain_config.at("NUM_WI_L_4").get<int>()});
    auto G_CB_SIZE_L_5       = atf::tp("G_CB_SIZE_L_5",       {plain_config.at("G_CB_SIZE_L_5").get<int>()});
    auto L_CB_SIZE_L_5       = atf::tp("L_CB_SIZE_L_5",       {plain_config.at("L_CB_SIZE_L_5").get<int>()});
    auto P_CB_SIZE_L_5       = atf::tp("P_CB_SIZE_L_5",       {plain_config.at("P_CB_SIZE_L_5").get<int>()});
    auto NUM_WG_L_5          = atf::tp("NUM_WG_L_5",          {plain_config.at("NUM_WG_L_5").get<int>()});
    auto NUM_WI_L_5          = atf::tp("NUM_WI_L_5",          {plain_config.at("NUM_WI_L_5").get<int>()});
    auto G_CB_SIZE_L_6       = atf::tp("G_CB_SIZE_L_6",       {plain_config.at("G_CB_SIZE_L_6").get<int>()});
    auto L_CB_SIZE_L_6       = atf::tp("L_CB_SIZE_L_6",       {plain_config.at("L_CB_SIZE_L_6").get<int>()});
    auto P_CB_SIZE_L_6       = atf::tp("P_CB_SIZE_L_6",       {plain_config.at("P_CB_SIZE_L_6").get<int>()});
    auto NUM_WG_L_6          = atf::tp("NUM_WG_L_6",          {plain_config.at("NUM_WG_L_6").get<int>()});
    auto NUM_WI_L_6          = atf::tp("NUM_WI_L_6",          {plain_config.at("NUM_WI_L_6").get<int>()});
    auto G_CB_SIZE_R_1       = atf::tp("G_CB_SIZE_R_1",       {plain_config.at("G_CB_SIZE_R_1").get<int>()});
    auto L_CB_SIZE_R_1       = atf::tp("L_CB_SIZE_R_1",       {plain_config.at("L_CB_SIZE_R_1").get<int>()});
    auto P_CB_SIZE_R_1       = atf::tp("P_CB_SIZE_R_1",       {plain_config.at("P_CB_SIZE_R_1").get<int>()});
    auto NUM_WG_R_1          = atf::tp("NUM_WG_R_1",          {plain_config.at("NUM_WG_R_1").get<int>()});
    auto NUM_WI_R_1          = atf::tp("NUM_WI_R_1",          {plain_config.at("NUM_WI_R_1").get<int>()});
    auto tuner = atf::exhaustive();
    tuner(CACHE_L_CB)(CACHE_P_CB)(G_CB_RES_DEST_LEVEL, L_CB_RES_DEST_LEVEL, P_CB_RES_DEST_LEVEL)
            (OCL_DIM_L_1, OCL_DIM_L_2, OCL_DIM_L_3, OCL_DIM_L_4, OCL_DIM_L_5, OCL_DIM_L_6, OCL_DIM_R_1)
            (G_CB_SIZE_L_1, L_CB_SIZE_L_1, P_CB_SIZE_L_1, NUM_WG_L_1, NUM_WI_L_1)
            (G_CB_SIZE_L_2, L_CB_SIZE_L_2, P_CB_SIZE_L_2, NUM_WG_L_2, NUM_WI_L_2)
            (G_CB_SIZE_L_3, L_CB_SIZE_L_3, P_CB_SIZE_L_3, NUM_WG_L_3, NUM_WI_L_3)
            (G_CB_SIZE_L_4, L_CB_SIZE_L_4, P_CB_SIZE_L_4, NUM_WG_L_4, NUM_WI_L_4)
            (G_CB_SIZE_L_5, L_CB_SIZE_L_5, P_CB_SIZE_L_5, NUM_WG_L_5, NUM_WI_L_5)
            (G_CB_SIZE_L_6, L_CB_SIZE_L_6, P_CB_SIZE_L_6, NUM_WG_L_6, NUM_WI_L_6)
            (G_CB_SIZE_R_1, L_CB_SIZE_R_1, P_CB_SIZE_R_1, NUM_WG_R_1, NUM_WI_R_1);

    // prepare kernel
    auto is_valid = [&](atf::configuration &config) -> bool {
        return true;
    };
    size_t res_g_l_size = M_1 * M_2 * M_3 * M_4 * M_5 * M_6 * sizeof(float);
    auto res_g_size =
            [&](unsigned int kernel, atf::configuration &config) -> size_t {
                size_t size = res_g_l_size;
                if (kernel == 1) {
                    if (config[G_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WG_R_1.name()].value().int_val();
                    }
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= config[NUM_WI_R_1.name()].value().int_val();
                    }
                } else {
                    if (config[L_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                        size *= std::min(config[NUM_WI_R_1.name()].value().int_val(), atf::ceil_lambda(std::min(config[L_CB_SIZE_R_1.name()].value().int_val(), std::min(config[NUM_WG_R_1.name()].value().int_val(), atf::ceil_lambda(config[G_CB_SIZE_R_1.name()].value().int_val(), config[L_CB_SIZE_R_1.name()].value().int_val()))), std::min(config[P_CB_SIZE_R_1.name()].value().int_val(), std::min(config[L_CB_SIZE_R_1.name()].value().int_val(), std::min(config[NUM_WG_R_1.name()].value().int_val(), atf::ceil_lambda(config[G_CB_SIZE_R_1.name()].value().int_val(), config[L_CB_SIZE_R_1.name()].value().int_val()))))));
                    }
                }
                if (config[P_CB_RES_DEST_LEVEL.name()].value().int_val() == 2) {
                    size *= 1;
                }
                return size;
            };
    auto int_res_size =
            [&](atf::configuration &config) -> size_t {
                size_t size = res_g_l_size * config[NUM_WG_R_1.name()].value().int_val();
                return size;
            };
    std::vector<float> int_res(res_g_l_size / sizeof(float)); for (int i = 0; i < int_res.size(); ++i) int_res[i] = 0;
    auto needs_second_kernel =
            [&](atf::configuration &config) -> bool {
                return config[NUM_WG_R_1.name()].value().int_val() > 1;
            };
    auto gs_1 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (NUM_WG_R_1) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (NUM_WG_R_1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WG_L_5) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WG_L_6) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WG_R_1) * (NUM_WI_R_1), 1)
    );
    auto ls_1 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (NUM_WI_R_1),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (NUM_WI_R_1),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (NUM_WI_R_1), 1)
    );
    auto gs_2 = atf::cf::GS(
              (OCL_DIM_L_1 == 0) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 1) * (NUM_WG_L_1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WG_L_2) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WG_L_3) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WG_L_4) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WG_L_5) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WG_L_6) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WG_L_1) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WG_L_2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WG_L_3) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WG_L_4) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WG_L_5) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WG_L_6) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))), 1)
    );
    auto ls_2 = atf::cf::LS(
              (OCL_DIM_L_1 == 0) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 0) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 0) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 0) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 0) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 0) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 0) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              (OCL_DIM_L_1 == 1) * (NUM_WI_L_1)
            + (OCL_DIM_L_2 == 1) * (NUM_WI_L_2)
            + (OCL_DIM_L_3 == 1) * (NUM_WI_L_3)
            + (OCL_DIM_L_4 == 1) * (NUM_WI_L_4)
            + (OCL_DIM_L_5 == 1) * (NUM_WI_L_5)
            + (OCL_DIM_L_6 == 1) * (NUM_WI_L_6)
            + (OCL_DIM_R_1 == 1) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))),

              atf::max((OCL_DIM_L_1 >= 2) * (NUM_WI_L_1), 1)
            * atf::max((OCL_DIM_L_2 >= 2) * (NUM_WI_L_2), 1)
            * atf::max((OCL_DIM_L_3 >= 2) * (NUM_WI_L_3), 1)
            * atf::max((OCL_DIM_L_4 >= 2) * (NUM_WI_L_4), 1)
            * atf::max((OCL_DIM_L_5 >= 2) * (NUM_WI_L_5), 1)
            * atf::max((OCL_DIM_L_6 >= 2) * (NUM_WI_L_6), 1)
            * atf::max((OCL_DIM_R_1 >= 2) * (atf::min(NUM_WI_R_1, atf::ceil(atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))), atf::min(P_CB_SIZE_R_1, atf::min(L_CB_SIZE_R_1, atf::min(NUM_WG_R_1, atf::ceil(G_CB_SIZE_R_1, L_CB_SIZE_R_1))))))), 1)
    );
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::NONE,
            "none",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl_md_hom(
            device_info,
            {atf::cf::kernel_info::FILENAME, "kernel/tc_abcdef_" + a_dims + "_" + b_dims + "_1.cl", "tc_1", " -DTYPE_T=float -DTYPE_TS=float"},
            atf::inputs(atf::buffer(a), atf::buffer(b)),
            gs_1, ls_1,
            {atf::cf::kernel_info::FILENAME, "kernel/tc_abcdef_" + a_dims + "_" + b_dims + "_2.cl", "tc_2"},
            atf::inputs(atf::buffer(c)),
            gs_2, ls_2,
            res_g_size,
            atf::buffer(int_res),
            int_res_size,
            needs_second_kernel,
            is_valid,
            1, 0, // 0 warm ups and 1 evaluation because proper measurement is done below
            true,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );

    // measure runtime
    std::cout << "measuring runtime... " << std::endl;
    std::cout.setstate(std::ios_base::failbit);
    auto best_config = tuner(kernel);
    std::cout.clear();
    kernel.warm_ups(10);
    kernel.evaluations(200);
    std::vector<std::vector<unsigned long long>> runtimes;
    kernel(best_config, &runtimes);

    // write runtime to file
    unsigned long long runtime = 0;
    for (const auto &times : runtimes) {
        if (!times.empty())
            runtime += *std::min_element(times.begin(), times.end());
    }
    std::string runtime_file_name = config_file_dir;
    runtime_file_name.append("tc_abcdef_" + a_dims + "_" + b_dims + "_runtime");
    std::ofstream runtime_file(runtime_file_name, std::ios::out | std::ios::trunc);
    runtime_file << (runtime / 1000000.0f);
    runtime_file.close();
}

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--mode",        1, false);
    args.addArgument("--platform-id", 1, false);
    args.addArgument("--device-id",   1, false);
    args.addArgument("--input-dims",  2, false);
    args.addArgument("--input-size",  7, false);
    args.addArgument("--tuning-time", 1, true);
    args.parse(static_cast<size_t>(argc), argv);
    std::string         mode        = args.retrieve_string("mode");
    size_t              platform_id = args.retrieve_size_t("platform-id");
    size_t              device_id   = args.retrieve_size_t("device-id");
    std::vector<std::string> input_dims  = args.retrieve_string_vector("input-dims");
    std::vector<size_t> input_size  = args.retrieve_size_t_vector("input-size");
    float               tuning_time;
    try {
        tuning_time = args.retrieve_float("tuning-time");
    } catch(...) {
        tuning_time = 5.0f;
    }
    const size_t M_1 = input_size[0];
    const size_t M_2 = input_size[1];
    const size_t M_3 = input_size[2];
    const size_t M_4 = input_size[3];
    const size_t M_5 = input_size[4];
    const size_t M_6 = input_size[5];
    const size_t N_1 = input_size[6];

    // get device
    atf::cf::device_info device_info(static_cast<const int &>(platform_id), static_cast<const int &>(device_id));
    device_info.initialize();
    std::string device_type;
    cl_device_type type;
    atf::cf::check_error(device_info.device().getInfo(CL_DEVICE_TYPE, &type));
    switch (type) {
        case CL_DEVICE_TYPE_CPU:
            device_type = "cpu";
            break;
        case CL_DEVICE_TYPE_GPU:
            device_type = "gpu";
            break;
        case CL_DEVICE_TYPE_ACCELERATOR:
            device_type = "accelerator";
            break;
        case CL_DEVICE_TYPE_CUSTOM:
            device_type = "custom";
            break;
        default:
            std::cerr << "unknown device type." << std::endl;
            exit(EXIT_FAILURE);
    }

    // prepare inputs
    size_t a_size = 1;
    if (input_dims[0].find('a') != std::string::npos) a_size *= M_1;
    if (input_dims[0].find('b') != std::string::npos) a_size *= M_2;
    if (input_dims[0].find('c') != std::string::npos) a_size *= M_3;
    if (input_dims[0].find('d') != std::string::npos) a_size *= M_4;
    if (input_dims[0].find('e') != std::string::npos) a_size *= M_5;
    if (input_dims[0].find('f') != std::string::npos) a_size *= M_6;
    if (input_dims[0].find('g') != std::string::npos) a_size *= N_1;
    std::vector<float> a(a_size); for (int i = 0; i < a.size(); ++i) a[i] = (i % 100) + 1;
    size_t b_size = 1;
    if (input_dims[1].find('a') != std::string::npos) b_size *= M_1;
    if (input_dims[1].find('b') != std::string::npos) b_size *= M_2;
    if (input_dims[1].find('c') != std::string::npos) b_size *= M_3;
    if (input_dims[1].find('d') != std::string::npos) b_size *= M_4;
    if (input_dims[1].find('e') != std::string::npos) b_size *= M_5;
    if (input_dims[1].find('f') != std::string::npos) b_size *= M_6;
    if (input_dims[1].find('g') != std::string::npos) b_size *= N_1;
    std::vector<float> b(b_size); for (int i = 0; i < b.size(); ++i) b[i] = (i % 100) + 1;
    std::vector<float> c(M_1 * M_2 * M_2 * M_4 * M_5 * M_6); for (int i = 0; i < c.size(); ++i) c[i] = 0;

    if (mode == "tune") {
        tune(platform_id, device_id, device_info, device_type, tuning_time, input_dims[0], input_dims[1], M_1, M_2, M_3, M_4, M_5, M_6, N_1, a, b, c);
    } else if (mode == "bench") {
        bench(platform_id, device_id, device_info, device_type, input_dims[0], input_dims[1], M_1, M_2, M_3, M_4, M_5, M_6, N_1, a, b, c);
    } else {
        std::cerr << "unknown mode! possible values: tune, bench" << std::endl;
        exit(EXIT_FAILURE);
    }
}