// clang-format off
/*
 * gaussian-sp.sdsl.c: This file is part of the SDSLC project.
 *
 * SDSLC: A compiler for high performance stencil computations
 *
 * Copyright (C) 2011-2013 Ohio State University
 *
 * This program can be redistributed and/or modified under the terms
 * of the license specified in the LICENSE.txt file at the root of the
 * project.
 *
 * Contact: P Sadayappan <saday@cse.ohio-state.edu>
 */

/*
 * @file: gaussian-sp.sdsl.c
 * @author: Tom Henretty <henretty@cse.ohio-state.edu>
 */
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

// Problem parameters
#ifndef H
#define H (INPUT_SIZE_0)
#endif

#ifndef W
#define W (INPUT_SIZE_1)
#endif

#ifndef T
//#define T (4)
#define T (1)
#endif

float aref[2][H][W];
float a[2][H][W];

/** Main program */
int main(int argc, char *argv[]) {
	int i, j, ii, jj, t, tt;
	double refElapsed, sdslElapsed;
	double refGFLOPS, sdslGFLOPS;
	float(*inref)[W], (*outref)[W];

	// Initialize arrays
	for (i = 0; i < H; i++) {
		for (j = 0; j < W; j++) {
			aref[0][i][j] = aref[1][i][j] = sin(i) * cos(j);
			a[0][i][j] = a[1][i][j] = sin(i) * cos(j);
		}
	}

// Compute reference
#ifndef NOREF
	for (t = 0; t < T; t++) {
		inref = aref[t & 1];
		outref = aref[(t + 1) & 1];
		for (i = 2; i < H - 2; i++) {
#pragma ivdep
#pragma vector always
			for (j = 2; j < W - 2; j++) {
				outref[i][j] =
				    (2 * inref[i - 2][j - 2] + 4 * inref[i - 2][j - 1] +
				     5 * inref[i - 2][j] + 4 * inref[i - 2][j + 1] +
				     2 * inref[i - 2][j + 2] + 4 * inref[i - 1][j - 2] +
				     9 * inref[i - 1][j - 1] + 12 * inref[i - 1][j] +
				     9 * inref[i - 1][j + 1] + 4 * inref[i - 1][j + 2] +
				     5 * inref[i][j - 2] + 12 * inref[i][j - 1] + 15 * inref[i][j] +
				     12 * inref[i][j + 1] + 5 * inref[i][j + 2] +
				     4 * inref[i + 1][j - 2] + 9 * inref[i + 1][j - 1] +
				     12 * inref[i + 1][j] + 9 * inref[i + 1][j + 1] +
				     4 * inref[i + 1][j + 2] + 2 * inref[i + 2][j - 2] +
				     4 * inref[i + 2][j - 1] + 5 * inref[i + 2][j] +
				     4 * inref[i + 2][j + 1] + 2 * inref[i + 2][j + 2]) /
				    159;
			}
		}
	}
#endif

// Compute ppcg
#ifndef NOREF
	for (t = 0; t < T; t++) {
		inref = a[t & 1];
		outref = a[(t + 1) & 1];
#pragma scop
		for (i = 2; i < H - 2; i++) {
			for (j = 2; j < W - 2; j++) {
				outref[i][j] =
				    (2 * inref[i - 2][j - 2] + 4 * inref[i - 2][j - 1] +
				     5 * inref[i - 2][j] + 4 * inref[i - 2][j + 1] +
				     2 * inref[i - 2][j + 2] + 4 * inref[i - 1][j - 2] +
				     9 * inref[i - 1][j - 1] + 12 * inref[i - 1][j] +
				     9 * inref[i - 1][j + 1] + 4 * inref[i - 1][j + 2] +
				     5 * inref[i][j - 2] + 12 * inref[i][j - 1] + 15 * inref[i][j] +
				     12 * inref[i][j + 1] + 5 * inref[i][j + 2] +
				     4 * inref[i + 1][j - 2] + 9 * inref[i + 1][j - 1] +
				     12 * inref[i + 1][j] + 9 * inref[i + 1][j + 1] +
				     4 * inref[i + 1][j + 2] + 2 * inref[i + 2][j - 2] +
				     4 * inref[i + 2][j - 1] + 5 * inref[i + 2][j] +
				     4 * inref[i + 2][j + 1] + 2 * inref[i + 2][j + 2]) /
				    159;
			}
		}
#pragma endscop
	}
#endif

	// Check correctness
	inref = aref[T & 1];
	outref = a[T & 1];
	float maxDiff = 0.0, sumDiff = 0.0, diff, meanDiff;
	int numDiff = 0;
	for (i = 0; i < H; i++) {
		for (j = 0; j < W; j++) {
			diff = fabs(inref[i][j] - outref[i][j]);
			if (diff != 0.0f) {
#ifdef DEBUG
				printf("Diff at a[%d][%d] = %.6f\n", i, j, diff);
#endif
				numDiff++;
			}
			sumDiff += diff;
			if (diff > maxDiff) {
				maxDiff = diff;
			}
		}
	}
	meanDiff = sumDiff / ((float)(H * W));

	printf("Num diff  = %d\n", numDiff);
	printf("Sum diff  = %.8f\n", sumDiff);
	printf("Mean diff = %.8f\n", meanDiff);
	printf("Max diff  = %.8f\n", maxDiff);

	return 0;
}
