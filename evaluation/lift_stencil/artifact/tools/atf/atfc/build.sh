#!/bin/bash

pushd `dirname $0` > /dev/null
rm -rf ./build && \
mkdir build && \
cd build && \
cmake -DCMAKE_BUILD_TYPE=Release -DATFC_NO_BOOST_PROCESS=On ./.. && \
make -j && \
popd > /dev/null

