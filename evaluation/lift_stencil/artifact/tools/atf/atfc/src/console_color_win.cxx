// This file is part of the Auto Tuning Framework (ATF).


// (c) Dominique Bönninghoff 2016
// This file was neither created nor modified during the PJS, and thus retains
// its original license.

// MIT License, Copyright 2016 Dominique Bönninghoff

#include "platform.hxx"

#ifdef ATFC_IS_WINDOWS

#include "console_color.hxx"
#include <mutex>
#include <stdexcept>
#include <type_traits>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h> // Console functions
#include <io.h>      // Helper functions (isatty_fileno)
#undef WIN32_LEAN_AND_MEAN

namespace atfc {
namespace internal {
bool is_terminal(std::ostream &p_stream) {
  // TODO Dirty hack
  FILE *t_str;

  if (&p_stream == &std::cout)
    t_str = stdout;
  else if (&p_stream == &std::cerr)
    t_str = stderr;
  else
    return false; // We are not certain, so no colour stream.

  return _isatty(_fileno(t_str)) != 0;
}

HANDLE handle(std::ostream &p_stream) {
  // TODO Dirty hack
  if (&p_stream == &std::cout)
    return GetStdHandle(STD_OUTPUT_HANDLE);
  else if (&p_stream == &std::cerr)
    return GetStdHandle(STD_ERROR_HANDLE);
  else
    throw ::std::runtime_error("Color output: unsupported stream!");
}

WORD attr(HANDLE p_h) {
  CONSOLE_SCREEN_BUFFER_INFO info;

  if (GetConsoleScreenBufferInfo(p_h, &info)) {
    return info.wAttributes;
  } else
    throw ::std::runtime_error("Color output: failed to retrieve screen info!");
}

void set_attr(HANDLE p_h, WORD p_w) {
  if (!SetConsoleTextAttribute(p_h, p_w))
    throw ::std::runtime_error("Color output: failed to set text attribute!");
}

void update_attr(WORD &p_w, console_color p_clr, bool p_isBack) {
  static const WORD map[16][2]{
      /* RED */ {FOREGROUND_RED, BACKGROUND_RED},
      /* BRED */
      {FOREGROUND_RED | FOREGROUND_INTENSITY,
       BACKGROUND_RED | BACKGROUND_INTENSITY},

      /* GREEN */ {FOREGROUND_GREEN, BACKGROUND_GREEN},
      /* BGREEN */
      {FOREGROUND_GREEN | FOREGROUND_INTENSITY,
       BACKGROUND_GREEN | BACKGROUND_INTENSITY},

      /* YELLOW */
      {FOREGROUND_GREEN | FOREGROUND_RED, BACKGROUND_GREEN | BACKGROUND_RED},
      /* BYELLOW */
      {FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY,
       BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY},

      /* BLUE */ {FOREGROUND_BLUE, BACKGROUND_BLUE},
      /* BBLUE */
      {FOREGROUND_BLUE | FOREGROUND_INTENSITY,
       BACKGROUND_BLUE | BACKGROUND_INTENSITY},

      /* MAGENTA */
      {FOREGROUND_BLUE | FOREGROUND_RED, BACKGROUND_BLUE | BACKGROUND_RED},
      /* BMAGENTA */
      {FOREGROUND_BLUE | FOREGROUND_RED | FOREGROUND_INTENSITY,
       BACKGROUND_BLUE | BACKGROUND_RED | BACKGROUND_INTENSITY},

      /* CYAN */
      {FOREGROUND_GREEN | FOREGROUND_BLUE, BACKGROUND_GREEN | BACKGROUND_BLUE},
      /* BCYAN */
      {FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY,
       BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_INTENSITY},

      /* WHITE */
      {FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED,
       BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_RED},
      /* BWHITE */
      {FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED |
           FOREGROUND_INTENSITY,
       BACKGROUND_GREEN | BACKGROUND_BLUE | BACKGROUND_RED |
           BACKGROUND_INTENSITY},

      /* BLACK */ {0, 0},
      /* BBLACK */ {FOREGROUND_INTENSITY, BACKGROUND_INTENSITY}};

  // First nibble (counting from LSB) in WORD is foreground info,
  // second one is background info

  // Clear corresponding nibble
  p_w &= ~(0xF << (p_isBack ? 4 : 0));

  // Set new color information
  p_w |= map[static_cast<int>(p_clr)][p_isBack ? 1 : 0];

  return;
}

void set_color(std::ostream &p_stream, console_color p_clr,
               bool p_isBack = false) {
  // set_color is inherently single-threaded since it manages a single, global
  // state. This mutex is used to synchronize access.
  static ::std::mutex t_mtx{};

  // Critical section begin
  {
    // Engage lock
    ::std::lock_guard<::std::mutex> t_lck{t_mtx};

    // Flush stream to garantuee previous color output
    p_stream.flush();

    // Saved console attributes
    // TODO We use the same default for cerr and cout?!
    static WORD defAttrib{};
    static bool hasDefAttrib{};

    // Get the handle we will be using
    HANDLE hConsole = handle(p_stream);

    // If we havent saved default attributes, do so now
    if (!hasDefAttrib) {
      defAttrib = attr(hConsole);
      hasDefAttrib = true;
    }

    // Perform reset to defaults if requested
    if (p_clr == console_color::reset) {
      // We know for sure that defAttrib contains a valid attribute
      set_attr(hConsole, defAttrib);

      // Aquire new default the next time a color is set
      hasDefAttrib = false;
    } else {
      // Get current attributes, modify them according to request, then save
      // them back
      auto current = attr(hConsole);
      update_attr(current, p_clr, p_isBack);
      set_attr(hConsole, current);
    }
  }
  // Critical section end
}
} // namespace internal

color_manipulator foreground(console_color p_clr) {
  return color_manipulator(p_clr, false);
}

color_manipulator background(console_color p_clr) {
  return color_manipulator(p_clr, true);
}

style_manipulator style(console_style p_style) {
  return style_manipulator(p_style);
}

std::ostream &operator<<(std::ostream &p_stream, const style_manipulator &) {
  // Styles are not supported on windows
  return p_stream;
}

std::ostream &operator<<(std::ostream &p_stream,
                         const color_manipulator &p_manip) {
  if (internal::is_terminal(p_stream)) {
    internal::set_color(p_stream, p_manip.color(), p_manip.is_background());
  }
  return p_stream;
}

std::ostream &reset_color(std::ostream &p_stream) {
  if (internal::is_terminal(p_stream)) {
    internal::set_color(p_stream, console_color::reset);
  }
  return p_stream;
}
} // namespace atfc

#endif
