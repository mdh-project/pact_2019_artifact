// This file is part of the Auto Tuning Framework (ATF).


#include <diagnostics.hxx>
#include <parser/grammar.hxx>
#include <parser/parser.hxx>
#include <parser/parser_action.hxx>
#include <parser/parser_control.hxx>
#include <parser/parser_state.hxx>
#include <stdexcept>

namespace atfc {
auto parse(string_view p_src, string_view p_name, bool p_inline) -> job {
  // Create parser state object
  parser_state t_state;

  // Parse
  bool t_result{};

  try {
    if (p_inline)
      t_result = pegtl::parse<start<true>, parser_action, parser_control>(
          p_src.to_string(), p_name.to_string(), p_src, t_state);
    else
      t_result = pegtl::parse<start<false>, parser_action, parser_control>(
          p_src.to_string(), p_name.to_string(), p_src, t_state);
  } catch (...) {
    t_result = false;
  }

  if (t_result)
    return t_state.m_Job;
  else {
    post_summary();
    throw ::std::runtime_error("Failed to parse: pegtl::parse returned false");
  }
}
} // namespace atfc
