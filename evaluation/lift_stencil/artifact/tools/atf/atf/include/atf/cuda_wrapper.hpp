// This file is part of the Auto Tuning Framework (ATF).


// CUDA Wrapper for ATF
// Dominique Bönninghoff
#pragma once

#ifndef LIBATF_DISABLE_CUDA

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <stdio.h>
#include <tuple>
#include <vector>

#include <cuda.h>
#include <cuda_runtime.h>
#include <nvrtc.h>

#include "helper.hpp"
#include "tp_value.hpp"

using namespace std::literals::string_literals;

namespace atf {
namespace cf {
using cuda_device_id = ::std::size_t;

namespace detail {
template <typename T = ::std::runtime_error>
auto nvrtc_safe_call(nvrtcResult p_result, const ::std::string &p_message)
    -> void {
  if (p_result != NVRTC_SUCCESS) {
    throw T{p_message};
    ::std::exit(EXIT_FAILURE);
  }
}

template <typename T = ::std::runtime_error>
auto cuda_safe_call(cudaError_t p_result, const ::std::string &p_message)
    -> void {
  if (p_result != cudaSuccess) {
    throw T{p_message};
    ::std::exit(EXIT_FAILURE);
  }
}

template <typename T = ::std::runtime_error>
auto cuda_safe_call(CUresult p_result, const ::std::string &p_message) -> void {
  if (p_result != CUDA_SUCCESS) {
    throw T{p_message};
    ::std::exit(EXIT_FAILURE);
  }
}

template <typename GD0, typename GD1, typename GD2, typename BD0, typename BD1,
          typename BD2, typename... Ts>
class cuda_wrapper {
  using this_type = cuda_wrapper<GD0, GD1, GD2, BD0, BD1, BD2, Ts...>;
  using input_type = ::std::tuple<Ts...>;
  using block_dim_type = ::std::tuple<BD0, BD1, BD2>;
  using grid_dim_type = ::std::tuple<GD0, GD1, GD2>;
  using return_type = ::std::size_t;

public:
  cuda_wrapper() = delete;
  cuda_wrapper(const this_type &) = delete;
  cuda_wrapper(this_type &&) = default;

  const this_type &operator=(const this_type &) = delete;
  this_type &operator=(this_type &&) = default;

public:
  cuda_wrapper(cuda_device_id p_devId, const kernel_info &p_krnlInfo,
               const input_type &p_krnlInputs, const grid_dim_type &p_gridDim,
               const block_dim_type &p_blockDim)
      : m_DeviceID{p_devId}, m_KernelSource{p_krnlInfo.source()},
        m_KernelName{p_krnlInfo.name()}, m_KernelFlags{p_krnlInfo.flags()},
        m_KernelInputs{p_krnlInputs}, m_GridDimensions{p_gridDim},
        m_BlockDimensions{p_blockDim} {
    this->create_program();
    this->init_cuda();
    this->create_buffers();
  }

public:
  auto operator()(configuration &p_cfg) -> return_type {
    // Update tuning parameters
    this->update_tps(p_cfg);

    // Compile kernel
    this->compile_kernel(p_cfg);

    // Run kernel
    return this->run_kernel();
  }

private:
  auto run_kernel() -> return_type {
    // Retrieve PTX
    size_t t_ptxSize;
    nvrtc_safe_call<>(nvrtcGetPTXSize(m_Program, &t_ptxSize),
                      "Failed to retrieve PTX size");

    ::std::vector<char> t_ptxCode(t_ptxSize);
    nvrtc_safe_call<>(nvrtcGetPTX(m_Program, t_ptxCode.data()),
                      "Failed to retrieve PTX code");

    // Load PTX
    CUmodule t_module;
    CUfunction t_kernel;

    cuda_safe_call<>(cuModuleLoadDataEx(&t_module, t_ptxCode.data(), 0, 0, 0),
                     "Failed to load module data");
    cuda_safe_call<>(
        cuModuleGetFunction(&t_kernel, t_module, m_KernelName.c_str()),
        "Failed to retrieve kernel handle");

    // Create benchmark events
    cudaEvent_t t_start, t_stop;
    cuda_safe_call<>(cudaEventCreate(&t_start), "Failed to create events");
    cuda_safe_call<>(cudaEventCreate(&t_stop), "Failed to create events");

    // Retrieve grid and block dimensions
    const size_t gd0 = std::get<0>(m_GridDimensions).get_value();
    const size_t gd1 = std::get<1>(m_GridDimensions).get_value();
    const size_t gd2 = std::get<2>(m_GridDimensions).get_value();

    const size_t bd0 = std::get<0>(m_BlockDimensions).get_value();
    const size_t bd1 = std::get<1>(m_BlockDimensions).get_value();
    const size_t bd2 = std::get<2>(m_BlockDimensions).get_value();

    // Launch kernel
    cuda_safe_call<>(cudaEventRecord(t_start, 0),
                     "Failed to record start event");

    const auto t_kernelResult =
        cuLaunchKernel(t_kernel, gd0, gd1, gd2, bd0, bd1, bd2, 0, nullptr,
                       m_KernelInputPtrs.data(), nullptr);

    cuda_safe_call<>(cudaEventRecord(t_stop, 0), "Failed to record stop event");

    // Check for success
    if (t_kernelResult != CUDA_SUCCESS)
      throw ::std::exception();

    // Profiling
    float t_runtimeInMs = 0.f;

    cuda_safe_call<>(cudaEventSynchronize(t_stop),
                     "Failed to synchronize events");
    cuda_safe_call<>(cudaEventElapsedTime(&t_runtimeInMs, t_start, t_stop),
                     "Failed to retrieve elapsed time");

    cuda_safe_call<>(cudaEventDestroy(t_start), "Failed to destroy events");
    cuda_safe_call<>(cudaEventDestroy(t_stop), "Failed to destroy events");

    // TODO not in µs?
    return static_cast<size_t>(t_runtimeInMs);
  }

  auto compile_kernel(configuration &p_cfg) -> void {
    // Retrieve flags
    ::std::vector<::std::string> t_flags;
    ::std::vector<const char *> t_views;
    this->create_flags(p_cfg, t_flags, t_views);

    // Compile kernel
    const auto t_result = nvrtcCompileProgram(
        m_Program, static_cast<int>(t_flags.size()), t_views.data());

    // If compilation failed, retrieve log
    if (t_result != NVRTC_SUCCESS) {
      // Query log size
      size_t t_logSize;
      nvrtc_safe_call<>(nvrtcGetProgramLogSize(m_Program, &t_logSize),
                        "Failed to retrieve log size");

      // Retrieve log
      ::std::vector<char> t_log(t_logSize);
      nvrtc_safe_call<>(nvrtcGetProgramLog(m_Program, t_log.data()),
                        "Failed to retrieve log");

      ::std::cout << t_log.data() << ::std::endl;

      throw ::std::exception();
    }
  }

  auto create_flags(configuration &p_cfg, ::std::vector<::std::string> &p_dest,
                    ::std::vector<const char *> &p_views) -> void {

    for (const auto &t_tp : p_cfg)
      p_dest.emplace_back(" -D "s + t_tp.second.name() + "=" +
                          static_cast<::std::string>(t_tp.second.value()));

    for (::std::size_t i = 0; i < m_KernelBufferSizes.size(); ++i)
      p_dest.emplace_back(" -D N_"s + ::std::to_string(i) + "=" +
                          ::std::to_string(m_KernelBufferSizes[i]));

    // Append additional kernel flags
    p_dest.push_back(m_KernelFlags);

    // Create views
    ::std::transform(p_dest.begin(), p_dest.end(),
                     ::std::back_inserter(p_views),
                     [](auto &t_str) -> const char * { return t_str.c_str(); });
  }

  auto create_buffers() -> void {
    this->create_buffers_and_set_args(
        ::std::make_index_sequence<sizeof...(Ts)>());
  }

  auto create_program() -> void {
    nvrtc_safe_call<>(nvrtcCreateProgram(&m_Program, m_KernelSource.c_str(),
                                         nullptr, 0, nullptr, nullptr),
                      "Failed to create NVRTC program");
  }

  auto init_cuda() -> void {
    cuda_safe_call<>(cuInit(0), "Failed to initialize CUDA");
    cuda_safe_call<>(cuDeviceGet(&m_Device, m_DeviceID),
                     "Failed to retrieve specified device");
    cuda_safe_call<>(cuCtxCreate(&m_Context, 0, m_Device),
                     "Failed to create context");
  }

  auto update_tps(configuration &p_cfg) -> void {
    // Update all tuning parameters
    for (auto &t_tp : p_cfg) {
      // Retrieve new value and value pointer
      auto t_value = t_tp.second.value();
      auto t_value_ptr = t_tp.second.tp_value_ptr();

      switch (t_value.type_id()) {
      case value_type::int_t: {
        *static_cast<int *>(t_value_ptr) = static_cast<int>(t_value);
        break;
      }

      case value_type::size_t_t: {
        *static_cast<::std::size_t *>(t_value_ptr) =
            static_cast<::std::size_t>(t_value);
        break;
      }

      case value_type::float_t: {
        *static_cast<float *>(t_value_ptr) = static_cast<float>(t_value);
        break;
      }

      case value_type::double_t: {
        *static_cast<double *>(t_value_ptr) = static_cast<double>(t_value);
        break;
      }

      default:
        throw ::std::runtime_error("cuda_wrapper::update_tps: Unknown type_id");
      }
    }
  }

private:
  // --- Copied from old CUDA wrapper
  template <size_t... Is>
  void create_buffers_and_set_args(std::index_sequence<Is...>) {
    create_buffers_and_set_args_impl(std::get<Is>(m_KernelInputs)...);
  }

  template <typename T, typename... ARGs>
  void create_buffers_and_set_args_impl(scalar<T> &scalar, ARGs &... args) {
    // set kernel arg
    m_KernelInputPtrs.emplace_back(scalar.get_ptr());

    create_buffers_and_set_args_impl(args...);
  }

  template <typename T, typename... ARGs>
  void create_buffers_and_set_args_impl(buffer_class<T> &buffer,
                                        ARGs &... args) {
    m_KernelBufferSizes.emplace_back(buffer.size());

    m_KernelBuffers.emplace_back();
    auto &ptr = m_KernelBuffers.back();

    cuda_safe_call<>(cuMemAlloc(&ptr, buffer.size() * sizeof(T)),
                     "Failed to allocate buffer");
    cuda_safe_call<>(cuMemcpyHtoD(ptr, buffer.get(), buffer.size() * sizeof(T)),
                     "Failed to copy buffer data to device");

    // set kernel arg
    m_KernelInputPtrs.emplace_back(&ptr);

    create_buffers_and_set_args_impl(args...);
  }

  void create_buffers_and_set_args_impl() {}

private:
  // --- CUDA Device Info
  cuda_device_id m_DeviceID;

  // --- Kernel data
  ::std::string m_KernelSource;
  ::std::string m_KernelName;
  ::std::string m_KernelFlags;
  grid_dim_type m_GridDimensions;
  block_dim_type m_BlockDimensions;

  // --- Kernel buffers
  ::std::vector<CUdeviceptr> m_KernelBuffers;
  ::std::vector<::std::size_t> m_KernelBufferSizes;

  // --- Kernel inputs
  input_type m_KernelInputs;
  ::std::vector<void *> m_KernelInputPtrs;

  // --- NVRCT values
  CUdevice m_Device;
  CUcontext m_Context;
  nvrtcProgram m_Program;
};
} // namespace detail

template <typename GD0, typename GD1, typename GD2, //< Grid dimensions
          typename BD0, typename BD1, typename BD2, //< Block dimensions
          typename... Ts>
auto cuda(cuda_device_id device, const kernel_info &kernel,
          const ::std::tuple<Ts...> &kernel_inputs,

          ::std::tuple<GD0, GD1, GD2> grid_dim,
          ::std::tuple<BD0, BD1, BD2> block_dim) {
  return detail::cuda_wrapper<GD0, GD1, GD2, BD0, BD1, BD2, Ts...>{
      device, kernel, kernel_inputs, grid_dim, block_dim};
}

template <typename T0, typename T1 = ::std::size_t, typename T2 = ::std::size_t>
auto grid_dim(T0 &&gd0, T1 &&gd1 = 1U, T2 &&gd2 = 1U) {
  return ::std::make_tuple(wrapper<T0 &&>(::std::forward<T0>(gd0)),
                           wrapper<T1 &&>(::std::forward<T1>(gd1)),
                           wrapper<T2 &&>(::std::forward<T2>(gd2)));
}

template <typename T0, typename T1 = ::std::size_t, typename T2 = ::std::size_t>
auto block_dim(T0 &&bd0, T1 &&bd1 = 1U, T2 &&bd2 = 1U) {
  return ::std::make_tuple(wrapper<T0 &&>(::std::forward<T0>(bd0)),
                           wrapper<T1 &&>(::std::forward<T1>(bd1)),
                           wrapper<T2 &&>(::std::forward<T2>(bd2)));
}
} // namespace cf
} // namespace atf

#endif
