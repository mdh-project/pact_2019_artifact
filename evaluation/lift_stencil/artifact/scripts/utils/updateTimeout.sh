#!/bin/bash

if [ "$#" -lt 2 ]; then
    echo "Value required as parameter!"
    exit -1
fi

VALUE=$2

DIR="$1" &&
FILES=$( find $DIR -type f | grep json$ ) &&
PARAM_TO_CHANGE="timeout_in_seconds" &&
for FILE in ${FILES} ; do
    sed -i "s/.*${PARAM_TO_CHANGE}.*/    \"${PARAM_TO_CHANGE}\" : ${VALUE},/" $FILE
done
