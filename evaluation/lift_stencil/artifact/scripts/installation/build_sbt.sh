#!/bin/bash
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

pushd $ROOTDIR/tools > /dev/null &&
wget https://piccolo.link/sbt-0.13.16.tgz &&
tar -xvf sbt-0.13.16.tgz &&
rm sbt-0.13.16.tgz &&
popd > /dev/null
