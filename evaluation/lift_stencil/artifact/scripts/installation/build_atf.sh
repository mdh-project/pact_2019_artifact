#!/bin/bash
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

pushd $ATF > /dev/null &&
CC=$(which gcc) CXX=$(which g++) ./build.sh &&
popd > /dev/null
