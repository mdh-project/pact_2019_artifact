#include <iostream>
#include <cstdlib>
#include <argparse.hpp>
#include <atf.h>
#include <json.hpp>

void bench(size_t platform_id, size_t device_id, atf::cf::device_info &device_info, const std::string &device_type,
           const std::string &application, const std::vector<size_t> &input_size,
           const std::vector<float> &in, const std::vector<float> &out) {
    std::string prefix = std::getenv("ARTIFACT_ROOT");
    prefix.append("/results/")
            .append(device_type).append("/")
            .append(std::to_string(platform_id)).append("/")
            .append(std::to_string(device_id)).append("/lift/")
            .append(application).append("_");
    if (application == "gaussian") {
        prefix.append(std::to_string(input_size[0])).append("x").append(std::to_string(input_size[1]));
    } else {
        prefix.append(std::to_string(input_size[0])).append("x").append(std::to_string(input_size[1])).append("x").append(std::to_string(input_size[2]));
    }

    // read configuration
    int gs_0 = 1, gs_1 = 1, gs_2 = 1;
    int ls_0 = 1, ls_1 = 1, ls_2 = 1;
    std::ifstream config_file(prefix + "_config.json", std::ios::in);
    if (config_file.fail()) {
        std::cerr << "Unable to open configuration file. Has lift not yet been tuned for this input size?" << std::endl;
        exit(EXIT_FAILURE);
    }
    auto plain_config = nlohmann::json::parse(config_file);
    gs_0 = plain_config.at("GLOBAL_SIZE_0").get<int>();
    gs_1 = plain_config.at("GLOBAL_SIZE_1").get<int>();
    if (plain_config.find("GLOBAL_SIZE_2") != plain_config.end()) gs_2 = plain_config.at("GLOBAL_SIZE_2").get<int>();
    ls_0 = plain_config.at("LOCAL_SIZE_0").get<int>();
    ls_1 = plain_config.at("LOCAL_SIZE_1").get<int>();
    if (plain_config.find("LOCAL_SIZE_2") != plain_config.end()) ls_2 = plain_config.at("LOCAL_SIZE_2").get<int>();

    // set configuration for kernel execution
    auto GS_0 = atf::tp("TP_GS_0", {gs_0});
    auto GS_1 = atf::tp("TP_GS_1", {gs_1});
    auto GS_2 = atf::tp("TP_GS_2", {gs_2});
    auto LS_0 = atf::tp("TP_LS_0", {ls_0});
    auto LS_1 = atf::tp("TP_LS_1", {ls_1});
    auto LS_2 = atf::tp("TP_LS_2", {ls_2});
    auto tuner = atf::exhaustive();
    tuner(GS_0, GS_1, GS_2)(LS_0, LS_1, LS_2);

    // prepare kernel
    auto is_valid = [&](atf::configuration &config) -> bool {
        return true;
    };
    atf::cf::process_wrapper_info process_wrapper = {
            atf::cf::NONE,
            "none",
            "none",
            atf::cf::CHECK_NONE,
            "wrap"
    };
    atf::cf::timeout warm_up_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    atf::cf::timeout evaluation_timeout = {
            atf::cf::ABSOLUTE,
            {.absolute = 0}
    };
    auto kernel = atf::cf::ocl<float>(
            device_info,
            {atf::cf::kernel_info::FILENAME, prefix + "_kernel.cl", "KERNEL"},
            atf::inputs(atf::buffer(in), atf::buffer(out)),
            atf::cf::GS(GS_0, GS_1, GS_2),
            atf::cf::LS(LS_0, LS_1, LS_2),
            is_valid,
            1, 0, // 0 warm ups and 1 evaluation because proper measurement is done below
            true,
            process_wrapper,
            warm_up_timeout,
            evaluation_timeout
    );

    // measure runtime
    std::cout << "measuring runtime... " << std::endl;
    std::cout.setstate(std::ios_base::failbit);
    auto best_config = tuner(kernel);
    std::cout.clear();
    kernel.warm_ups(10);
    kernel.evaluations(200);
    std::vector<std::vector<unsigned long long>> runtimes;
    kernel(best_config, &runtimes);

    // write runtime to file
    unsigned long long runtime = 0;
    for (const auto &times : runtimes) {
        if (!times.empty())
            runtime += *std::min_element(times.begin(), times.end());
    }
    std::ofstream runtime_file(prefix + "_runtime", std::ios::out | std::ios::trunc);
    runtime_file << (runtime / 1000000.0f);
    runtime_file.close();
}

int main(int argc, const char **argv) {
    // define and parse arguments
    ArgumentParser args;
    args.addArgument("--platform-id", 1, false);
    args.addArgument("--device-id",   1, false);
    args.addArgument("--application", 1, false);
    args.addArgument("--input-size",  3, false);
    args.parse(static_cast<size_t>(argc), argv);
    size_t      platform_id = args.retrieve_size_t("platform-id");
    size_t      device_id   = args.retrieve_size_t("device-id");
    std::string application = args.retrieve_string("application");
    if (application != "gaussian" && application != "j3d7pt") {
        std::cerr << "unknown application" << std::endl;
        exit(EXIT_FAILURE);
    }
    std::vector<size_t> input_size = args.retrieve_size_t_vector("input-size");

    // get device
    atf::cf::device_info device_info(static_cast<const int &>(platform_id), static_cast<const int &>(device_id));
    device_info.initialize();
    std::string device_type;
    cl_device_type type;
    atf::cf::check_error(device_info.device().getInfo(CL_DEVICE_TYPE, &type));
    switch (type) {
        case CL_DEVICE_TYPE_CPU:
            device_type = "cpu";
            break;
        case CL_DEVICE_TYPE_GPU:
            device_type = "gpu";
            break;
        default:
            std::cerr << "Lift artifact only supports device types cpu and gpu." << std::endl;
            exit(EXIT_FAILURE);
    }

    // prepare inputs
    size_t H = input_size[0];
    size_t W = input_size[1];
    size_t D = input_size[2];
    std::vector<float> in(H * W * D); for (int i = 0; i < in.size(); ++i) in[i] = (i % 100) + 1;
    std::vector<float> out(H * W * D); for (int i = 0; i < out.size(); ++i) out[i] = 0;

    // benchmark kernel
    bench(platform_id, device_id, device_info, device_type, application, input_size, in, out);
}