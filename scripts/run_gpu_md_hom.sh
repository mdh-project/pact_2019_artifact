#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi
: ${OCL_GPU_PLATFORM_ID?"Please set the environment variable OCL_GPU_PLATFORM_ID."}
if [ -z "$OCL_GPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_GPU_DEVICE_ID?"Please set the environment variable OCL_GPU_DEVICE_ID."}
if [ -z "$OCL_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
{
  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMM.frameworks[] | test("^MDH$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_new
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_new &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."input sizes"[]'`; do
      ./md_hom_new_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMV.frameworks[] | test("^MDH$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_new
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_new &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMV."input sizes"[]'`; do
      ./md_hom_new_gemv --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Gaussian (2D)".frameworks[] | test("^MDH$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_new
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_new &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."input sizes"[]'`; do
      ./md_hom_new_gaussian_static --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[]-4 | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Jacobi (3D)".frameworks[] | test("^MDH$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_new
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_new &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Jacobi (3D)"."input sizes"[]'`; do
      ./md_hom_new_j3d7pt_static --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[]-2 | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Multi-Channel Convolution".frameworks[] | test("^MDH$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_new
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_new &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Multi-Channel Convolution"."input sizes"[]'`; do
      ./md_hom_new_multi_channel_convolution --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size `echo $is | jq -r '[(.[0:2]+[(.[2:4] | .[]-2)]+.[4:5])[] | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Probabilistic Record Linkage".frameworks[] | test("^MDH$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_new
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_new &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Probabilistic Record Linkage"."input sizes"[]'`; do
      ./md_hom_new_rl --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size $is $is
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Tensor Contractions".frameworks[] | test("^MDH$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_new
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_new &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_new/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Tensor Contractions"."input sizes"[]'`; do
      ./md_hom_new_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims `echo $is | jq -r '[.[0][1:3][] | tostring] | join(" ")'` --input-size `echo $is | jq -r '[.[1][] | tostring] | join(" ")'`
    done
  fi

  printf "\n\nmd_hom execution successful!\n"
} || {
  printf "\n\nmd_hom execution failed!\n"
  exit 1
}