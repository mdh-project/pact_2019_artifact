#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi
: ${OCL_CPU_PLATFORM_ID?"Please set the environment variable OCL_CPU_PLATFORM_ID."}
if [ -z "$OCL_CPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_CPU_DEVICE_ID?"Please set the environment variable OCL_CPU_DEVICE_ID."}
if [ -z "$OCL_CPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
{
  if [[ -n "$ENABLE_LIFT" && "$ENABLE_LIFT" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Gaussian (2D)".frameworks[] | test("^Lift$")) as $item (false; . or $item)'`" = "true" ]]; then
      cd $ARTIFACT_ROOT/build/evaluation/lift_stencil/artifact/ &&
      source environment.env &&
      export OCL_PLATFORM_ID=$OCL_CPU_PLATFORM_ID &&
      export OCL_DEVICE_ID=$OCL_CPU_DEVICE_ID &&
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."input sizes"[]'`; do
        cd $ARTIFACT_ROOT/build/evaluation/lift_stencil/artifact/ &&

        (
          # prepare benchmark
          is_0=`echo $is | jq -r '.[0]'`
          is_1=`echo $is | jq -r '.[1]'`
          rm -rf benchmarks/tmp &> /dev/null
          cp -r benchmarks/gaussian benchmarks/tmp &&
          cd benchmarks/tmp &&
          find . -type f -exec sed -i "s/DEVICE_ID/${OCL_CPU_DEVICE_ID}/g" {} + &&
          find . -type f -exec sed -i "s/INPUT_SIZE_0/${is_0}/g" {} + &&
          find . -type f -exec sed -i "s/INPUT_SIZE_1/${is_1}/g" {} + &&

          # generate kernel
          cd cpu &&
          ../../../scripts/utils/updateTimeout.sh `pwd` `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."tuning time (hours)" * 3600 | floor'` &&
          CONFIG=$(ls | grep json) &&
          HighLevelRewrite --file $CONFIG ../gaussian.lift &&
          MemoryMappingRewrite --file $CONFIG gaussian &&
          GenericKernelPrinter --file $CONFIG gaussian &&

          # tune
          cd gaussianCl
          CL=Cl &&
          TUNE=$PWD &&
          pushd $ATF/build &&
          rm *.cl
          rm *.csv
          rm *.log
          do_you_even_tune.sh $TUNE &&
          popd > /dev/null &&
          pushd $TUNE &&
          analyze_tuning.sh &&
          popd > /dev/null &&
          cd ../.. &&

          # save best kernel and configuration
          mkdir -p ${ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift &&
          cp cpu/gaussianCl/best.cl ${ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift/gaussian_`echo $is | jq -r '[.[] | tostring] | join("x")'`_kernel.cl &&
          hash=`grep -oP "(?<=// Low-level hash: ).*" cpu/gaussianCl/best.cl` &&
          tmp=`grep -oP "(?<=$hash.cl -> )\\d+" cpu/gaussianCl/summary.txt` &&
          gs_0=`grep -oP "(?<=$hash.cl -> $tmp \\()\\d+" cpu/gaussianCl/summary.txt` &&
          gs_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;)\\d+" cpu/gaussianCl/summary.txt` &&
          ls_0=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;)\\d+" cpu/gaussianCl/summary.txt` &&
          ls_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$ls_0;)\\d+" cpu/gaussianCl/summary.txt` &&
          printf "{\n    \"GLOBAL_SIZE_0\": $gs_0,\n    \"GLOBAL_SIZE_1\": $gs_1,\n    \"LOCAL_SIZE_0\": $ls_0,\n    \"LOCAL_SIZE_1\": $ls_1\n}" > ${ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift/gaussian_`echo $is | jq -r '[.[] | tostring] | join("x")'`_config.json
        )
      done
    fi

    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Jacobi (3D)".frameworks[] | test("^Lift$")) as $item (false; . or $item)'`" = "true" ]]; then
      cd $ARTIFACT_ROOT/build/evaluation/lift_stencil/artifact/ &&
      source environment.env &&
      export OCL_PLATFORM_ID=$OCL_CPU_PLATFORM_ID &&
      export OCL_DEVICE_ID=$OCL_CPU_DEVICE_ID &&
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Jacobi (3D)"."input sizes"[]'`; do
        cd $ARTIFACT_ROOT/build/evaluation/lift_stencil/artifact/ &&

        (
          # prepare benchmark
          is_0=`echo $is | jq -r '.[0]'`
          is_1=`echo $is | jq -r '.[1]'`
          is_2=`echo $is | jq -r '.[2]'`
          rm -rf benchmarks/tmp &> /dev/null
          cp -r benchmarks/j3d7pt benchmarks/tmp &&
          cd benchmarks/tmp &&
          find . -type f -exec sed -i "s/DEVICE_ID/${OCL_CPU_DEVICE_ID}/g" {} + &&
          find . -type f -exec sed -i "s/INPUT_SIZE_0/${is_0}/g" {} + &&
          find . -type f -exec sed -i "s/INPUT_SIZE_1/${is_1}/g" {} + &&
          find . -type f -exec sed -i "s/INPUT_SIZE_2/${is_2}/g" {} + &&

          # generate kernel
          cd cpu &&
          ../../../scripts/utils/updateTimeout.sh `pwd` `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Jacobi (3D)"."tuning time (hours)" * 3600 | floor'` &&
          CONFIG=$(ls | grep json) &&
          HighLevelRewrite --file $CONFIG ../j3d7pt.lift &&
          MemoryMappingRewrite --file $CONFIG j3d7pt &&
          GenericKernelPrinter --file $CONFIG j3d7pt &&

          # tune
          cd j3d7ptCl
          CL=Cl &&
          TUNE=$PWD &&
          pushd $ATF/build &&
          rm *.cl
          rm *.csv
          rm *.log
          do_you_even_tune.sh $TUNE &&
          popd > /dev/null &&
          pushd $TUNE &&
          analyze_tuning.sh &&
          popd > /dev/null &&
          cd ../.. &&

          # save best kernel and configuration
          mkdir -p ${ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift &&
          cp cpu/j3d7ptCl/best.cl ${ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift/j3d7pt_`echo $is | jq -r '[.[] | tostring] | join("x")'`_kernel.cl &&
          hash=`grep -oP "(?<=// Low-level hash: ).*" cpu/j3d7ptCl/best.cl` &&
          tmp=`grep -oP "(?<=$hash.cl -> )\\d+" cpu/j3d7ptCl/summary.txt` &&
          gs_0=`grep -oP "(?<=$hash.cl -> $tmp \\()\\d+" cpu/j3d7ptCl/summary.txt` &&
          gs_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;)\\d+" cpu/j3d7ptCl/summary.txt` &&
          gs_2=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;)\\d+" cpu/j3d7ptCl/summary.txt` &&
          ls_0=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;)\\d+" cpu/j3d7ptCl/summary.txt` &&
          ls_1=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;$ls_0;)\\d+" cpu/j3d7ptCl/summary.txt` &&
          ls_2=`grep -oP "(?<=$hash.cl -> $tmp \\($gs_0;$gs_1;$gs_2;$ls_0;$ls_1;)\\d+" cpu/j3d7ptCl/summary.txt` &&
          printf "{\n    \"GLOBAL_SIZE_0\": $gs_0,\n    \"GLOBAL_SIZE_1\": $gs_1,\n    \"GLOBAL_SIZE_2\": $gs_2,\n    \"LOCAL_SIZE_0\": $ls_0,\n    \"LOCAL_SIZE_1\": $ls_1,\n    \"LOCAL_SIZE_2\": $ls_2\n}" > ${ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift/j3d7pt_`echo $is | jq -r '[.[] | tostring] | join("x")'`_config.json
        )
      done
    fi
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMM.frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."input sizes"[]'`; do
      rm -rf opentuner.*
      rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/gemm_`echo $is | jq -r '[.[] | tostring] | join("x")'`_runtime &> /dev/null
      ./md_hom_initial_gemm --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'` --tuning-time `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."tuning time (hours)"'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMV.frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMV."input sizes"[]'`; do
      rm -rf opentuner.*
      rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/gemv_`echo $is | jq -r '[.[] | tostring] | join("x")'`_runtime &> /dev/null
      ./md_hom_initial_gemv --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'` --tuning-time `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMV."tuning time (hours)"'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Gaussian (2D)".frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."input sizes"[]'`; do
      rm -rf opentuner.*
      rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/gaussian_static_`echo $is | jq -r '[.[] | tostring] | join("x")'`_runtime &> /dev/null
      ./md_hom_initial_gaussian_static --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size `echo $is | jq -r '[.[]-4 | tostring] | join(" ")'` --tuning-time `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."tuning time (hours)"'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Jacobi (3D)".frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Jacobi (3D)"."input sizes"[]'`; do
      rm -rf opentuner.*
      rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/j3d7pt_static_`echo $is | jq -r '[.[] | tostring] | join("x")'`_runtime &> /dev/null
      ./md_hom_initial_j3d7pt_static --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size `echo $is | jq -r '[.[]-2 | tostring] | join(" ")'` --tuning-time `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Jacobi (3D)"."tuning time (hours)"'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Probabilistic Record Linkage".frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/
    rm -rf opentuner.*
    rm $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/rl_*_runtime &> /dev/null
    ./md_hom_initial_rl --mode tune --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size 1024 1024 --tuning-time `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Probabilistic Record Linkage"."tuning time (hours)"'`
  fi

  printf "\n\nReference tuning successful!\n"
} || {
  printf "\n\nReference tuning failed!\n"
  exit 1
}