#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
{
  if [[ -n "$ENABLE_CPU" && "$ENABLE_CPU" -eq 1 ]]; then
    $ARTIFACT_ROOT/scripts/use_cpu_defaults.sh
  fi
  if [[ -n "$ENABLE_GPU" && "$ENABLE_GPU" -eq 1 ]]; then
    $ARTIFACT_ROOT/scripts/use_gpu_defaults.sh
  fi
} || {
  printf "\n\Using defaults failed!\n"
  exit 1
}