import sys
import glob
import os
import re
from os import listdir
from os.path import isdir, join
from tabulate import tabulate

results_dir = sys.argv[1]

def build_map(path, application):
    runtimes = {}
    for r in glob.glob("{0}/{1}_*_runtime".format(path, application)):
        file_name = os.path.split(r)[-1]
        size = re.match("{0}_(.*)_runtime".format(application), file_name).group(1)
        # replace input sizes with keys used in the paper
        if "gemm" in application:
            if size == "10x500x64":
                size = "RW"
            elif size == "1024x1024x1024":
                size = "PC"
        elif "gemv" in application:
            if size == "4096x4096":
                size = "RW"
            elif size == "8192x8192":
                size = "PC"
        elif "gaussian" in application:
            if size == "224x224":
                size = "RW"
            elif size == "4096x4096":
                size = "PC"
        elif "j3d7pt" in application:
            if size == "256x256x256":
                size = "RW"
            elif size == "512x512x512":
                size = "PC"
        elif "multi_channel_convolution" in application:
            if size == "1x512x7x7x512":
                size = "RW"
        elif "rl" in application:
            if size == "32768x32768":
                size = "2^15"
            elif size == "65536x65536":
                size = "2^16"
            elif size == "131072x131072":
                size = "2^17"
            elif size == "262144x262144":
                size = "2^18"
            elif size == "524288x524288":
                size = "2^19"
            elif size == "1048576x1048576":
                size = "2^20"
        elif "tc" in application:
            if size == "abcdef_gdab_efgc":
                size = "RW 1"
            elif size == "abcdef_gdac_efgb":
                size = "RW 2"
            elif size == "abcdef_gdbc_efga":
                size = "RW 3"
            elif size == "abcdef_geab_dfgc":
                size = "RW 4"
            elif size == "abcdef_geac_dfgb":
                size = "RW 5"
            elif size == "abcdef_gebc_dfga":
                size = "RW 6"
            elif size == "abcdef_gfab_degc":
                size = "RW 7"
            elif size == "abcdef_gfac_degb":
                size = "RW 8"
            elif size == "abcdef_gfbc_dega":
                size = "RW 9"

        with open(r, "r") as file:
            runtimes[size] = float(file.read())
    return runtimes


# CPU
if isdir("{0}/cpu".format(results_dir)):
    mkl_gemm = build_map("{0}/cpu/mkl/".format(results_dir), "gemm")
    mkl_gemv = build_map("{0}/cpu/mkl/".format(results_dir), "gemv")
    mkl_jit_gemm = build_map("{0}/cpu/mkl_jit/".format(results_dir), "gemm")
    mkldnn_gaussian = build_map("{0}/cpu/mkldnn/".format(results_dir), "gaussian")
    mkldnn_mcc = build_map("{0}/cpu/mkldnn/".format(results_dir), "multi_channel_convolution")
    ekr_rl = build_map("{0}/cpu/ekr/".format(results_dir), "rl")
    # adapt runtime unit
    for key in ekr_rl:
        ekr_rl[key] = ekr_rl[key] / 1000

    for platform_id in [f for f in listdir("{0}/cpu/".format(results_dir)) if isdir(join("{0}/cpu/".format(results_dir), f))]:
        if not re.match("^[0-9]+$", platform_id):
            continue
        for device_id in [f for f in listdir("{0}/cpu/{1}".format(results_dir, platform_id)) if isdir(join("{0}/cpu/{1}".format(results_dir, platform_id), f))]:
            if not re.match("^[0-9]+$", device_id):
                continue

            lift_dir = "{0}/cpu/{1}/{2}/lift".format(results_dir, platform_id, device_id)
            lift_gaussian = build_map(lift_dir, "gaussian")
            lift_j3d7pt = build_map(lift_dir, "j3d7pt")
            lift_gemm = build_map(lift_dir, "gemm")
            lift_gemv = build_map(lift_dir, "gemv")

            md_hom_initial_dir = "{0}/cpu/{1}/{2}/md_hom_initial".format(results_dir, platform_id, device_id)
            md_hom_initial_gaussian_static = build_map(md_hom_initial_dir, "gaussian_static")
            md_hom_initial_j3d7pt_static = build_map(md_hom_initial_dir, "j3d7pt_static")
            md_hom_initial_gemm = build_map(md_hom_initial_dir, "gemm")
            md_hom_initial_gemv = build_map(md_hom_initial_dir, "gemv")
            md_hom_initial_rl = build_map(md_hom_initial_dir, "rl")

            md_hom_new_dir = "{0}/cpu/{1}/{2}/md_hom_new".format(results_dir, platform_id, device_id)
            md_hom_new_gaussian_static = build_map(md_hom_new_dir, "gaussian_static")
            md_hom_new_j3d7pt_static = build_map(md_hom_new_dir, "j3d7pt_static")
            md_hom_new_gemm = build_map(md_hom_new_dir, "gemm")
            md_hom_new_gemv = build_map(md_hom_new_dir, "gemv")
            md_hom_new_mcc = build_map(md_hom_new_dir, "multi_channel_convolution")
            md_hom_new_rl = build_map(md_hom_new_dir, "rl")

            print("")
            print("")
            print("=== CPU (Platform: {0}, Device: {1}) ===".format(platform_id, device_id))
            print("")

            print("--- GEMM: Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_gemm:
                md_hom_new_runtime = md_hom_new_gemm[key]
                speedup_md_hom_initial = None
                if key in md_hom_initial_gemm:
                    speedup_md_hom_initial = md_hom_initial_gemm[key] / md_hom_new_runtime
                speedup_lift = None
                if key in lift_gemm:
                    speedup_lift = lift_gemm[key] / md_hom_new_runtime
                speedup_mkl = None
                if key in mkl_gemm:
                    speedup_mkl = mkl_gemm[key] / md_hom_new_runtime
                speedup_mkl_jit = None
                if key in mkl_jit_gemm:
                    speedup_mkl_jit = mkl_jit_gemm[key] / md_hom_new_runtime
                data.append([key, speedup_md_hom_initial, speedup_lift, speedup_mkl, speedup_mkl_jit])
            print(tabulate(data, headers=["input size", "md_hom_initial", "Lift", "Intel MKL", "Intel MKL-JIT"]))
            print("")

            print("--- GEMV: Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_gemv:
                md_hom_new_runtime = md_hom_new_gemv[key]
                speedup_md_hom_initial = None
                if key in md_hom_initial_gemv:
                    speedup_md_hom_initial = md_hom_initial_gemv[key] / md_hom_new_runtime
                speedup_lift = None
                if key in lift_gemv:
                    speedup_lift = lift_gemv[key] / md_hom_new_runtime
                speedup_mkl = None
                if key in mkl_gemv:
                    speedup_mkl = mkl_gemv[key] / md_hom_new_runtime
                data.append([key, speedup_md_hom_initial, speedup_lift, speedup_mkl])
            print(tabulate(data, headers=["input size", "md_hom_initial", "Lift", "Intel MKL"]))
            print("")

            print("--- Gaussian (2D): Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_gaussian_static:
                speedup_md_hom_initial = None
                if key in md_hom_initial_gaussian_static:
                    speedup_md_hom_initial = md_hom_initial_gaussian_static[key] / md_hom_new_gaussian_static[key]
                speedup_lift = None
                if key in lift_gaussian:
                    speedup_lift = lift_gaussian[key] / md_hom_new_gaussian_static[key]
                speedup_mkldnn = None
                if key in mkldnn_gaussian:
                    speedup_mkldnn = mkldnn_gaussian[key] / md_hom_new_gaussian_static[key]
                data.append([key, speedup_md_hom_initial, speedup_lift, speedup_mkldnn])
            print(tabulate(data, headers=["input size", "md_hom_initial", "Lift", "Intel MKL-DNN"]))
            print("")

            print("--- Jacobi (3D): Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_j3d7pt_static:
                md_hom_new_runtime = md_hom_new_j3d7pt_static[key]
                speedup_md_hom_initial = None
                if key in md_hom_initial_j3d7pt_static:
                    speedup_md_hom_initial = md_hom_initial_j3d7pt_static[key] / md_hom_new_runtime
                speedup_lift = None
                if key in lift_j3d7pt:
                    speedup_lift = lift_j3d7pt[key] / md_hom_new_runtime
                data.append([key, speedup_md_hom_initial, speedup_lift])
            print(tabulate(data, headers=["input size", "md_hom_initial", "Lift"]))
            print("")

            print("--- Multi Channel Convolution: Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_mcc:
                md_hom_new_runtime = md_hom_new_mcc[key]
                speedup_mkldnn = None
                if key in mkldnn_mcc:
                    speedup_mkldnn = mkldnn_mcc[key] / md_hom_new_runtime
                data.append([key, speedup_mkldnn])
            print(tabulate(data, headers=["input size", "Intel MKL-DNN"]))
            print("")

            print("--- Record Linkage: Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_rl:
                md_hom_new_runtime = md_hom_new_rl[key]
                speedup_ekr = None
                if key in ekr_rl:
                    speedup_ekr = ekr_rl[key] / md_hom_new_runtime
                speedup_md_hom_initial = None
                if key in md_hom_initial_rl:
                    speedup_md_hom_initial = md_hom_initial_rl[key] / md_hom_new_runtime
                data.append([key, speedup_ekr, speedup_md_hom_initial])
            print(tabulate(data, headers=["input size", "EKR", "md_hom_initial"]))
            print("")


# GPU
if isdir("{0}/gpu".format(results_dir)):
    cublas_gemm = build_map("{0}/gpu/cublas/".format(results_dir), "gemm")
    cublas_gemv = build_map("{0}/gpu/cublas/".format(results_dir), "gemv")
    cublaslt_gemm = build_map("{0}/gpu/cublaslt/".format(results_dir), "gemm")
    cudnn_gaussian = build_map("{0}/gpu/cudnn/".format(results_dir), "gaussian")
    cudnn_mcc = build_map("{0}/gpu/cudnn/".format(results_dir), "multi_channel_convolution")
    cogent_tc = build_map("{0}/gpu/cogent/".format(results_dir), "tc")
    tc_tc = build_map("{0}/gpu/tensor_comprehensions/".format(results_dir), "tc")
    ekr_rl = build_map("{0}/cpu/ekr/".format(results_dir), "rl")
    # adapt runtime unit
    for key in ekr_rl:
        ekr_rl[key] = ekr_rl[key] / 1000

    for platform_id in [f for f in listdir("{0}/gpu/".format(results_dir)) if isdir(join("{0}/gpu/".format(results_dir), f))]:
        if not re.match("^[0-9]+$", platform_id):
            continue
        for device_id in [f for f in listdir("{0}/gpu/{1}".format(results_dir, platform_id)) if isdir(join("{0}/gpu/{1}".format(results_dir, platform_id), f))]:
            if not re.match("^[0-9]+$", device_id):
                continue

            lift_dir = "{0}/gpu/{1}/{2}/lift".format(results_dir, platform_id, device_id)
            lift_gaussian = build_map(lift_dir, "gaussian")
            lift_j3d7pt = build_map(lift_dir, "j3d7pt")
            lift_gemm = build_map(lift_dir, "gemm")
            lift_gemv = build_map(lift_dir, "gemv")

            md_hom_initial_dir = "{0}/gpu/{1}/{2}/md_hom_initial".format(results_dir, platform_id, device_id)
            md_hom_initial_gaussian_static = build_map(md_hom_initial_dir, "gaussian_static")
            md_hom_initial_j3d7pt_static = build_map(md_hom_initial_dir, "j3d7pt_static")
            md_hom_initial_gemm = build_map(md_hom_initial_dir, "gemm")
            md_hom_initial_gemv = build_map(md_hom_initial_dir, "gemv")
            md_hom_initial_tc = build_map(md_hom_initial_dir, "tc")
            md_hom_initial_rl = build_map(md_hom_initial_dir, "rl")

            md_hom_new_dir = "{0}/gpu/{1}/{2}/md_hom_new".format(results_dir, platform_id, device_id)
            md_hom_new_gaussian_static = build_map(md_hom_new_dir, "gaussian_static")
            md_hom_new_j3d7pt_static = build_map(md_hom_new_dir, "j3d7pt_static")
            md_hom_new_gemm = build_map(md_hom_new_dir, "gemm")
            md_hom_new_gemv = build_map(md_hom_new_dir, "gemv")
            md_hom_new_mcc = build_map(md_hom_new_dir, "multi_channel_convolution")
            md_hom_new_tc = build_map(md_hom_new_dir, "tc")
            md_hom_new_rl = build_map(md_hom_new_dir, "rl")

            print("")
            print("")
            print("=== GPU (Platform: {0}, Device: {1}) ===".format(platform_id, device_id))
            print("")

            print("--- GEMM: Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_gemm:
                md_hom_new_runtime = md_hom_new_gemm[key]
                speedup_md_hom_initial = None
                if key in md_hom_initial_gemm:
                    speedup_md_hom_initial = md_hom_initial_gemm[key] / md_hom_new_runtime
                speedup_lift = None
                if key in lift_gemm:
                    speedup_lift = lift_gemm[key] / md_hom_new_runtime
                speedup_cublas = None
                if key in cublas_gemm:
                    speedup_cublas = cublas_gemm[key] / md_hom_new_runtime
                speedup_cublaslt = None
                if key in cublaslt_gemm:
                    speedup_cublaslt = cublaslt_gemm[key] / md_hom_new_runtime
                data.append([key, speedup_md_hom_initial, speedup_lift, speedup_cublas, speedup_cublaslt])
            print(tabulate(data, headers=["input size", "md_hom_initial", "Lift", "NVIDIA cuBLAS", "NVIDIA cuBLASLt"]))
            print("")

            print("--- GEMV: Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_gemv:
                md_hom_new_runtime = md_hom_new_gemv[key]
                speedup_md_hom_initial = None
                if key in md_hom_initial_gemv:
                    speedup_md_hom_initial = md_hom_initial_gemv[key] / md_hom_new_runtime
                speedup_lift = None
                if key in lift_gemv:
                    speedup_lift = lift_gemv[key] / md_hom_new_runtime
                speedup_cublas = None
                if key in cublas_gemv:
                    speedup_cublas = cublas_gemv[key] / md_hom_new_runtime
                data.append([key, speedup_md_hom_initial, speedup_lift, speedup_cublas])
            print(tabulate(data, headers=["input size", "md_hom_initial", "Lift", "NVIDIA cuBLAS"]))
            print("")

            print("--- Gaussian (2D): Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_gaussian_static:
                speedup_md_hom_initial = None
                if key in md_hom_initial_gaussian_static:
                    speedup_md_hom_initial = md_hom_initial_gaussian_static[key] / md_hom_new_gaussian_static[key]
                speedup_lift = None
                if key in lift_gaussian:
                    speedup_lift = lift_gaussian[key] / md_hom_new_gaussian_static[key]
                speedup_cudnn = None
                if key in cudnn_gaussian:
                    speedup_cudnn = cudnn_gaussian[key] / md_hom_new_gaussian_static[key]
                data.append([key, speedup_md_hom_initial, speedup_lift, speedup_cudnn])
            print(tabulate(data, headers=["input size", "md_hom_initial", "Lift", "NVIDIA cuDNN"]))
            print("")

            print("--- Jacobi (3D): Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_j3d7pt_static:
                md_hom_new_runtime = md_hom_new_j3d7pt_static[key]
                speedup_md_hom_initial = None
                if key in md_hom_initial_j3d7pt_static:
                    speedup_md_hom_initial = md_hom_initial_j3d7pt_static[key] / md_hom_new_runtime
                speedup_lift = None
                if key in lift_j3d7pt:
                    speedup_lift = lift_j3d7pt[key] / md_hom_new_runtime
                data.append([key, speedup_md_hom_initial, speedup_lift])
            print(tabulate(data, headers=["input size", "md_hom_initial", "Lift"]))
            print("")

            print("--- Multi Channel Convolution: Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_mcc:
                md_hom_new_runtime = md_hom_new_mcc[key]
                speedup_cudnn = None
                if key in cudnn_mcc:
                    speedup_cudnn = cudnn_mcc[key] / md_hom_new_runtime
                data.append([key, speedup_cudnn])
            print(tabulate(data, headers=["input size", "NVIDIA cuDNN"]))
            print("")

            print("--- Tensor Contractions: Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_tc:
                md_hom_new_runtime = md_hom_new_tc[key]
                speedup_md_hom_initial = None
                if key in md_hom_initial_tc:
                    speedup_md_hom_initial = md_hom_initial_tc[key] / md_hom_new_runtime
                speedup_cogent = None
                if key in cogent_tc:
                    speedup_cogent = cogent_tc[key] / md_hom_new_runtime
                speedup_tc = None
                if key in tc_tc:
                    speedup_tc = tc_tc[key] / md_hom_new_runtime
                data.append([key, speedup_md_hom_initial, speedup_cogent, speedup_tc])
            print(tabulate(data, headers=["input size", "md_hom_initial", "COGENT", "Tensor Comprehensions"]))
            print("")

            print("--- Record Linkage: Speedup of md_hom over references ---")
            data = []
            for key in md_hom_new_rl:
                md_hom_new_runtime = md_hom_new_rl[key]
                speedup_ekr = None
                if key in ekr_rl:
                    speedup_ekr = ekr_rl[key] / md_hom_new_runtime
                speedup_md_hom_initial = None
                if key in md_hom_initial_rl:
                    speedup_md_hom_initial = md_hom_initial_rl[key] / md_hom_new_runtime
                data.append([key, speedup_ekr, speedup_md_hom_initial])
            print(tabulate(data, headers=["input size", "EKR", "md_hom_initial"]))
            print("")
