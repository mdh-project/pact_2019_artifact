#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi
: ${OCL_CPU_PLATFORM_ID?"Please set the environment variable OCL_CPU_PLATFORM_ID."}
if [ -z "$OCL_CPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_CPU_DEVICE_ID?"Please set the environment variable OCL_CPU_DEVICE_ID."}
if [ -z "$OCL_CPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
{
  rm -rf results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/* &> /dev/null
  mkdir -p results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/ &> /dev/null
  cp -r defaults/cpu/* results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/ &&
  printf "\n\nUsing cpu defaults. Note that performance might be suboptimal!\n"
} || {
  printf "\n\nFailed to use cpu defaults. This should not be happening!\n"
  exit 1
}