#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi
: ${OCL_CPU_PLATFORM_ID?"Please set the environment variable OCL_CPU_PLATFORM_ID."}
if [ -z "$OCL_CPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_CPU_DEVICE_ID?"Please set the environment variable OCL_CPU_DEVICE_ID."}
if [ -z "$OCL_CPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_CPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
{
  if [[ -n "$ENABLE_MKL" && "$ENABLE_MKL" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMM.frameworks[] | test("^MKL$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Intel MKL
      cd $ARTIFACT_ROOT/build/evaluation/mkl &&
      mkdir -p ${ARTIFACT_ROOT}/results/cpu/mkl
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."input sizes"[]'`; do
        LD_LIBRARY_PATH=${MKLROOT}/lib/intel64:${ARTIFACT_ROOT}/extern/mkl-dnn/external/mklml_lnx_2019.0.3.20190220/lib/:$LD_LIBRARY_PATH ./mkl_gemm --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
      done
    fi
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMV.frameworks[] | test("^MKL$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Intel MKL
      cd $ARTIFACT_ROOT/build/evaluation/mkl &&
      mkdir -p ${ARTIFACT_ROOT}/results/cpu/mkl
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMV."input sizes"[]'`; do
        LD_LIBRARY_PATH=${MKLROOT}/lib/intel64:${ARTIFACT_ROOT}/extern/mkl-dnn/external/mklml_lnx_2019.0.3.20190220/lib/:$LD_LIBRARY_PATH ./mkl_gemv --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
      done
    fi

    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMM.frameworks[] | test("^MKL-JIT$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Intel MKL-JIT
      cd $ARTIFACT_ROOT/build/evaluation/mkl_jit &&
      mkdir -p ${ARTIFACT_ROOT}/results/cpu/mkl_jit
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."input sizes"[]'`; do
        LD_LIBRARY_PATH=${MKLROOT}/lib/intel64:${ARTIFACT_ROOT}/extern/mkl-dnn/external/mklml_lnx_2019.0.3.20190220/lib/:$LD_LIBRARY_PATH ./mkl_jit_gemm --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
      done
    fi
  fi

  if [[ -n "$ENABLE_MKL_DNN" && "$ENABLE_MKL_DNN" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Gaussian (2D)".frameworks[] | test("^MKL-DNN$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Intel MKL-DNN
      cd $ARTIFACT_ROOT/build/evaluation/mkldnn &&
      mkdir -p ${ARTIFACT_ROOT}/results/cpu/mkldnn
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."input sizes"[]'`; do
        ./mkldnn_gaussian --input-size `echo $is | jq -r '[.[]-4 | tostring] | join(" ")'`
      done
    fi

    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Multi-Channel Convolution".frameworks[] | test("^MKL-DNN$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Intel MKL-DNN
      cd $ARTIFACT_ROOT/build/evaluation/mkldnn &&
      mkdir -p ${ARTIFACT_ROOT}/results/cpu/mkldnn
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Multi-Channel Convolution"."input sizes"[]'`; do
        ./mkldnn_multi_channel_convolution --input-size `echo $is | jq -r '[(.[0:2]+[(.[2:4] | .[]-2)]+.[4:5])[] | tostring] | join(" ")'`
      done
    fi
  fi

  if [[ -n "$ENABLE_LIFT" && "$ENABLE_LIFT" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMM.frameworks[] | test("^Lift$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Lift BLAS
      cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
      (
        export PARENT_ARTIFACT_ROOT=$ARTIFACT_ROOT
        export ARTIFACT_ROOT=`pwd`

        for is in `cat $PARENT_ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."input sizes"[]'`; do
          {
            lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_CPU_PLATFORM_ID -d $OCL_CPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s `echo $is | jq -r '[.[] | tostring] | join(" -s ")'` > gemm_lift_`echo $is | jq -r '[.[] | tostring] | join("x")'`.log &&
            mkdir -p ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift &&
            grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_`echo $is | jq -r '[.[] | tostring] | join("x")'`.log > ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift/gemm_`echo $is | jq -r '[.[] | tostring] | join("x")'`_runtime
          }
        done
      )
    fi

    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMV.frameworks[] | test("^Lift$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Lift BLAS
      cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
      (
        export PARENT_ARTIFACT_ROOT=$ARTIFACT_ROOT
        export ARTIFACT_ROOT=`pwd`

        for is in `cat $PARENT_ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMV."input sizes"[]'`; do
          {
            lift/scripts/MatrixVector --iterations 210 --variant 3 --ig --il -s `echo $is | jq -r '[.[] | tostring] | join(" -s ")'` -l 64 -g 4096 --loadKernel generated_kernels/gemv/gemv_N.cl -p $OCL_CPU_PLATFORM_ID -d $OCL_CPU_DEVICE_ID > gemv_lift_`echo $is | jq -r '[.[] | tostring] | join("x")'`.log &&
            mkdir -p ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift &&
            grep -oP "(?<=MIN: )\\d+\\.\\d+" gemv_lift_`echo $is | jq -r '[.[] | tostring] | join("x")'`.log > ${PARENT_ARTIFACT_ROOT}/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/lift/gemv_`echo $is | jq -r '[.[] | tostring] | join("x")'`_runtime
          }
        done
      )
    fi
  fi

  if [[ -n "$ENABLE_LIFT" && "$ENABLE_LIFT" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Gaussian (2D)".frameworks[] | test("^Lift$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Lift stencil
      cd $ARTIFACT_ROOT/build/evaluation/lift_stencil &&
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."input sizes"[]'`; do
        ./bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --application gaussian --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'` 1
      done
    fi

    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Jacobi (3D)".frameworks[] | test("^Lift$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Lift stencil
      cd $ARTIFACT_ROOT/build/evaluation/lift_stencil &&
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Jacobi (3D)"."input sizes"[]'`; do
        ./bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --application j3d7pt --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
      done
    fi
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMM.frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."input sizes"[]'`; do
      ./md_hom_initial_gemm --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMV.frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMV."input sizes"[]'`; do
      ./md_hom_initial_gemv --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Gaussian (2D)".frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."input sizes"[]'`; do
      ./md_hom_initial_gaussian_static --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size `echo $is | jq -r '[.[]-4 | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Jacobi (3D)".frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Jacobi (3D)"."input sizes"[]'`; do
      ./md_hom_initial_j3d7pt_static --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size `echo $is | jq -r '[.[]-2 | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Probabilistic Record Linkage".frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/$OCL_CPU_PLATFORM_ID/$OCL_CPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Probabilistic Record Linkage"."input sizes"[]'`; do
      ./md_hom_initial_rl --mode bench --platform-id $OCL_CPU_PLATFORM_ID --device-id $OCL_CPU_DEVICE_ID --input-size $is $is
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Probabilistic Record Linkage".frameworks[] | test("^EKR$")) as $item (false; . or $item)'`" = "true" ]]; then
    # EKR
    cd $ARTIFACT_ROOT/build/evaluation/ekr &&
    mkdir -p $ARTIFACT_ROOT/results/cpu/ekr/
    threads=`nproc --all`
    threads=`echo "$threads*2" | bc`
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Probabilistic Record Linkage"."input sizes"[]'`; do
      java -Xms512m -Xmx14g -jar record-linkage-java.jar $is $is $threads | grep -oP "[0-9]+" > $ARTIFACT_ROOT/results/cpu/ekr/rl_${is}x${is}_runtime
    done
  fi

  printf "\n\nReference execution successful!\n"
} || {
  printf "\n\nReference execution failed!\n"
  exit 1
}