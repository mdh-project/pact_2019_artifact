#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi
: ${OCL_GPU_PLATFORM_ID?"Please set the environment variable OCL_GPU_PLATFORM_ID."}
if [ -z "$OCL_GPU_PLATFORM_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_PLATFORM_ID."
	exit 1
fi
: ${OCL_GPU_DEVICE_ID?"Please set the environment variable OCL_GPU_DEVICE_ID."}
if [ -z "$OCL_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable OCL_GPU_DEVICE_ID."
	exit 1
fi
: ${CUDA_GPU_DEVICE_ID?"Please set the environment variable CUDA_GPU_DEVICE_ID."}
if [ -z "$CUDA_GPU_DEVICE_ID" ]
then
    	echo "Please set the environment variable CUDA_GPU_DEVICE_ID."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
{
  if [[ -n "$ENABLE_CUBLAS" && "$ENABLE_CUBLAS" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMM.frameworks[] | test("^cuBLAS$")) as $item (false; . or $item)'`" = "true" ]]; then
      # NVIDIA cuBLAS
      cd $ARTIFACT_ROOT/build/evaluation/cublas &&
      mkdir -p ${ARTIFACT_ROOT}/results/gpu/cublas
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."input sizes"[]'`; do
        ./cublas_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
      done
    fi
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMV.frameworks[] | test("^cuBLAS$")) as $item (false; . or $item)'`" = "true" ]]; then
      # NVIDIA cuBLAS
      cd $ARTIFACT_ROOT/build/evaluation/cublas &&
      mkdir -p ${ARTIFACT_ROOT}/results/gpu/cublas
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMV."input sizes"[]'`; do
        ./cublas_gemv --device-id $CUDA_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
      done
    fi
  fi

  if [[ -n "$ENABLE_CUBLASLT" && "$ENABLE_CUBLASLT" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMM.frameworks[] | test("^cuBLASLt$")) as $item (false; . or $item)'`" = "true" ]]; then
      # NVIDIA cuBLASLt
      cd $ARTIFACT_ROOT/build/evaluation/cublaslt &&
      mkdir -p ${ARTIFACT_ROOT}/results/gpu/cublaslt
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."input sizes"[]'`; do
        ./cublaslt_gemm --device-id $CUDA_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
      done
    fi
  fi

  if [[ -n "$ENABLE_CUDNN" && "$ENABLE_CUDNN" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Gaussian (2D)".frameworks[] | test("^cuDNN$")) as $item (false; . or $item)'`" = "true" ]]; then
      # NVIDIA cuDNN
      cd $ARTIFACT_ROOT/build/evaluation/cudnn &&
      mkdir -p ${ARTIFACT_ROOT}/results/gpu/cudnn
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."input sizes"[]'`; do
        ./cudnn_gaussian --device-id $CUDA_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[]-4 | tostring] | join(" ")'`
      done
    fi

    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Multi-Channel Convolution".frameworks[] | test("^cuDNN$")) as $item (false; . or $item)'`" = "true" ]]; then
      # NVIDIA cuDNN
      cd $ARTIFACT_ROOT/build/evaluation/cudnn &&
      mkdir -p ${ARTIFACT_ROOT}/results/gpu/cudnn
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Multi-Channel Convolution"."input sizes"[]'`; do
        ./cudnn_multi_channel_convolution --device-id $CUDA_GPU_DEVICE_ID --input-size `echo $is | jq -r '[(.[0:2]+[(.[2:4] | .[]-2)]+.[4:5])[] | tostring] | join(" ")'`
      done
    fi
  fi

  if [[ -n "$ENABLE_LIFT" && "$ENABLE_LIFT" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMM.frameworks[] | test("^Lift$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Lift BLAS
      cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
      (
        export PARENT_ARTIFACT_ROOT=$ARTIFACT_ROOT
        export ARTIFACT_ROOT=`pwd`

        for is in `cat $PARENT_ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."input sizes"[]'`; do
          {
            lift/scripts/MatrixMultiplication --iterations 210 --loadKernel generated_kernels/mm_nvidia/mm_nvidia.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID -l 32 -l 8 -g 256 -g 128 -s `echo $is | jq -r '[.[] | tostring] | join(" -s ")'` > gemm_lift_`echo $is | jq -r '[.[] | tostring] | join("x")'`.log &&
            mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
            grep -oP "(?<=MIN: )\\d+\\.\\d+" gemm_lift_`echo $is | jq -r '[.[] | tostring] | join("x")'`.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemm_`echo $is | jq -r '[.[] | tostring] | join("x")'`_runtime
          }
        done
      )
    fi

    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMV.frameworks[] | test("^Lift$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Lift BLAS
      cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
      (
        export PARENT_ARTIFACT_ROOT=$ARTIFACT_ROOT
        export ARTIFACT_ROOT=`pwd`

        for is in `cat $PARENT_ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMV."input sizes"[]'`; do
          {
            lift/scripts/MatrixVector --iterations 210 --variant 3 --ig --il -s `echo $is | jq -r '[.[] | tostring] | join(" -s ")'` -l 64 -g 4096 --loadKernel generated_kernels/gemv/gemv_N.cl -p $OCL_GPU_PLATFORM_ID -d $OCL_GPU_DEVICE_ID > gemv_lift_`echo $is | jq -r '[.[] | tostring] | join("x")'`.log &&
            mkdir -p ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift &&
            grep -oP "(?<=MIN: )\\d+\\.\\d+" gemv_lift_`echo $is | jq -r '[.[] | tostring] | join("x")'`.log > ${PARENT_ARTIFACT_ROOT}/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/lift/gemv_`echo $is | jq -r '[.[] | tostring] | join("x")'`_runtime
          }
        done
      )
    fi
  fi

  if [[ -n "$ENABLE_LIFT" && "$ENABLE_LIFT" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Gaussian (2D)".frameworks[] | test("^Lift$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Lift stencil
      cd $ARTIFACT_ROOT/build/evaluation/lift_stencil &&
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."input sizes"[]'`; do
        ./bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --application gaussian --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'` 1
      done
    fi

    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Jacobi (3D)".frameworks[] | test("^Lift$")) as $item (false; . or $item)'`" = "true" ]]; then
      # Lift stencil
      cd $ARTIFACT_ROOT/build/evaluation/lift_stencil &&
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Jacobi (3D)"."input sizes"[]'`; do
        ./bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --application j3d7pt --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
      done
    fi
  fi

  if [[ -n "$ENABLE_COGENT" && "$ENABLE_COGENT" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Tensor Contractions".frameworks[] | test("^COGENT$")) as $item (false; . or $item)'`" = "true" ]]; then
      # COGENT
      cd ${ARTIFACT_ROOT}/build/evaluation/cogent
      mkdir -p $ARTIFACT_ROOT/results/gpu/cogent/
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Tensor Contractions"."input sizes"[]'`; do
        cp bench_pattern.sh bench.sh &&
        sed -i "s/INPUT_SIZE/`echo $is | jq -r '[.[1][] | tostring] | join(" ")'`/g" bench.sh &&
        cp input_strings/tccg/input_tccg_49_pattern.in input_strings/tccg/input_tccg_49.in
        sed -i "s/INPUT_DIMS/`echo $is | jq -r '[.[0][] | tostring] | join("-")'`/g" input_strings/tccg/input_tccg_49.in &&
        sed -i "s/INPUT_SIZE/`echo $is | jq -r '[.[1][] | tostring] | join(" ")'`/g" input_strings/tccg/input_tccg_49.in &&
        sed -i "s/DIMS_T3/`echo $is | jq -r '[.[0][0] | split("")[] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
        sed -i "s/DIMS_T2/`echo $is | jq -r '[.[0][1] | split("")[] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
        sed -i "s/DIMS_V2/`echo $is | jq -r '[.[0][2] | split("")[] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
        sed -i "s/DIMS_REDUCE/`echo $is | jq -r '[(def intersection(x;y):      ( (x|unique) + (y|unique) | sort) as $sorted      | reduce range(1; $sorted|length) as $i          ([]; if $sorted[$i] == $sorted[$i-1] then . + [$sorted[$i]] else . end) ; intersection(.[0][1]|split("");.[0][2]|split(""))-(.[0][0]|split("")))[] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
        sed -i "s/DIMS_SIZES_T3/`echo $is | jq -r '[([([.[0][] | split("")] | add | unique | sort), .[1]] | transpose | map( {(.[0]): .[1]}) | add) as $sizes | (.[0][0] | split(""))[] | ., $sizes[.] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
        sed -i "s/DIMS_SIZES_REDUCE/`echo $is | jq -r '[([([.[0][] | split("")] | add | unique | sort), .[1]] | transpose | map( {(.[0]): .[1]}) | add) as $sizes | (def intersection(x;y):      ( (x|unique) + (y|unique) | sort) as $sorted      | reduce range(1; $sorted|length) as $i          ([]; if $sorted[$i] == $sorted[$i-1] then . + [$sorted[$i]] else . end) ; intersection(.[0][1]|split("");.[0][2]|split(""))-(.[0][0]|split("")))[] | ., $sizes[.] | tostring] | join(",")'`/g" input_strings/tccg/input_tccg_49.in &&
        cp drivers/tccg-float/code_main_tccg_49_pattern.c drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/INPUT_DIMS/`echo $is | jq -r '[.[0][] | tostring] | join("-")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/DIMS_T2/`echo $is | jq -r '[.[0][1] | split("")[] | tostring] | join(",")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/DIMS_V2/`echo $is | jq -r '[.[0][2] | split("")[] | tostring] | join(",")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/DIMS_SIZES_T3/`echo $is | jq -r '[([([.[0][] | split("")] | add | unique | sort), .[1]] | transpose | map( {(.[0]): .[1]}) | add) as $sizes | (.[0][0] | split(""))[] | ., $sizes[.] | tostring] | join(",")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/DIMS_SIZES_REDUCE/`echo $is | jq -r '[([([.[0][] | split("")] | add | unique | sort), .[1]] | transpose | map( {(.[0]): .[1]}) | add) as $sizes | (def intersection(x;y):      ( (x|unique) + (y|unique) | sort) as $sorted      | reduce range(1; $sorted|length) as $i          ([]; if $sorted[$i] == $sorted[$i-1] then . + [$sorted[$i]] else . end) ; intersection(.[0][1]|split("");.[0][2]|split(""))-(.[0][0]|split("")))[] | ., $sizes[.] | tostring] | join(",")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/SIZE_VARS/`echo $is | jq -r '[([.[0][] | split("")] | add | unique | sort)[] | "size_idx_"+.+" = 16"] | join(", ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/NUM_INPUTS/`echo $is | jq -r '[.[0][] | split("")] | add | unique | sort | length'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/READ_ARGS/`echo $is | jq -r '[([.[0][] | split("")] | add | unique | sort)[] | "size_idx_"+.+" = atoi(argv[++i]);"] | join(" ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/REDUCE_VARS/`echo $is | jq -r '[(def intersection(x;y):      ( (x|unique) + (y|unique) | sort) as $sorted      | reduce range(1; $sorted|length) as $i          ([]; if $sorted[$i] == $sorted[$i-1] then . + [$sorted[$i]] else . end) ; intersection(.[0][1]|split("");.[0][2]|split(""))-(.[0][0]|split("")))[] | tostring | "size_idx_"+.] | join(" * ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/T3_VARS/`echo $is | jq -r '[.[0][0] | split("")[] | tostring | "size_idx_"+.] | join(" * ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/T2_VARS/`echo $is | jq -r '[.[0][1] | split("")[] | tostring | "size_idx_"+.] | join(" * ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/V2_VARS/`echo $is | jq -r '[.[0][2] | split("")[] | tostring | "size_idx_"+.] | join(" * ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        sed -i "s/ALL_VARS/`echo $is | jq -r '[([.[0][] | split("")] | add | unique | sort)[] | tostring | "size_idx_"+.] | join(", ")'`/g" drivers/tccg-float/code_main_tccg_49.c &&
        ./bench.sh &&
        cat cogent_fb_results.txt | head -n 1 | tail -n 1 | grep -oP "time\(ms\):  \K[^ ]+" > $ARTIFACT_ROOT/results/gpu/cogent/tc_`echo $is | jq -r '[.[0][] | tostring] | join("_")'`_runtime
      done
    fi
  fi

  if [[ -n "$ENABLE_TENSOR_COMPREHENSIONS" && "$ENABLE_TENSOR_COMPREHENSIONS" -eq 1 ]]; then
    if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Tensor Contractions".frameworks[] | test("^Tensor Comprehensions$")) as $item (false; . or $item)'`" = "true" ]]; then
      # TC
      cd ${ARTIFACT_ROOT}/build/evaluation/tensor_comprehensions
      mkdir -p $ARTIFACT_ROOT/results/gpu/tensor_comprehensions
      for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Tensor Contractions"."input sizes"[]'`; do
        python bench.py `echo $is | jq -r '[.[0][] | tostring] | join(" ")'` `echo $is | jq -r '[.[1][] | tostring] | join(" ")'`
      done
    fi
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMM.frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMM."input sizes"[]'`; do
      ./md_hom_initial_gemm --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (.GEMV.frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '.GEMV."input sizes"[]'`; do
      ./md_hom_initial_gemv --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[] | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Gaussian (2D)".frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Gaussian (2D)"."input sizes"[]'`; do
      ./md_hom_initial_gaussian_static --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[]-4 | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Jacobi (3D)".frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Jacobi (3D)"."input sizes"[]'`; do
      ./md_hom_initial_j3d7pt_static --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size `echo $is | jq -r '[.[]-2 | tostring] | join(" ")'`
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Probabilistic Record Linkage".frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Probabilistic Record Linkage"."input sizes"[]'`; do
      ./md_hom_initial_rl --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-size $is $is
    done
  fi

  if [[ "`cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c 'reduce (."Tensor Contractions".frameworks[] | test("^MDH initial$")) as $item (false; . or $item)'`" = "true" ]]; then
    # md_hom_initial
    cd $ARTIFACT_ROOT/build/evaluation/md_hom_initial &&
    mkdir -p $ARTIFACT_ROOT/results/gpu/$OCL_GPU_PLATFORM_ID/$OCL_GPU_DEVICE_ID/md_hom_initial/
    for is in `cat $ARTIFACT_ROOT/$EXPERIMENTS_FILE | jq -c '."Tensor Contractions"."input sizes"[]'`; do
      ./md_hom_initial_tc --mode bench --platform-id $OCL_GPU_PLATFORM_ID --device-id $OCL_GPU_DEVICE_ID --input-dims `echo $is | jq -r '[.[0][1:3][] | tostring] | join(" ")'` --input-size `echo $is | jq -r '[.[1][] | tostring] | join(" ")'`
    done
  fi

  printf "\n\nReference execution successful!\n"
} || {
  printf "\n\nReference execution failed!\n"
  exit 1
}