#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
{
  if [[ -n "$ENABLE_CPU" && "$ENABLE_CPU" -eq 1 ]]; then
    $ARTIFACT_ROOT/scripts/tune_cpu_md_hom.sh &&
    $ARTIFACT_ROOT/scripts/tune_cpu_references.sh
  fi
  if [[ -n "$ENABLE_GPU" && "$ENABLE_GPU" -eq 1 ]]; then
    $ARTIFACT_ROOT/scripts/tune_gpu_md_hom.sh &&
    $ARTIFACT_ROOT/scripts/tune_gpu_references.sh
  fi
} || {
  printf "\n\Tuning failed!\n"
  exit 1
}