#!/usr/bin/env bash

: ${ARTIFACT_ROOT?"Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."}
if [ -z "$ARTIFACT_ROOT" ]
then
    	echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."
	exit 1
fi

cd $ARTIFACT_ROOT || { echo "Please execute \"source environment.env\" in the root dir of the artifact (the directory containing the scripts folder) first."; exit 1; }
mkdir build &> /dev/null || { rm -rf build; mkdir build; }
{
  # fix permissions in case files have been uploaded using sftp
  chmod u+x $ARTIFACT_ROOT/evaluation/cogent/*.sh &&
  chmod u+x $ARTIFACT_ROOT/evaluation/lift_blas/lift/scripts/* &&
  chmod u+x $ARTIFACT_ROOT/evaluation/lift_blas/lift/*.sh &&
  chmod u+x $ARTIFACT_ROOT/evaluation/lift_stencil/artifact/scripts/* -R &&
  chmod u+x $ARTIFACT_ROOT/evaluation/lift_stencil/artifact/tools/atf/atf/build.sh &&
  chmod u+x $ARTIFACT_ROOT/evaluation/lift_stencil/artifact/tools/atf/atfc/build.sh &&
  chmod u+x $ARTIFACT_ROOT/evaluation/lift_stencil/artifact/tools/atf/atfc/assets/*.sh &&
  chmod u+x $ARTIFACT_ROOT/extern/mkl-dnn/scripts/*.sh &&
  chmod u+x $ARTIFACT_ROOT/scripts/*.sh &&

  # download MKL-DNN bundled MKL
  if [[ -n "$ENABLE_CPU" && "$ENABLE_CPU" -eq 1 && -n "$ENABLE_MKL_DNN" && "$ENABLE_MKL_DNN" -eq 1 ]]; then
    cd $ARTIFACT_ROOT/extern/mkl-dnn/scripts &&
    ./prepare_mkl.sh
  fi &&

  # initialize CMake project
  cd $ARTIFACT_ROOT/build &&
  cmake -DCMAKE_BUILD_TYPE="Release" -DCMAKE_MODULE_PATH=$ARTIFACT_ROOT/cmake/Modules ${@:1} .. &&
  make -j `nproc` &&

  # md_hom_new
  cp -r $ARTIFACT_ROOT/evaluation/md_hom_new/kernel $ARTIFACT_ROOT/build/evaluation/md_hom_new/ &&

  # md_hom_initial
  cp -r $ARTIFACT_ROOT/evaluation/md_hom_initial/kernel $ARTIFACT_ROOT/build/evaluation/md_hom_initial/ &&

  if [[ -n "$ENABLE_LIFT" && "$ENABLE_LIFT" -eq 1 ]]; then
    # Lift stencils
    mkdir -p $ARTIFACT_ROOT/build/evaluation/lift_stencil &&
    cp -r $ARTIFACT_ROOT/evaluation/lift_stencil/artifact $ARTIFACT_ROOT/build/evaluation/lift_stencil/artifact &&
    cd $ARTIFACT_ROOT/build/evaluation/lift_stencil/artifact &&
    source environment.env &&
    ./scripts/installation/build_sbt.sh &&
    ./scripts/installation/build_lift.sh &&
    ./scripts/installation/build_atf.sh &&

    # Lift BLAS
    cp -r $ARTIFACT_ROOT/evaluation/lift_blas $ARTIFACT_ROOT/build/evaluation/ &&
    cd $ARTIFACT_ROOT/build/evaluation/lift_blas &&
    cd lift &&
    sbt compile &&
    cd .. &&
    ARTIFACT_ROOT=`pwd` ./lift/scripts/GenerateGemv &&
    ARTIFACT_ROOT=`pwd` APART_VECTOR_CAST=1 ./lift/scripts/GenerateMMNvidia
  fi &&

  if [[ -n "$ENABLE_EKR" && "$ENABLE_EKR" -eq 1 ]]; then
    # EKR Record Linkage
    cp -r $ARTIFACT_ROOT/evaluation/ekr $ARTIFACT_ROOT/build/evaluation/ &&
    cd $ARTIFACT_ROOT/build/evaluation/ekr/data &&
    for f in `ls *.csv`
    do
      cp $f n_$f
      mv $f i_$f
    done
  fi &&

  if [[ -n "$ENABLE_COGENT" && "$ENABLE_COGENT" -eq 1 ]]; then
    # COGENT
    mkdir -p $ARTIFACT_ROOT/build/evaluation/ &&
    cp -r $ARTIFACT_ROOT/evaluation/cogent $ARTIFACT_ROOT/build/evaluation/
  fi &&

  if [[ -n "$ENABLE_TENSOR_COMPREHENSIONS" && "$ENABLE_TENSOR_COMPREHENSIONS" -eq 1 ]]; then
    # Tensor Comprehensions
    mkdir -p $ARTIFACT_ROOT/build/evaluation/ &&
    cp -r $ARTIFACT_ROOT/evaluation/tensor_comprehensions $ARTIFACT_ROOT/build/evaluation/
  fi &&

  printf "\n\nArtifact installation successful!\n"
} || {
  printf "\n\nArtifact installation failed!\n"
  exit 1
}