# Generating Portable High-Performance Code via Multi-Dimensional Homomorphisms

This artifact contains the workflow to reproduce the results shown in the paper *Generating Portable High-Performance Code via Multi-Dimensional Homomorphisms* accepted for publication at the [28th International Conference on Parallel Architectures and Compilation Techniques (PACT) 2019](https://pactconf.org). The user is invited to perform the steps described below.

In addition to the application examples discussed in the paper (BLAS routines, stencil computations, data mining and machine learning), this repository also contains our preliminary implementations for several other applications, which confirm the wide applicability of our approach: Ensemble of Classifier Chains (ECC), Multi-Layer Perceptron (MLP), and Support Vector Machines (SVM). The source code for these examples can be found in the `preliminary` folder.

## Software Requirements

- an OpenCL 1.2 driver and runtime environment
- OpenGL (libmesa)
- CMake 3.8 or higher
- a compiler supporting C++14 or higher
- Boost 1.56 or higher
- OpenSSL
- finger
- Python 2.7 (not Python 3.x)
- tabulate Python package
- OpenTuner 0.8.0
- Java 8 SDK
- Intel MKL
- NVIDIA cuBLAS
- NVIDIA cuDNN
- Tensor Comprehensions
- jq

## Workflow

The workflow of this artifact is divided into three main steps: **installation**, **tuning**, and **benchmarking**. Note that the tuning step may take a long time (~475h per device), because for each routine and input size both MDH and some of the references are tuned for several hours. The user may edit the file `experiments.json` to decrease the overall auto-tuning time, e.g., by reducing the number of input sizes to auto-tune for or the tuning time per framework. Alternatively, the tuning step can be omitted and the tuning results found on the system described in the paper are used instead. **Be aware that - in case the artifact is executed on devices different from the ones listed in the paper - the tuning step has to be exectued in order to achieve the best, and thus portable, performance. Omitting the tuning step and using our provided parameter values may cause suboptimal performance on a different device and, therefore, prevent the user from reproducing the results shown in the paper.**

All experiments are compiled with the `-O3` flag. Additionally, for the Intel MKL experiments, we use the following flags, as advised by the [Intel Math Kernel Library Link Line Advisor](https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor):

`-Wl,--no-as-needed -lmkl_intel_ilp64 -lmkl_intel_thread -lmkl_core -liomp5 -lpthread -lm -ldl`

### Step 1: Installation

Before compiling the artifact, the following dependencies have to be installed:

- **OpenCL 1.2 driver and runtime:**
  
  Download and install the OpenCL driver and runtime from the vendor website of the utilized hardware.
  
- **OpenGL (libmesa)**:
  
  `$ sudo apt-get install libgl1-mesa-dev`
  
- **CMake 3.8 or higher**:

  Download CMake from the developer website:
  
  `$ wget https://cmake.org/files/v3.13/cmake-3.13.0-rc3-Linux-x86_64.sh`
  
  Install CMake locally. If asked if you want to include the subdirectory in the installation path, type `y`:
  
  `$ /bin/bash cmake-3.13.0-rc3-Linux-x86_64.sh`
  
  Make CMake available:
  
  ``$ export PATH=`pwd`/cmake-3.13.0-rc3-Linux-x86_64/bin:$PATH``
  
- **A compiler supporting C++14 or higher**:
  
  `$ sudo apt-get install gcc g++`

- **Boost 1.56 or higher**:

  `$ sudo apt-get install libboost-all-dev`
  
- **OpenSSL**:

  `$ sudo apt-get install libssl-dev`
  
- **finger**:

  `$ sudo apt-get install finger`

- **Python 2.7 (not Python 3.x)**:

  `$ sudo apt-get install python-dev python-pip`
  
- **tabulate Python package**:

  `$ sudo pip install tabulate`

- **OpenTuner 0.8.0**:

  OpenTuner has dependencies itself. The OpenTuner dependencies can be installed with:
  
  `$ sudo apt-get install sqlite3 libsqlite3-dev`
  
  Afterwards, install OpenTuner by executing:
  
  `$ sudo pip install opentuner`
  
- **Java 8 SDK**:

  The Oracle Java SDK can be installed as follows:
  
  ```
  $ sudo su
  $ echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | $ tee /etc/apt/sources.list.d/webupd8team-java.list
  $ echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list
  $ apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
  $ apt-get update
  $ apt-get install oracle-java8-installer
  $ exit
  ```
  
  After installing Java, please restart your terminal session.
  
- **Intel MKL**:

  Intel MKL can be downloaded at [https://software.intel.com/en-us/mkl/choose-download/linux](https://software.intel.com/en-us/mkl/choose-download/linux). After registering, the Intel MKL library can be downloaded and installed using the provided installation scripts.


- **NVIDIA cuBLAS**:

  NVIDIA cuBLAS is bundled into NVIDIA CUDA Toolkit. The CUDA Toolkit can be downloaded at [https://developer.nvidia.com/cuda-downloads](https://developer.nvidia.com/cuda-downloads). Choose your preferred download type and follow the instructions to install NVIDIA CUDA Toolkit.

- **NVIDIA cuDNN**:

  NVIDIA cuDNN has to be manually installed into an existing NVIDIA CUDA Toolkit installation. The required steps can be found at [https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html](https://docs.nvidia.com/deeplearning/sdk/cudnn-install/index.html).
  
- **Tensor Comprehensions**:

  Tensor Comprehensions can be installed as a conda package. Installation instructions for conda can be found at [https://conda.io/projects/conda/en/latest/user-guide/install/index.html](https://conda.io/projects/conda/en/latest/user-guide/install/index.html). Afterwards, Tensor Comprehensions can be installed as follows:
  
  `$ conda install -y -c pytorch -c tensorcomp tensor_comprehensions`
  
- **jq**:

  `$ sudo apt-get install jq`
  
- **Compiling the artifact**:

  Clone the repository:

  `$ git clone https://gitlab.com/mdh-project/pact_2019_artifact.git`
  
  Change into the artifact directory:
  
  `$ cd pact_2019_artifact`
  
  Edit the file `environment.env` to set the OpenCL platform and device id, and optionally disable references that you do not wish to evaluate. Note that in order to evaluate COGENT, the `CUDA_ARCH` variable in `environment.env` has to be set appropriately. Afterwards, execute:
  
  `$ source environment.env`

  Compile artifact's source code:

  `$ ./scripts/install.sh`

  All arguments provided to script install.sh will be directly forwarded to CMake when building the binaries. For example, forwarding arguments can be required if some dependencies cannot be found by CMake (e.g., when dependencies are not installed in their default locations).


### Step 2: Auto-Tuning

As default, we provide pre-tuned kernels for Intel Xeon E5-2640 v2 CPU and NVIDIA Tesla V100-SXM2-16GB GPU (provided in the `defaults` folder). To use these kernels, execute:

`$ ./scripts/use_defaults.sh`

**Be aware that -- in case the artifact is executed on devices different from the ones listed in the paper -- the tuning has to be exectued in order to achieve the best, and thus portable, performance. Omitting the tuning and using our provided pre-tuned kernels may cause suboptimal performance on a different device and, therefore, prevent the user from reproducing the results shown in the paper.**

The tuning can be run by executing:

`$ ./scripts/tune.sh`

Note that this step may take a long time (~475h per device), because for each routine and input size both MDH and some of the references are tuned for several hours. The user may edit the file `experiments.json` to decrease the overall auto-tuning time, e.g., by reducing the number of input sizes to auto-tune for or the tuning time per framework. Be aware that reducing the tuning time may lead to low performance!

### Step 3: Benchmarking

- Execute benchmarks:
  
  `$ ./scripts/benchmark.sh`
  
- Print results:
  
  `$ ./scripts/print_results.sh`
  
  The best found configurations for our implementations and the ones for the tunable references can be found in the `results` folder.
