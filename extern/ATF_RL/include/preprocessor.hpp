//
// Created by  on 18.04.18.
//

#ifndef preprocessor_h
#define preprocessor_h

#include <string>
#include <vector>

namespace atf {
namespace cf {

void preprocess(const std::string &source, std::string &dest, const std::string &flags);

void preprocess(const std::string &source, std::string &dest, const std::vector<std::string> &defines);

}
}

#endif //preprocessor_h
