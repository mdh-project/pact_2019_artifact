//
// Created by   on 15.02.18.
//
#include "process_wrapper_helper.hpp"

#define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>
#undef  __CL_ENABLE_EXCEPTIONS

#include <iostream>
#include <thread>

namespace atf {
namespace cf {

void ns_sleep(unsigned long long ns) {
    int result = 0;
    {
        struct timespec ts_remaining = {
                ns / 1000000000L,
                ns % 1000000000L
        };

        do {
            struct timespec ts_sleep = ts_remaining;
            result = nanosleep(&ts_sleep, &ts_remaining);
        } while (result != 0 && errno == EINTR);
    }
    if (result != 0) {
        std::cerr << "nanosleep failed: " << strerror(errno) << std::endl;
        exit(1);
    }
}

}
}